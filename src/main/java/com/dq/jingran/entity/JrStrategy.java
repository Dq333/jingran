package com.dq.jingran.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * (JrStrategy)实体类
 *
 * @author Dq
 * @since 2022-09-30 17:38:03
 */
@Data
public class JrStrategy implements Serializable {
    private static final long serialVersionUID = -60430491052710832L;
    /**
    * 旅游攻略id
    */
    private String  id;
    /**
    * 攻略标题
    */
    private String title;
    /**
    * 用户昵称
    */
    private String nickname;
    /**
    * 用户头像
    */
    private String useravatar;
    /**
    * 出发时间
    */
    private String gotime;
    /**
    * 标签
    */
    private String tag;
    /**
    * 同行
    */
    private String friend;
    /**
     * 同行
     */
    private String feeling;
    /**
    * 正文
    */
    private String article;
    /**
    * 总共多少天
    */
    private String totalDay;
    /**
    * 总共多少张图片
    */
    private String totalPicture;
    /**
    * 图像1
    */
    private String img1;
    /**
    * 图形2
    */
    private String img2;
    /**
    * 图像3
    */
    private String img3;

}