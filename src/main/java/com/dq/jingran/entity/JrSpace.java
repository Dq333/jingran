package com.dq.jingran.entity;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * (JrSpace)实体类
 *
 * @author Dq
 * @since 2022-09-24 15:51:47
 */
@Data
public class JrSpace implements Serializable {
    private static final long serialVersionUID = 988244920223739179L;
    /**
    * 空间特有id
    */
    private String id;

    /**
     * 发布者id
     */
    private String userId;
    /**
    * 发布空间的评论
    */
    private String reviews;
    /**
    * 发布空间时需要的图片
    */
    private String img;
    /**
    * 上传时间
    */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtTime;

}