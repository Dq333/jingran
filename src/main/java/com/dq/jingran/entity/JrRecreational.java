package com.dq.jingran.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * (JrRecreational)实体类
 *
 * @author Dq
 * @since 2022-09-30 14:32:32
 */
@Data
public class JrRecreational implements Serializable {
    private static final long serialVersionUID = 818395896151937637L;
    /**
    * 景点休闲区id
    */
    private String id;
    /**
    * 休闲区的图片url
    */
    private String recreationalImg;
    /**
    * 景点id
    */
    private String seneryId;

}