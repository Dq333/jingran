package com.dq.jingran.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * (JrFoods)实体类
 *
 * @author Dq
 * @since 2022-09-27 18:13:52
 */
@Data
public class JrFoods implements Serializable {
    private static final long serialVersionUID = 713190044339329987L;
    /**
    * 食物id
    */
    private String id;
    /**
    * 食物名称（标题）
    */
    private String title;
    /**
    * 打分
    */
    private Double score;

    private String phone;
    private String workTime;
    private String address;
    /**
    * 评论人数
    */
    private Integer commendPerson;
    /**
    * 人均消费
    */
    private Double price;
    /**
    * 特色 （四川第一名菜）
    */
    private String features;
    /**
    * 图片url
    */
    private String img;
    /**
    * 食物推荐语  只是针对水果
    */
    private String command;
    /**
    * 状态（0为水果 ，1为食物）
    */
    private Integer state;

}