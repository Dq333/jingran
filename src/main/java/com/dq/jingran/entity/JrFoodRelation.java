package com.dq.jingran.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * (JrFoodRelation)实体类
 *
 * @author Dq
 * @since 2022-10-18 14:23:08
 */
@Data
public class JrFoodRelation implements Serializable {
    private static final long serialVersionUID = 294367714478114204L;
    /**
    * 食物图片
    */
    private String id;
    
    private String foodId;
    
    private String img;

}