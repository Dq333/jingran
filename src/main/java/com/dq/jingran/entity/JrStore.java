package com.dq.jingran.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * (JrStore)实体类
 *
 * @author Dq
 * @since 2022-10-15 18:52:21
 */
@Data
public class JrStore implements Serializable {
    private static final long serialVersionUID = 568158157638910267L;
    
    private String id;
    
    private String uid;
    
    private String proId;
    /**
    * (1 美食 2 酒店 3风景)
    */
    private Integer state;

}