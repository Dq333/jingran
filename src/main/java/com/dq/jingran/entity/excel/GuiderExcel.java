package com.dq.jingran.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @project: jingran
 * @ClassName: GuiderExcel
 * @author: dq
 * @creat: 2022/9/22 19:12
 */
@Data
public class GuiderExcel {

    @ExcelProperty(index = 0)
    private String name;

    @ExcelProperty(index = 1)
    private Integer age;
    @ExcelProperty(index = 2)
    private Integer expYear;
    @ExcelProperty(index = 3)
    private String img;
    @ExcelProperty(index = 4)
    private String introduce;
}
