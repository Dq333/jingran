package com.dq.jingran.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @project: jingran
 * @ClassName: TitleImg
 * @author: dq
 * @creat: 2022/9/20 11:23
 */
@Data
public class TitleImg {
    @ExcelProperty(index = 0)
    private String title;

    @ExcelProperty(index = 1)
    private String address;

    @ExcelProperty(index = 2)
    private String introduce;

    @ExcelProperty(index = 3)
    private String location;

    @ExcelProperty(index = 4)
    private String id;
}
