package com.dq.jingran.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @project: jingran
 * @ClassName: FoodExcel
 * @author: dq
 * @creat: 2022/9/22 9:35
 */
@Data
public class FoodExcel {
    @ExcelProperty(index = 0)
    private String title;

    @ExcelProperty(index = 1)
    private String img;

    @ExcelProperty(index = 2)
    private String command;

    @ExcelProperty(index = 3)
    private Double averagePrice;



}
