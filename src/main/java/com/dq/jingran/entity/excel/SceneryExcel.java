package com.dq.jingran.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @project: jingran
 * @ClassName: SceneryExcel
 * @author: dq
 * @creat: 2022/9/27 21:07
 */
@Data
public class SceneryExcel {
    /**
     * 具体的旅游景点（琼海）
     */
    @ExcelProperty(index = 0)
    private String title;
    /**
     * 景点细节介绍
     */
    @ExcelProperty(index = 1)
    private String introduce;
    /**
     * 琼海景点的img
     */
    @ExcelProperty(index = 2)
    private String img;
    /**
     * 景点价格
     */
    @ExcelProperty(index = 3)
    private String price;
    /**
     * 观看人数
     */
    @ExcelProperty(index = 4)
    private String viewCount;
    /**
     * 分类
     */
    @ExcelProperty(index = 5)
    private String category;
    /**
     * 得分
     */
    @ExcelProperty(index = 6)
    private String score;
    /**
     * 旅游天数
     */
    @ExcelProperty(index = 7)
    private String travelDay;
    /**
     * 开始地方
     */
    @ExcelProperty(index = 8)
    private String goPlace;
    /**
     * 集合地方
     */
    @ExcelProperty(index = 9)
    private String togetherPlace;
    /**
     * 分开的地方
     */
    @ExcelProperty(index = 10)
    private String breakupPlace;
}
