package com.dq.jingran.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @project: jingran
 * @ClassName: FestivalExcel
 * @author: dq
 * @creat: 2022/9/22 19:10
 */
@Data
public class FestivalExcel {
    @ExcelProperty(index = 0)
    private String title;

    @ExcelProperty(index = 1)
    private String img;

    @ExcelProperty(index = 2)
    private String introduce;

    @ExcelProperty(index = 3)
    private String starteTime;

    @ExcelProperty(index = 4)
    private String endTime;
}
