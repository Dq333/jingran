package com.dq.jingran.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @project: jingran
 * @ClassName: AccommodationExcel
 * @author: dq
 * @creat: 2022/9/26 20:51
 */
@Data
public class AccommodationExcel {
    @ExcelProperty(index = 0)
    private String title;

    @ExcelProperty(index = 1)
    private String img_intro;

    @ExcelProperty(index = 2)
    private String address;

    @ExcelProperty(index = 3)
    private String advantage;

    @ExcelProperty(index = 4)
    private String score;

    @ExcelProperty(index = 5)
    private String price;

    @ExcelProperty(index = 6)
    private String p1;

    @ExcelProperty(index = 7)
    private String p2;

    @ExcelProperty(index = 8)
    private String p3;

    @ExcelProperty(index = 9)
    private String p4;

    @ExcelProperty(index = 10)
    private String p5;

    @ExcelProperty(index = 11)
    private String p6;

    @ExcelProperty(index =12)
    private String introduce;
}
