package com.dq.jingran.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @title 旅游攻略
 * @project: jingran
 * @ClassName: StaategyExcel
 * @author: dq
 * @creat: 2022/9/22 19:14
 */
@Data
public class StrategyExcel {
    @ExcelProperty(index = 0)
    private String title;

    @ExcelProperty(index = 1)
    private String avatar;

    @ExcelProperty(index = 2)
    private String nickname;

    @ExcelProperty(index = 3)
    private String img1;

    @ExcelProperty(index = 4)
    private String img2;

    @ExcelProperty(index = 5)
    private String img3;

    @ExcelProperty(index = 6)
    private String gotime;

    @ExcelProperty(index = 7)
    private String tag;

    @ExcelProperty(index = 8)
    private String friend;

    @ExcelProperty(index = 9)
    private String feeling;

    @ExcelProperty(index = 10)
    private String article;

    @ExcelProperty(index = 11)
    private String totalTime;

    @ExcelProperty(index = 12)
    private String totalImg;


}
