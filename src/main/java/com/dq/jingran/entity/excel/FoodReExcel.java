package com.dq.jingran.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @project: jingran
 * @ClassName: FoodReExcel
 * @author: dq
 * @creat: 2022/10/18 14:22
 */
@Data
public class FoodReExcel {
    @ExcelProperty(index = 0)
    private String fid;
    @ExcelProperty(index = 1)
    private String img;

}
