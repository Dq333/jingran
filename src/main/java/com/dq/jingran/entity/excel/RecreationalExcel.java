package com.dq.jingran.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @project: jingran
 * @ClassName: RecreationalExcel
 * @author: dq
 * @creat: 2022/9/30 14:55
 */
@Data
public class RecreationalExcel {
    @ExcelProperty(index = 0)
    private String img0;
    @ExcelProperty(index = 1)
    private String img1;
    @ExcelProperty(index = 2)
    private String img2;
    @ExcelProperty(index = 3)
    private String img3;
    @ExcelProperty(index = 4)
    private String img4;
    @ExcelProperty(index = 5)
    private String id;

}
