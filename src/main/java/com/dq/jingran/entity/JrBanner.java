package com.dq.jingran.entity;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * (JrBanner)实体类
 *
 * @author Dq
 * @since 2022-09-27 20:43:50
 */
@Data
public class JrBanner implements Serializable {
    private static final long serialVersionUID = 810108668598923960L;
    /**
    * id
    */
    private String id;
    /**
    * 图片banner
    */
    private String img;
    /**
    * 权重
    */
    private Integer sort;
    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtTime;

}