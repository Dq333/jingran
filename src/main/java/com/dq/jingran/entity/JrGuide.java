package com.dq.jingran.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * (JrGuide)实体类
 *
 * @author Dq
 * @since 2022-09-22 19:19:18
 */
@Data
public class JrGuide implements Serializable {
    private static final long serialVersionUID = 806131396697516454L;
    /**
    * id
    */
    private String id;
    /**
    * 姓名
    */
    private String name;
    /**
     * 性别 0为女生 1为男生
     */
    private Integer sex;
    /**
    * 年龄
    */
    private Integer age;
    /**
     * 电话号码
     */
    private String phone;
    /**
     * 工龄
     */
    private Integer expYear;
    /**
    * 个人图片
    */
    private String img;
    /**
    * 个人介绍
    */
    private String introduce;

}