package com.dq.jingran.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * (JrDetails)实体类
 *
 * @author Dq
 * @since 2022-09-26 20:56:14
 */
@Data
public class JrDetails implements Serializable {
    private static final long serialVersionUID = 112249115916983285L;
    /**
    * 图片id
    */
    private String id;
    /**
    * 住宿id
    */
    private String accommodationId;
    /**
    * 图片url
    */
    private String img;

}