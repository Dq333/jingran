package com.dq.jingran.entity;

import java.util.Date;
import java.io.Serializable;
import lombok.Data;

/**
 * (JrFestival)实体类
 *
 * @author Dq
 * @since 2022-09-22 19:19:06
 */
@Data
public class JrFestival implements Serializable {
    private static final long serialVersionUID = -39116742725378723L;
    /**
    * 节日id
    */
    private String id;
    /**
    * 节日主题
    */
    private String titile;
    /**
    * 节日的介绍
    */
    private String introduce;
    /**
    * 节日的图片
    */
    private String img;
    /**
    * 举办时间
    */
    private String startTime;
    /**
    * 结束时间
    */
    private String endTime;

}