package com.dq.jingran.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * (JrAccommodation)实体类
 *
 * @author Dq
 * @since 2022-09-26 20:55:54
 */
@Data
public class JrAccommodation implements Serializable {
    private static final long serialVersionUID = 589802166253488295L;
    /**
    * 住宿的id
    */
    private String id;
    /**
     * 住宿的电话
     */
    private String phone;
    /**
     * 住宿的收藏
     */
    private String collection;
    /**
     * 住宿的修饰
     */
    private String modified;
    /**
    * 住宿的名称
    */
    private String title;
    /**
     * 住宿的名称
     */
    private String price;
    /**
    * 住宿地址
    */
    private String address;
    /**
    * 平均消费
    */
    private String advantage;
    /**
    * 住宿的简介
    */
    private String introduce;
    /**
    * 平均得分
    */
    private Double accommodationScore;
    /**
    * 住宿图片
    */
    private String accommodationImg;

}