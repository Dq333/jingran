package com.dq.jingran.entity;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * (JrComments)实体类
 *
 * @author Dq
 * @since 2022-09-24 15:54:04
 */
@Data
public class JrComments implements Serializable {
    private static final long serialVersionUID = -89636319111705029L;
    /**
    * 评论id
    */
    private String id;
    /**
     * 发布者id
     */
    private String userId;

    /**
    * 空间的索引条件
    */
    private String spaceId;
    /**
    * 评论的语言
    */
    private String reviews;
    /**
    * 发布的时间
    */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtTime;

}