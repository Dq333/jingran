package com.dq.jingran.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * (JrScenery)实体类
 *
 * @author Dq
 * @since 2022-09-27 17:52:11
 */
@Data
public class JrScenery implements Serializable {
    private static final long serialVersionUID = 940371442594866221L;
    /**
    * 旅游景点id
    */
    private String id;
    /**
    * 具体的旅游景点（琼海）
    */
    private String title;
    /**
    * 景点细节介绍
    */
    private String introduce;
    /**
    * 琼海景点的img
    */
    private String img;

    /**
    * 景点地址（省 市）
    */
    private String location;
    /**
    * 观看人数
    */
    private Integer viewCount;
    /**
    * 分类
    */
    private String category;
    /**
    * 得分
    */
    private Double score;
    /**
    * 旅游天数
    */
    private String travelDay;
    /**
    * 开始地方
    */
    private String goPlace;
    /**
    * 集合地方
    */
    private String togetherPlace;
    /**
    * 分开的地方
    */
    private String breakupPlace;
    /**
     * 景点价格
     */
    private String price;

}