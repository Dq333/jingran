package com.dq.jingran.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * (JrUser)实体类
 *
 * @author Dq
 * @since 2022-09-19 17:44:02
 */
@Data
public class JrUser implements Serializable {
    private static final long serialVersionUID = 184028060126083307L;
    /**
    * 用户id
    */
    private String id;
    /**
    * 用户姓名
    */
    private String username;
    /**
    * 用户密码
    */
    private String password;
    /**
    * 用户头像（给予默认头像）
    */
    private String avatar;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}