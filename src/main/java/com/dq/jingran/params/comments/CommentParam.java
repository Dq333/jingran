package com.dq.jingran.params.comments;

import lombok.Data;

/**
 * @project: jingran
 * @ClassName: CommentParam
 * @author: dq
 * @creat: 2022/9/24 16:56
 */
@Data
public class CommentParam {
    /**
     * 用户id的映射
     */
    private String userId;
    /**
     * 空间id的映射
     */
    private String spaceId;
    /**
     * 评论的详细类容
     */
    private String reviews;
}
