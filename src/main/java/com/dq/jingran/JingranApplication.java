package com.dq.jingran;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @project: jingran
 * @ClassName: JingranApplication
 * @author: dq
 * @creat: 2022/9/19 18:12
 */
@SpringBootApplication
public class JingranApplication {
    public static void main(String[] args) {
        SpringApplication.run(JingranApplication.class,args);
    }
}
