package com.dq.jingran.mapper;

import com.dq.jingran.entity.JrComments;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (JrComments)表数据库访问层
 *
 * @author Dq
 * @since 2022-09-24 15:54:04
 */
@Mapper
public interface JrCommentsMapper extends BaseMapper<JrComments> {

}