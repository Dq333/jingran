package com.dq.jingran.mapper;

import com.dq.jingran.entity.JrUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (JrUser)表数据库访问层
 *
 * @author Dq
 * @since 2022-09-19 17:44:02
 */
@Mapper
public interface JrUserMapper extends BaseMapper<JrUser> {

}