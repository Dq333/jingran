package com.dq.jingran.mapper;

import com.dq.jingran.entity.JrBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (JrBanner)表数据库访问层
 *
 * @author Dq
 * @since 2022-09-27 20:43:50
 */
@Mapper
public interface JrBannerMapper extends BaseMapper<JrBanner> {

}