package com.dq.jingran.mapper;

import com.dq.jingran.entity.JrScenery;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (JrScenery)表数据库访问层
 *
 * @author Dq
 * @since 2022-09-27 17:52:12
 */
@Mapper
public interface JrSceneryMapper extends BaseMapper<JrScenery> {

}