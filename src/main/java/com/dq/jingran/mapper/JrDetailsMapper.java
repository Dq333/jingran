package com.dq.jingran.mapper;

import com.dq.jingran.entity.JrDetails;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (JrDetails)表数据库访问层
 *
 * @author Dq
 * @since 2022-09-26 20:56:14
 */
@Mapper
public interface JrDetailsMapper extends BaseMapper<JrDetails> {

}