package com.dq.jingran.mapper;

import com.dq.jingran.entity.JrStore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (JrStore)表数据库访问层
 *
 * @author Dq
 * @since 2022-10-15 18:52:21
 */
@Mapper
public interface JrStoreMapper extends BaseMapper<JrStore> {

}