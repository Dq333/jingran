package com.dq.jingran.mapper;

import com.dq.jingran.entity.JrFestival;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (JrFestival)表数据库访问层
 *
 * @author Dq
 * @since 2022-09-22 19:19:06
 */
@Mapper
public interface JrFestivalMapper extends BaseMapper<JrFestival> {

}