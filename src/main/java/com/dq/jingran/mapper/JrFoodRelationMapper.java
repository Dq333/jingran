package com.dq.jingran.mapper;

import com.dq.jingran.entity.JrFoodRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (JrFoodRelation)表数据库访问层
 *
 * @author Dq
 * @since 2022-10-18 14:23:08
 */
@Mapper
public interface JrFoodRelationMapper extends BaseMapper<JrFoodRelation> {

}