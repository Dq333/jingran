package com.dq.jingran.mapper;

import com.dq.jingran.entity.JrRecreational;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (JrRecreational)表数据库访问层
 *
 * @author Dq
 * @since 2022-09-30 14:32:39
 */
@Mapper
public interface JrRecreationalMapper extends BaseMapper<JrRecreational> {

}