package com.dq.jingran.mapper;

import com.dq.jingran.entity.JrFoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (JrFoods)表数据库访问层
 *
 * @author Dq
 * @since 2022-09-27 18:13:53
 */
@Mapper
public interface JrFoodsMapper extends BaseMapper<JrFoods> {

}