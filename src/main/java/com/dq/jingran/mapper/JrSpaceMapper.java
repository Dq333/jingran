package com.dq.jingran.mapper;

import com.dq.jingran.entity.JrSpace;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (JrSpace)表数据库访问层
 *
 * @author Dq
 * @since 2022-09-24 15:51:47
 */
@Mapper
public interface JrSpaceMapper extends BaseMapper<JrSpace> {

}