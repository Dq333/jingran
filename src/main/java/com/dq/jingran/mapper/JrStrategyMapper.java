package com.dq.jingran.mapper;

import com.dq.jingran.entity.JrStrategy;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (JrStrategy)表数据库访问层
 *
 * @author Dq
 * @since 2022-09-30 17:38:04
 */
@Mapper
public interface JrStrategyMapper extends BaseMapper<JrStrategy> {

}