package com.dq.jingran.mapper;

import com.dq.jingran.entity.JrGuide;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (JrGuide)表数据库访问层
 *
 * @author Dq
 * @since 2022-09-22 19:19:18
 */
@Mapper
public interface JrGuideMapper extends BaseMapper<JrGuide> {

}