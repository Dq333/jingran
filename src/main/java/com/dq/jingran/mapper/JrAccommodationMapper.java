package com.dq.jingran.mapper;

import com.dq.jingran.entity.JrAccommodation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (JrAccommodation)表数据库访问层
 *
 * @author Dq
 * @since 2022-09-26 20:55:54
 */
@Mapper
public interface JrAccommodationMapper extends BaseMapper<JrAccommodation> {

}