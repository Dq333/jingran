package com.dq.jingran.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.dq.jingran.entity.JrRecreational;
import com.dq.jingran.entity.excel.RecreationalExcel;
import com.dq.jingran.service.JrRecreationalService;

/**
 * @project: jingran
 * @ClassName: ExcelRecreationalListener
 * @author: dq
 * @creat: 2022/9/30 14:56
 */
public class ExcelRecreationalListener extends AnalysisEventListener<RecreationalExcel> {
    private JrRecreationalService jrRecreationalService;

    public ExcelRecreationalListener(JrRecreationalService recreationalService) {
        this.jrRecreationalService = recreationalService;
    }

    public ExcelRecreationalListener() {
    }

    @Override
    public void invoke(RecreationalExcel data, AnalysisContext context) {

        System.out.println("begin"+data.getImg1());
        if (data.getImg0()!=null){
            JrRecreational jrRecreational = new JrRecreational();
            jrRecreational.setSeneryId(data.getId());
            jrRecreational.setRecreationalImg(data.getImg0());
            jrRecreationalService.save(jrRecreational);
        }
        if (data.getImg1()!=null){
            JrRecreational jrRecreational = new JrRecreational();
            jrRecreational.setSeneryId(data.getId());
            jrRecreational.setRecreationalImg(data.getImg1());
            jrRecreationalService.save(jrRecreational);
        }
        if (data.getImg2()!=null){
            JrRecreational jrRecreational = new JrRecreational();
            jrRecreational.setSeneryId(data.getId());
            jrRecreational.setRecreationalImg(data.getImg2());
            jrRecreationalService.save(jrRecreational);
        }
        if (data.getImg3()!=null){
            JrRecreational jrRecreational = new JrRecreational();
            jrRecreational.setSeneryId(data.getId());
            jrRecreational.setRecreationalImg(data.getImg3());
            jrRecreationalService.save(jrRecreational);
        }
        if (data.getImg4()!=null){
            JrRecreational jrRecreational = new JrRecreational();
            jrRecreational.setSeneryId(data.getId());
            jrRecreational.setRecreationalImg(data.getImg4());
            jrRecreationalService.save(jrRecreational);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
