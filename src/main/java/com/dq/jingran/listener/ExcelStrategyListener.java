package com.dq.jingran.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.dq.jingran.entity.JrStrategy;
import com.dq.jingran.entity.excel.StrategyExcel;
import com.dq.jingran.service.JrStrategyService;

/**
 * @project: jingran
 * @ClassName: ExcelStrategyListener
 * @author: dq
 * @creat: 2022/9/30 17:47
 */
public class ExcelStrategyListener extends AnalysisEventListener<StrategyExcel> {

    private JrStrategyService jrStrategyService;

    public ExcelStrategyListener(JrStrategyService jrStrategyService) {
        this.jrStrategyService = jrStrategyService;
    }

    public ExcelStrategyListener() {
    }

    @Override
    public void invoke(StrategyExcel data, AnalysisContext context) {
        JrStrategy jrStrategy = new JrStrategy();
        jrStrategy.setTitle(data.getTitle());
        jrStrategy.setUseravatar(data.getAvatar());
        jrStrategy.setNickname(data.getNickname());
        jrStrategy.setImg1(data.getImg1());
        jrStrategy.setImg2(data.getImg2());
        jrStrategy.setImg3(data.getImg3());
        jrStrategy.setGotime(data.getGotime());
        jrStrategy.setTag(data.getTag());
        jrStrategy.setFriend(data.getFriend());
        jrStrategy.setFeeling(data.getFeeling());
        jrStrategy.setArticle(data.getArticle());
        jrStrategy.setTotalDay(data.getTotalImg());
        jrStrategy.setTotalPicture(data.getTotalImg());
        jrStrategyService.save(jrStrategy);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
