package com.dq.jingran.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.dq.jingran.entity.JrAccommodation;
import com.dq.jingran.entity.JrDetails;
import com.dq.jingran.entity.excel.AccommodationExcel;
import com.dq.jingran.service.JrAccommodationService;
import com.dq.jingran.service.JrDetailsService;
import lombok.extern.slf4j.Slf4j;

/**
 * @project: jingran
 * @ClassName: ExcelAccommodationListener
 * @author: dq
 * @creat: 2022/9/26 20:58
 */
@Slf4j
public class ExcelAccommodationListener extends AnalysisEventListener<AccommodationExcel> {

    private JrAccommodationService accommodationService;

    private JrDetailsService detailsService;

    public ExcelAccommodationListener() {
    }

    public ExcelAccommodationListener(JrAccommodationService accommodationService, JrDetailsService detailsService) {
        this.accommodationService = accommodationService;
        this.detailsService = detailsService;
    }

    @Override
    public void invoke(AccommodationExcel data, AnalysisContext context) {
        log.info("begin" ,data.getTitle());
        JrAccommodation jrAccommodation = new JrAccommodation();
        jrAccommodation.setTitle(data.getTitle());
        jrAccommodation.setIntroduce(data.getIntroduce());
        jrAccommodation.setAddress(data.getAddress());
        jrAccommodation.setAdvantage(data.getAdvantage());
        jrAccommodation.setAccommodationImg(data.getImg_intro());
        jrAccommodation.setAccommodationScore(Double.valueOf(data.getScore()));

        accommodationService.save(jrAccommodation);
        String accommodationId = jrAccommodation.getId();

        if (data.getP1() != null) {
            JrDetails jrDetails = new JrDetails();
            jrDetails.setAccommodationId(accommodationId);
            jrDetails.setImg(data.getP1());
            detailsService.save(jrDetails);
        }
        if (data.getP2() != null) {
            JrDetails jrDetails = new JrDetails();
            jrDetails.setAccommodationId(accommodationId);
            jrDetails.setImg(data.getP2());
            detailsService.save(jrDetails);
        }
        if (data.getP3() != null) {
            JrDetails jrDetails = new JrDetails();
            jrDetails.setAccommodationId(accommodationId);
            jrDetails.setImg(data.getP3());
            detailsService.save(jrDetails);
        }
        if (data.getP4() != null) {
            JrDetails jrDetails = new JrDetails();
            jrDetails.setAccommodationId(accommodationId);
            jrDetails.setImg(data.getP4());
            detailsService.save(jrDetails);
        }
        if (data.getP5()!= null) {
            JrDetails jrDetails = new JrDetails();
            jrDetails.setAccommodationId(accommodationId);
            jrDetails.setImg(data.getP5());
            detailsService.save(jrDetails);
        }
        if (data.getP6()!= null) {
            JrDetails jrDetails = new JrDetails();
            jrDetails.setAccommodationId(accommodationId);
            jrDetails.setImg(data.getP6());
            detailsService.save(jrDetails);
        }

    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
    }
}
