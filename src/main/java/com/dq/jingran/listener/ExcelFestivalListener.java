package com.dq.jingran.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.dq.jingran.entity.JrFestival;
import com.dq.jingran.entity.excel.FestivalExcel;
import com.dq.jingran.service.JrFestivalService;

/**
 * @project: jingran
 * @ClassName: ExcelFestivalListener
 * @author: dq
 * @creat: 2022/9/22 19:27
 */
public class ExcelFestivalListener extends AnalysisEventListener<FestivalExcel> {

    private JrFestivalService festivalService;

    public ExcelFestivalListener() {
    }

    public ExcelFestivalListener(JrFestivalService festivalService) {
        this.festivalService = festivalService;
    }

    @Override
    public void invoke(FestivalExcel data, AnalysisContext context) {
        JrFestival jrFestival = new JrFestival();
        jrFestival.setTitile(data.getTitle());
        /*String img = data.getImg();
        img = img.split("\\?")[0];*/
        jrFestival.setImg(data.getImg());
        jrFestival.setIntroduce(data.getIntroduce());
        jrFestival.setStartTime(data.getStarteTime());
        jrFestival.setEndTime(data.getEndTime());
        festivalService.save(jrFestival);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
