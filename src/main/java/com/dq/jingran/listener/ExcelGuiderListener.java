package com.dq.jingran.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.dq.jingran.entity.JrGuide;
import com.dq.jingran.entity.excel.GuiderExcel;
import com.dq.jingran.service.JrGuideService;

/**
 * @project: jingran
 * @ClassName: ExcelGuiderListener
 * @author: dq
 * @creat: 2022/9/22 19:27
 */
public class ExcelGuiderListener extends AnalysisEventListener<GuiderExcel> {
    //bean传递
    private JrGuideService guideService;
    public ExcelGuiderListener() {
    }

    public ExcelGuiderListener(JrGuideService jrGuideService){
        this.guideService = jrGuideService;
    }

    @Override
    public void invoke(GuiderExcel data, AnalysisContext context) {
        JrGuide jrGuide = new JrGuide();
        jrGuide.setName(data.getName());
        jrGuide.setAge(data.getAge());
        /*String img = data.getImg();
        img = img.split("\\?")[0];*/
        jrGuide.setImg(data.getImg());
        jrGuide.setSex(1);
        jrGuide.setExpYear(data.getExpYear());
        jrGuide.setIntroduce(data.getIntroduce());
        guideService.save(jrGuide);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
