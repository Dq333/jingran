package com.dq.jingran.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.dq.jingran.entity.JrFoodRelation;
import com.dq.jingran.entity.excel.FoodReExcel;
import com.dq.jingran.service.JrFoodRelationService;

/**
 * @project: jingran
 * @ClassName: ExcelFoodRelListener
 * @author: dq
 * @creat: 2022/10/18 14:24
 */
public class ExcelFoodRelListener extends AnalysisEventListener<FoodReExcel> {
    private JrFoodRelationService foodRelationService;
    public ExcelFoodRelListener() {
    }

    public ExcelFoodRelListener(JrFoodRelationService foodRelationService) {
        this.foodRelationService = foodRelationService;
    }

    @Override
    public void invoke(FoodReExcel data, AnalysisContext context) {
        JrFoodRelation jrFoodRelation = new JrFoodRelation();
        jrFoodRelation.setFoodId(data.getFid());
        jrFoodRelation.setImg(data.getImg());
        foodRelationService.save(jrFoodRelation);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
