package com.dq.jingran.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.dq.jingran.entity.JrScenery;
import com.dq.jingran.entity.excel.SceneryExcel;
import com.dq.jingran.service.JrSceneryService;

/**
 * @project: jingran
 * @ClassName: ExcelSceneryListener
 * @author: dq
 * @creat: 2022/9/27 21:15
 */
public class ExcelSceneryListener extends AnalysisEventListener<SceneryExcel> {
    private JrSceneryService sceneryService;

    public ExcelSceneryListener() {
    }

    public ExcelSceneryListener(JrSceneryService sceneryService) {
        this.sceneryService = sceneryService;
    }

    @Override
    public void invoke(SceneryExcel data, AnalysisContext context) {
        JrScenery jrScenery = new JrScenery();
        jrScenery.setTitle(data.getTitle());
        jrScenery.setIntroduce(data.getIntroduce());
        jrScenery.setImg(data.getImg());
        jrScenery.setPrice(data.getPrice());
        jrScenery.setViewCount(Integer.parseInt(data.getViewCount()));
        jrScenery.setCategory(data.getCategory());
        jrScenery.setLocation(data.getTogetherPlace());
        jrScenery.setScore(Double.parseDouble(data.getScore()));
        jrScenery.setTravelDay(data.getTravelDay());
        jrScenery.setGoPlace(data.getGoPlace());
        jrScenery.setTogetherPlace(data.getTogetherPlace());
        jrScenery.setBreakupPlace(data.getBreakupPlace());

        sceneryService.save(jrScenery);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
