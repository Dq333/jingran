package com.dq.jingran.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.dq.jingran.entity.JrFoods;
import com.dq.jingran.entity.excel.FoodExcel;
import com.dq.jingran.service.JrFoodsService;

/**
 * @project: jingran
 * @ClassName: ExcelFoodListener
 * @author: dq
 * @creat: 2022/9/22 15:49
 */
public class ExcelFoodListener extends AnalysisEventListener<FoodExcel>{
    private JrFoodsService foodsService;

    public ExcelFoodListener() {
    }

    public ExcelFoodListener(JrFoodsService jrFoodsService) {
        this.foodsService = jrFoodsService;
    }

    @Override
    public void invoke(FoodExcel data, AnalysisContext context) {
        JrFoods jrFoods = new JrFoods();
        jrFoods.setTitle(data.getTitle());
        jrFoods.setCommand(data.getCommand());
        jrFoods.setPrice(data.getAveragePrice());
        /*String img = data.getImg();
        String img_total = img.split("\\?")[0];*/
        jrFoods.setImg(data.getImg());
        jrFoods.setState(1);
        foodsService.save(jrFoods);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
