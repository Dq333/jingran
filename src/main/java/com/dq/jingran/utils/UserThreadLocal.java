package com.dq.jingran.utils;

import com.dq.jingran.entity.JrUser;

/**
 * @project: jingran
 * @ClassName: UserThreadLocal
 * @author: dq
 * @creat: 2022/10/9 8:15
 */
public class UserThreadLocal {
    private JrUser user;
    private UserThreadLocal(){};
    private static final ThreadLocal<JrUser> LOCAL = new ThreadLocal<>();

    public static void put(JrUser user) {
        LOCAL.set(user);
    }

    public static JrUser get(){
        return LOCAL.get();
    }

    public static void delete(){
        LOCAL.remove();
    }
}
