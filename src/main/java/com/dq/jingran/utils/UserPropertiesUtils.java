package com.dq.jingran.utils;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


/**
 * @project: jingran
 * @ClassName: UserPropertiesUtils
 * @author: dq
 * @creat: 2022/10/15 15:30
 */
@Configuration
@PropertySource("classpath:application-dev.yml")
@ConfigurationProperties(prefix = "oss")
@Data
public class UserPropertiesUtils {
    //读取yml 中的文件
    private String secretId; //keyId
    private String secretKey; //密钥
    private String bucketName; //存储名称
    private String region; //地区
    private String prefix; //文件名
    private String url; //url
}
