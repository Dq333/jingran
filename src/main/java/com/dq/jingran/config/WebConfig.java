package com.dq.jingran.config;

import com.dq.jingran.exception.LoginHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @project: jingran
 * @ClassName: WebConfig
 * @author: dq
 * @creat: 2022/9/19 17:49
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    private LoginHandler loginHandler;

    /**
     * 添加拦截器  实现线程隔离
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginHandler)
                .addPathPatterns("/user/test")
                .addPathPatterns("/user/getInfo");
    }

    /**
     * 整体跨域配置
     * @param registry
     */
   /* @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry
                .addMapping("/**")
                .allowCredentials(true)
                .allowedMethods("*");
    }*/
}
