package com.dq.jingran.controller;

import com.dq.jingran.entity.JrFestival;
import com.dq.jingran.service.JrFestivalService;
import com.dq.jingran.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * JrFestival-API
 * @Title 节日类
 * @author Dq
 * @since 2022-09-22 19:19:06
 */
@RestController
@RequestMapping("festival")
@CrossOrigin
public class JrFestivalController {
    @Autowired
    private JrFestivalService festivalService;

    @GetMapping("/getAll")
    public R getAllFestival(){
        List<JrFestival> festivalList =festivalService.getAllFestivalData();
        return R.success_Normal().data("festivalList",festivalList);
    }


}