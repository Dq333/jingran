package com.dq.jingran.controller;

import com.dq.jingran.entity.JrStrategy;
import com.dq.jingran.service.JrStrategyService;
import com.dq.jingran.vo.R;
import com.dq.jingran.vo.strategy.StrategyListVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * JrStrategy-API
 * @author Dq
 * @since 2022-09-30 17:38:02
 */
@RestController
@RequestMapping("jrStrategy")
@CrossOrigin
public class JrStrategyController {

    @Autowired
    private JrStrategyService jrStrategyService;


    /**
     * 获取简单的数据
     * @return
     */
    @GetMapping("list")
    public R getListData(){
        List<StrategyListVo> strategyVos =jrStrategyService.getDateList();
        return R.success_Normal().data("strategyList",strategyVos);
    }

    /**
     * 根据id获取当前所有数据
     * @param id
     * @return
     */
    @PostMapping("getOneAllInfoById/{id}")
    public R getAllInfoById(@PathVariable("id") String id){
        JrStrategy jrStrategy = jrStrategyService.getById(id);
        return R.success_Normal().data("strategy",jrStrategy);
    }


}