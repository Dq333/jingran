package com.dq.jingran.controller;

import com.dq.jingran.entity.JrFoods;
import com.dq.jingran.service.JrFoodsService;
import com.dq.jingran.utils.MybatisUtils;
import com.dq.jingran.vo.R;
import com.dq.jingran.vo.food.FoodVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * JrFoods-API
 * @author Dq
 * @since 2022-09-27 18:13:51
 */
@RestController
@RequestMapping("jrFoods")
@CrossOrigin
public class JrFoodsController {

    @Autowired
    private JrFoodsService jrFoodsService;

    /**
     * 根据state获取数据
     * @param state
     * @return
     */
    @GetMapping("/getInfoBySatute/{state}")
    public R getInfo(@PathVariable("state") String state){

        List<JrFoods> foodsList =jrFoodsService.getAllInfo(state);
        return R.success_Normal().data("FoodsInfo",foodsList);
    }

    /**
     * 随机获取食物
     * @return
     */
    @PostMapping("getLimitData")
    public R getLimitData(){
        List<JrFoods> foodsList=jrFoodsService.getLimitData();
        return R.success_Normal().data("FoodsInfo",foodsList);
    }

    /**
     * 获取食物的详情
     * @param fId
     * @param uid
     * @return
     */
    @PostMapping("getFoodDetail/{foodId}/{uid}")
    public R getFoodDetail(@PathVariable("foodId") String fId,@PathVariable("uid") String uid){
        FoodVo food =jrFoodsService.getFoodDetail(fId,uid);
        return R.success_Normal().data("food",food);
    }
    
}