package com.dq.jingran.controller;

import com.dq.jingran.entity.JrUser;
import com.dq.jingran.exception.ExceptionVo;
import com.dq.jingran.service.JrUserService;
import com.dq.jingran.utils.UserThreadLocal;
import com.dq.jingran.vo.R;
import com.dq.jingran.vo.user.ChangeUserNameParams;
import com.dq.jingran.vo.user.UpdatePassParams;
import com.dq.jingran.vo.user.UserParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * JrUser-API
 * @author Dq
 * @since 2022-09-19 17:44:01
 */
@RestController
@RequestMapping("user")
@CrossOrigin
public class JrUserController {

    @Autowired
    private JrUserService userService;

    /**
     * 用户登录
     * @return
     */
    @PostMapping("login")
    public R login(@RequestBody UserParams userParams){
        String token = userService.login(userParams);
        return R.success_Normal().data("token",token);

    }

    /**
     * 注册接口
     * @param user
     * @return
     */
    @PostMapping("register")
    public R registerUser(@RequestBody UserParams user){
        String token=userService.registerUser(user);
        return R.success_Normal().data("token",token);
    }

    /**
     * 获取用户基本信息
     * @param token
     * @return
     */
    @GetMapping("getInfo")
    public R getBasicUserInfo(@RequestHeader("auth") String token){
        // UserThreadLocal
        JrUser jrUser =userService.checkTokenAndReturnData(token);
        return R.success_Normal().data("user",jrUser);
    }

    @GetMapping("getNew")
    public R getgetNew(@RequestHeader("auth") String token){
        // UserThreadLocal
        JrUser jrUser =userService.getNew(token);
        return R.success_Normal().data("user",jrUser);
    }

    @GetMapping("test")
    public R test(){
        JrUser jrUser = UserThreadLocal.get();
        return R.success_Normal().data("user",jrUser);
    }

    /**
     * 修改密码
     * f8d8c25d4b9dd50a1de8af7f40554754
     * 7fa8282ad93047a4d6fe6111c93b308a
     * @param userParams
     * @return
     */
    @PostMapping("updatePass")
    public R updatePass(@RequestBody UpdatePassParams userParams){
        userService.updatePass(userParams);
        return R.success_Normal().message("修改密码成功");
    }

    /**
     * 上传头像到腾讯云Oss
     * https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/avater/avatar.png
     * https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/avater/2022/10/16/a3929f174edd4355a20a94eb90425670.jpg
     * @return
     */
    @PostMapping("changeImg")
    public R changeImg(String auth, MultipartFile file){
        System.out.println("111");
        if (StringUtils.isEmpty(auth)){
            return R.error("请求头不能为空");
        }
        if (file.getSize()==0){
            throw new ExceptionVo(10002,"图片不能为空");
        }

        String avatarUrl =userService.upImgToTencentOss(auth,file);
        return R.success_Normal().data("avatar",avatarUrl);
    }


    /**
     * 修改昵称
     * @param userParams
     * @return
     */
    @PostMapping("changeName")
    public R changeName(@RequestBody ChangeUserNameParams userParams){
        if (StringUtils.isEmpty(userParams.getUserId())){
            return R.error("userId不能为空");
        }
        if (StringUtils.isEmpty(userParams.getUserName())){
            return R.error("修改名称不能为空");
        }
        userService.changeName(userParams);
        return R.success_Normal().message("修改昵称成功").data("userName",userParams.getUserName());
    }



}