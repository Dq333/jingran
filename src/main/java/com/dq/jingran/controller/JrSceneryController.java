package com.dq.jingran.controller;

import com.dq.jingran.entity.JrScenery;
import com.dq.jingran.exception.ExceptionVo;
import com.dq.jingran.service.JrSceneryService;
import com.dq.jingran.vo.R;
import com.dq.jingran.vo.scenery.JrSceneryVo;
import com.dq.jingran.vo.scenery.SceneryVo;
import com.dq.jingran.vo.scenery.SearchSceneryParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * JrScenery-API 旅游景点
 * @author Dq
 * @since 2022-09-27 17:52:10
 */
@RestController
@RequestMapping("jrScenery")
@CrossOrigin
public class JrSceneryController {

    @Autowired
    private JrSceneryService jrSceneryService;

    @PostMapping("/search")
    public R searchScenery(@RequestBody SearchSceneryParams sceneryParams){
        if (StringUtils.isEmpty(sceneryParams.getFuzzyName())){
            throw new ExceptionVo(401,"传入的参数不能为空");
        }
        List<SceneryVo> sceneryVos=jrSceneryService.searchItems(sceneryParams);
        if (sceneryVos ==null){
            return R.success_Normal().message("相关内容为空，请搜索其他景点~").code(20005);
        }
        return R.success_Normal().data("scenerys",sceneryVos);
    }

    /**
     * 获取景点基本信息
     * @return
     */
    @GetMapping("getSceneryBasicInfo")
    public R getBasicInfo(){
        List<SceneryVo> sceneryList=jrSceneryService.getBasicInfo();
        return R.success_Normal().data("sceneryList",sceneryList);
    }

    /**
     * 根据单一id查询数据
     * @param pid
     * @param userId
     * @return
     */
    @PostMapping("getSceneryAllInfo/{pid}/{uid}")
    public R getSceneryAllInfo(@PathVariable("pid") String pid,@PathVariable("uid") String userId){
        JrSceneryVo jrScenery = jrSceneryService.getByAllById(pid,userId);
        return R.success_Normal().data("jrScenery",jrScenery);
    }

    /**
     * 获取部分景点信息
     * @return
     */
    @PostMapping("getPartInfo")
    public R getPartInfo(){
        List<SceneryVo> sceneryList=jrSceneryService.getPartInfo();
        return R.success_Normal().data("sceneryList",sceneryList);
    }


}