package com.dq.jingran.controller;

import com.dq.jingran.entity.JrGuide;
import com.dq.jingran.service.JrGuideService;
import com.dq.jingran.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * JrGuide-API
 * @Title 导游类
 * @author Dq
 * @since 2022-09-22 19:19:18
 */
@RestController
@RequestMapping("guide")
@CrossOrigin
public class JrGuideController {


    @Autowired
    private JrGuideService guideService;

    @GetMapping("getAll")
    public R getAllGuider(){
        List<JrGuide> guides = guideService.list();
        return R.success_Normal().data("guide",guides);
    }
}