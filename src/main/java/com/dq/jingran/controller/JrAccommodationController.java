package com.dq.jingran.controller;

import com.dq.jingran.service.JrAccommodationService;
import com.dq.jingran.vo.R;
import com.dq.jingran.vo.accommodation.AccommodationVo;
import com.dq.jingran.vo.accommodation.PublishAccAndDetailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * JrAccommodation-API
 * @author Dq
 * @since 2022-09-26 20:55:54
 */
@RestController
@RequestMapping("accommodation")
@CrossOrigin
public class JrAccommodationController {

    @Autowired
    private JrAccommodationService accommodationService;

    /**
     * 获取简单的住宿信息
     * @return
     */
    @PostMapping("getBasicAccommodationInfo")
    public R getBasicAccommodationInfo(){
        List<AccommodationVo> accommodationVoList=accommodationService.getBasicAccommodationInfo();
        return R.success_Normal().data("accommodationList",accommodationVoList);
    }

    /**
     * 获取单一住宿的具体信息
     * @param accommodationId
     * @return
     */
    @PostMapping("getDetailAccommodationInfo/{accommodationId}/{uid}")
    public R getDetailAccommodationInfo(@PathVariable("accommodationId") String accommodationId ,@PathVariable String uid){
        PublishAccAndDetailVo accAndDetailVos=accommodationService.getDetailAccommodationInfoById(accommodationId,uid);
        return R.success_Normal().data("accAndDetailList",accAndDetailVos);
    }

}