package com.dq.jingran.controller;

import com.dq.jingran.entity.JrBanner;
import com.dq.jingran.service.JrBannerService;
import com.dq.jingran.vo.R;
import com.dq.jingran.vo.banner.BannerParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * JrBanner-API
 * @author Dq
 * @since 2022-09-27 20:43:50
 */
@RestController
@RequestMapping("jrBanner")
@CrossOrigin
public class JrBannerController {
    @Autowired
    private JrBannerService jrBannerService;

    @PostMapping("addBanner")
    public R addBanner(@RequestBody BannerParam bannerParam){
        jrBannerService.addBanner(bannerParam);
        return R.success_Normal();
    }

    @GetMapping("getBanner")
    public R getBanner(){
        List<JrBanner> bannerList = jrBannerService.getBanner();
        return R.success_Normal().data("bannerList",bannerList);
    }


}