package com.dq.jingran.controller;

import com.dq.jingran.exception.ExceptionVo;
import com.dq.jingran.service.JrStoreService;
import com.dq.jingran.vo.R;
import com.dq.jingran.vo.store.DelStoreParams;
import com.dq.jingran.vo.store.StoreParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * JrStore-API 收藏
 *
 * @author Dq
 * @since 2022-10-15 18:52:21
 */
@RestController
@RequestMapping("store")
@CrossOrigin
public class JrStoreController {

    @Autowired
    private JrStoreService storeService;

    /**
     * 添加收藏
     *
     * @param storeParams
     * @return
     */
    @PostMapping("addStore")
    public R addStore(@RequestBody StoreParams storeParams) {
        if (StringUtils.isEmpty(storeParams.getUid()) || StringUtils.isEmpty(storeParams.getProId()) ||StringUtils.isEmpty(storeParams.getState())){
            throw new ExceptionVo(54188,"你妈了个逼，乱传数据？  能不能看清楚再传");
        }
        storeService.addStore(storeParams);
        return R.success_Normal();
    }

    /**
     * 获取所有list
     * @param userId
     * @param state
     * @return
     */
    @GetMapping("list/{uid}/{state}")
    public R storeList(@PathVariable("uid") String userId,@PathVariable("state") String state){
        Map<String,Object> result =storeService.listStore(userId,state);
        if (result==null){
            return R.success_Normal().message("亲~ 你还未在该分栏收藏任何商品哟!");
        }
        return R.success_Normal().data(result);
    }

    /**
     * 删除关系表
     * @param storeParams
     * @return
     */
    @PostMapping("delStore")
    public R delStore(@RequestBody DelStoreParams storeParams){
        storeService.delStore(storeParams);
        return R.success_Normal().message("删除成功");
    }

}