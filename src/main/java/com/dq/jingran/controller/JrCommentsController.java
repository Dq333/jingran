package com.dq.jingran.controller;

import com.dq.jingran.params.comments.CommentParam;
import com.dq.jingran.service.JrCommentsService;
import com.dq.jingran.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * JrComments-API
 * @author Dq
 * @since 2022-09-24 15:54:04
 */
@RestController
@RequestMapping("comments")
@CrossOrigin
public class JrCommentsController {
    @Autowired
    private JrCommentsService commentsService;

    /**
     * 添加评论
     * @param commentParam
     * @return
     */
    @PostMapping("addCommend")
    public R addCommend(@RequestBody CommentParam commentParam){
        commentsService.addComment(commentParam);
        return R.success_Normal().message("添加成功");
    }
}