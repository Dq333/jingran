package com.dq.jingran.controller;

import com.dq.jingran.entity.JrSpace;
import com.dq.jingran.service.JrSpaceService;
import com.dq.jingran.vo.R;
import com.dq.jingran.vo.space.SpaceAndCommendsVo;
import com.dq.jingran.vo.space.upfile.UpSpace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * JrSpace-API
 * @author Dq
 * @since 2022-09-24 15:51:47
 */
@RestController
@RequestMapping("space")
@CrossOrigin
public class JrSpaceController {

    @Autowired
    private JrSpaceService spaceService;

    /**
     * 获取空间和评论的数据
     * @return
     */
    @GetMapping("/getAll")
    public R getAll(){
        List<SpaceAndCommendsVo> spaceAndCommendsVoList =spaceService.getAllInfoAndCommend();
        return R.success_Normal().data("spaceAndCommendsList",spaceAndCommendsVoList);
    }

    /**
     * 发表文章
     * @return
     */
    @PostMapping("upSpace")
    public R upSpace(MultipartFile file, @RequestParam("userId") String userId,@RequestParam("reviews") String reviews) {
        if (StringUtils.isEmpty(userId) || file==null){
            return R.error("参数不能为空");
        }
        spaceService.upSpace(file,userId,reviews);
        return R.success_Normal();
    }
}