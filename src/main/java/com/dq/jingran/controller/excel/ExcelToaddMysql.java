package com.dq.jingran.controller.excel;

import com.dq.jingran.entity.JrFoods;
import com.dq.jingran.entity.JrRecreational;
import com.dq.jingran.entity.JrStrategy;
import com.dq.jingran.service.*;
import com.dq.jingran.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @project: jingran
 * @ClassName: ExcelToaddMysql
 * @author: dq
 * @creat: 2022/9/20 11:05
 */
@RestController
@RequestMapping("excel")
public class ExcelToaddMysql {
    //用于excel下级构造方法无法传递bean
    @Autowired
    private JrRecreationalService recreationalService;
    @Autowired
    private JrFoodsService foodsService;
    @Autowired
    private JrGuideService guideService;
    @Autowired
    private JrFestivalService festivalService;
    @Autowired
    private JrAccommodationService accommodationService;
    @Autowired
    private JrDetailsService detailsService;
    @Autowired
    private JrSceneryService jrSceneryService;
    @Autowired
    private JrRecreationalService jrRecreationalService;
    @Autowired
    private JrStrategyService jrStrategyService;
    @Autowired
    private JrFoodRelationService foodRelationService;

    @Autowired
    private ExcelApiService excelApiService;


    @PostMapping("addFoodRel")
    public R addFoodRel(MultipartFile file) {
        excelApiService.addFoodRel(file, foodRelationService);
        return R.success_Normal();
    }


    /**
     * 用于添加小景点接口
     *
     * @param file
     * @return
     */
    @PostMapping("addTileImg")
    public R addTileImg(MultipartFile file) {
        excelApiService.addTileImg(file, recreationalService);
        return R.success_Normal();
    }


    /**
     * 添加食物excel
     *
     * @param file
     * @return
     */
    @PostMapping("addFood")
    public R addFood(MultipartFile file) {
        excelApiService.addFood(file, foodsService);
        return R.success_Normal().message("添加成功");
    }

    /**
     * 添加节日
     * @param file
     * @return
     */
    @PostMapping("addFestival")
    public R addFestival(MultipartFile file) {
        excelApiService.addFestival(file, festivalService);
        return R.success_Normal().message("添加成功");
    }

    /**
     * 添加导游
     *
     * @param file
     * @return
     */
    @PostMapping("addGuide")
    public R addGuide(MultipartFile file) {
        excelApiService.addGuide(file, guideService);
        return R.success_Normal().message("添加成功");
    }


    @PostMapping("addHotel")
    public R addHotel(MultipartFile file) {
        excelApiService.addHotel(file, accommodationService, detailsService);
        return R.success_Normal().message("添加成功");
    }

    /**
     * 添加景点
     *
     * @param file
     * @return
     */
    @PostMapping("addScenery")
    public R addScenery(MultipartFile file) {
        excelApiService.addScenery(file, jrSceneryService);
        return R.success_Normal().message("添加成功");
    }

    /**
     * 添加景点下的轮播图
     * @param file
     * @return
     */
    @PostMapping("addRecreation")
    public R addRecreation(MultipartFile file) {
        excelApiService.addRecreation(file, jrRecreationalService);
        return R.success_Normal().message("添加成功");
    }


    @PostMapping("addStrategy")
    public R addStrategy(MultipartFile file) {
        excelApiService.addStrategy(file, jrStrategyService);
        return R.success_Normal().message("添加成功");
    }


}
