package com.dq.jingran.service.impl;

import com.dq.jingran.entity.JrGuide;
import com.dq.jingran.service.JrGuideService;
import com.dq.jingran.mapper.JrGuideMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * (JrGuide)表服务实现类
 *
 * @author Dq
 * @since 2022-09-22 19:19:18
 */
@Service
public class JrGuideServiceImpl  extends ServiceImpl<JrGuideMapper, JrGuide>  implements JrGuideService {
  
}