package com.dq.jingran.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.dq.jingran.entity.JrBanner;
import com.dq.jingran.exception.ExceptionVo;
import com.dq.jingran.service.JrBannerService;
import com.dq.jingran.mapper.JrBannerMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dq.jingran.vo.banner.BannerParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (JrBanner)表服务实现类
 *
 * @author Dq
 * @since 2022-09-27 20:43:50
 */
@Service
public class JrBannerServiceImpl  extends ServiceImpl<JrBannerMapper, JrBanner>  implements JrBannerService {

    @Autowired
    private JrBannerMapper jrBannerMapper;

    //插入banner
    @Override
    public void addBanner(BannerParam bannerParam) {
        JrBanner jrBanner = new JrBanner();
        if (bannerParam.getImg().isEmpty()){
            throw new ExceptionVo(20002,"图片不能为空");
        }
        jrBanner.setImg(bannerParam.getImg());
        if (bannerParam.getSort()==null){
            jrBanner.setSort(1);
        }else {
            jrBanner.setSort(bannerParam.getSort());
        }
        jrBannerMapper.insert(jrBanner);
    }


    //获取bannerList 集合
    @Override
    public List<JrBanner> getBanner() {
        LambdaQueryWrapper<JrBanner> jrBannerLambdaQueryWrapper = new LambdaQueryWrapper<>();
        jrBannerLambdaQueryWrapper.orderByAsc(JrBanner::getSort).last(" limit 8");
        List<JrBanner> bannerList = jrBannerMapper.selectList(jrBannerLambdaQueryWrapper);
        return bannerList;
    }
}