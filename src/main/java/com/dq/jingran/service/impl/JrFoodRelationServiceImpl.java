package com.dq.jingran.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.dq.jingran.entity.JrFoodRelation;
import com.dq.jingran.service.JrFoodRelationService;
import com.dq.jingran.mapper.JrFoodRelationMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * (JrFoodRelation)表服务实现类
 *
 * @author Dq
 * @since 2022-10-18 14:23:08
 */
@Service
public class JrFoodRelationServiceImpl  extends ServiceImpl<JrFoodRelationMapper, JrFoodRelation>  implements JrFoodRelationService {

    @Autowired
    private JrFoodRelationMapper foodRelationMapper;

    //查询所有
    @Override
    public List<String> selectOtherImg(String fId) {
        LambdaQueryWrapper<JrFoodRelation> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(JrFoodRelation::getFoodId,fId).select(JrFoodRelation::getImg);
        List<JrFoodRelation> jrFoodRelations = foodRelationMapper.selectList(queryWrapper);
        ArrayList<String> arrayList = new ArrayList<>();
        for (JrFoodRelation jrFoodRelation : jrFoodRelations) {
            arrayList.add(jrFoodRelation.getImg());
        }
        return arrayList;
    }
}