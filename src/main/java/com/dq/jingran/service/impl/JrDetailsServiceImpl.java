package com.dq.jingran.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.dq.jingran.entity.JrDetails;
import com.dq.jingran.service.JrDetailsService;
import com.dq.jingran.mapper.JrDetailsMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (JrDetails)表服务实现类
 *
 * @author Dq
 * @since 2022-09-26 20:56:14
 */
@Service
public class JrDetailsServiceImpl  extends ServiceImpl<JrDetailsMapper, JrDetails>  implements JrDetailsService {

    @Autowired
    private JrDetailsMapper detailsMapper;

    //根据父类查询所有图片
    @Override
    public List<JrDetails> getByAccommodationId(String accommodationId) {
        LambdaQueryWrapper<JrDetails> jrDetailsLambdaQueryWrapper = new LambdaQueryWrapper<>();
        jrDetailsLambdaQueryWrapper.eq(JrDetails::getAccommodationId,accommodationId);
        List<JrDetails> jrDetails = detailsMapper.selectList(jrDetailsLambdaQueryWrapper);
        return jrDetails;
    }
}