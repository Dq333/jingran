package com.dq.jingran.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.dq.jingran.entity.JrRecreational;
import com.dq.jingran.service.JrRecreationalService;
import com.dq.jingran.mapper.JrRecreationalMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (JrRecreational)表服务实现类
 *
 * @author Dq
 * @since 2022-09-30 14:32:41
 */
@Service
public class JrRecreationalServiceImpl extends ServiceImpl<JrRecreationalMapper, JrRecreational> implements JrRecreationalService {

    @Autowired
    private JrRecreationalMapper jrRecreationalMapper;

    //传入景点id 获取所有轮播图
    @Override
    public List<JrRecreational> getByParentId(String id) {
        LambdaQueryWrapper<JrRecreational> jrRecreationalLambdaQueryWrapper = new LambdaQueryWrapper<>();
        jrRecreationalLambdaQueryWrapper.eq(JrRecreational::getSeneryId, id);
        List<JrRecreational> recreationals = jrRecreationalMapper.selectList(jrRecreationalLambdaQueryWrapper);
        return recreationals;
    }
}