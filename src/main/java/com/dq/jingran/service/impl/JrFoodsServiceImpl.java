package com.dq.jingran.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dq.jingran.entity.JrFoods;
import com.dq.jingran.exception.ExceptionVo;
import com.dq.jingran.service.JrFoodRelationService;
import com.dq.jingran.service.JrFoodsService;
import com.dq.jingran.mapper.JrFoodsMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dq.jingran.service.JrStoreService;
import com.dq.jingran.utils.MybatisUtils;
import com.dq.jingran.vo.food.FoodVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (JrFoods)表服务实现类
 *
 * @author Dq
 * @since 2022-09-27 18:13:54
 */
@Service
public class JrFoodsServiceImpl extends ServiceImpl<JrFoodsMapper, JrFoods> implements JrFoodsService {
    @Autowired
    private JrFoodsMapper jrFoodsMapper;
    @Autowired
    private JrStoreService storeService;
    @Autowired
    private JrFoodRelationService foodRelationService;

    @Override
    public List<JrFoods> getAllInfo(String state) {
        /**
         * 0 查询水果
         * 1 查询食物
         */
        switch (state) {
            case "0": {
                LambdaQueryWrapper<JrFoods> jrFoodsLambdaQueryWrapper = new LambdaQueryWrapper<>();
                jrFoodsLambdaQueryWrapper.eq(JrFoods::getState, "0")
                        .select(JrFoods::getId,
                                JrFoods::getTitle,
                                JrFoods::getScore,
                                JrFoods::getCommendPerson,
                                JrFoods::getPrice,
                                JrFoods::getFeatures,
                                JrFoods::getImg,
                                JrFoods::getCommand);
                List<JrFoods> foodsList = jrFoodsMapper.selectList(jrFoodsLambdaQueryWrapper);
                return foodsList;
            }
            case "1": {
                LambdaQueryWrapper<JrFoods> jrFoodsLambdaQueryWrapper = new LambdaQueryWrapper<>();
                jrFoodsLambdaQueryWrapper.eq(JrFoods::getState, "1")
                        .select(JrFoods::getId,
                                JrFoods::getTitle,
                                JrFoods::getScore,
                                JrFoods::getCommendPerson,
                                JrFoods::getPrice,
                                JrFoods::getFeatures,
                                JrFoods::getWorkTime,
                                JrFoods::getImg);
                List<JrFoods> foodsList = jrFoodsMapper.selectList(jrFoodsLambdaQueryWrapper);
                return foodsList;

            }
            default:
                throw new ExceptionVo(20002, "索引越界 请检查你的state状态码");
        }
    }

    //随机获取食物
    @Override
    public List<JrFoods> getLimitData() {
        LambdaQueryWrapper<JrFoods> jrFoodsLambdaQueryWrapper = new LambdaQueryWrapper<JrFoods>();
        jrFoodsLambdaQueryWrapper.eq(JrFoods::getState, "1");
        List<JrFoods> foodsList = MybatisUtils.getAny(jrFoodsMapper, jrFoodsLambdaQueryWrapper, 6);
        return foodsList;
    }

    //根据id查询食物
    @Override
    public List<JrFoods> findListByUserFavority(List<String> objectIds) {
        List<JrFoods> foodsList = jrFoodsMapper.selectBatchIds(objectIds);
        return foodsList;
    }

    //获取食物的详情
    @Override
    public FoodVo getFoodDetail(String fId, String uid) {

        JrFoods food = jrFoodsMapper.selectById(fId);

        Boolean isLike = storeService.judgeIsLike(uid, fId);
        List<String> otherImg=foodRelationService.selectOtherImg(fId);
        FoodVo foodApi = copy(food);
        foodApi.setIsLike(isLike);
        foodApi.setOtherImg(otherImg);
        return foodApi;
    }

    public FoodVo copy(JrFoods food) {
        FoodVo foodVo = new FoodVo();
        BeanUtils.copyProperties(food, foodVo);
        return foodVo;
    }
}