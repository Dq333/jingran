package com.dq.jingran.service.impl;

import com.dq.jingran.entity.JrFestival;
import com.dq.jingran.service.JrFestivalService;
import com.dq.jingran.mapper.JrFestivalMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (JrFestival)表服务实现类
 *
 * @author Dq
 * @since 2022-09-22 19:19:06
 */
@Service
public class JrFestivalServiceImpl  extends ServiceImpl<JrFestivalMapper, JrFestival>  implements JrFestivalService {

    @Autowired
    private JrFestivalMapper festivalMapper;

    //查询所有节日数据
    @Override
    public List<JrFestival> getAllFestivalData() {
        List<JrFestival> festivalList = festivalMapper.selectList(null);
        return festivalList;
    }
}