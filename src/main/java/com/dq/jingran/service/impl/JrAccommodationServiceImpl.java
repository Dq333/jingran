package com.dq.jingran.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.dq.jingran.entity.JrAccommodation;
import com.dq.jingran.entity.JrDetails;
import com.dq.jingran.exception.ExceptionVo;
import com.dq.jingran.service.JrAccommodationService;
import com.dq.jingran.mapper.JrAccommodationMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dq.jingran.service.JrDetailsService;
import com.dq.jingran.service.JrStoreService;
import com.dq.jingran.vo.accommodation.AccommodationVo;
import com.dq.jingran.vo.accommodation.PublishAccAndDetailVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * (JrAccommodation)表服务实现类
 *
 * @author Dq
 * @since 2022-09-26 20:55:54
 */
@Service
public class JrAccommodationServiceImpl  extends ServiceImpl<JrAccommodationMapper, JrAccommodation>  implements JrAccommodationService {

    @Autowired
    private JrAccommodationMapper accommodationMapper;
    @Autowired
    private JrDetailsService detailsService;
    @Autowired
    private JrStoreService storeService;

    //获取简单的住宿信息
    @Override
    public List<AccommodationVo> getBasicAccommodationInfo() {
        /**
         * 根据一个住宿索引出他的小图片
         */
        LambdaQueryWrapper<JrAccommodation> jrAccommodationLambdaQueryWrapper = new LambdaQueryWrapper<>();
        jrAccommodationLambdaQueryWrapper.select(
                JrAccommodation::getId,
                JrAccommodation::getTitle,
                JrAccommodation::getAddress,
                JrAccommodation::getAdvantage,
                JrAccommodation::getAccommodationImg,
                JrAccommodation::getAccommodationScore,
                JrAccommodation::getPrice,
                JrAccommodation::getCollection
                );
        List<JrAccommodation> jrAccommodations = accommodationMapper.selectList(jrAccommodationLambdaQueryWrapper);
        List<AccommodationVo> copyList = copyList(jrAccommodations);
        return copyList;
    }

    //根据id获取住宿的详细信息
    @Override
    public PublishAccAndDetailVo getDetailAccommodationInfoById(String accommodationId,String uid) {
        Boolean isLike = storeService.judgeIsLike(uid, accommodationId);
        JrAccommodation jrAccommodation = accommodationMapper.selectById(accommodationId);
        List<JrDetails> details = detailsService.getByAccommodationId(accommodationId);
        PublishAccAndDetailVo publishAccAndDetailVo = new PublishAccAndDetailVo();
        publishAccAndDetailVo.setAccommodation(jrAccommodation);
        publishAccAndDetailVo.setDetails(details);
        publishAccAndDetailVo.setIsLike(isLike);
        return publishAccAndDetailVo;
    }

    //查询所有收藏
    @Override
    public List<AccommodationVo> findListByUserFavority(List<String> objectIds) {
        LambdaQueryWrapper<JrAccommodation> jrAccommodationQueryWrapper = new LambdaQueryWrapper<>();
        List<JrAccommodation> jrAccommodations = accommodationMapper.selectBatchIds(objectIds);
        List<AccommodationVo> copyList = copyList(jrAccommodations);
        return copyList;
    }

    //List复制
    private List<AccommodationVo> copyList(List<JrAccommodation> jrAccommodationList){
        ArrayList<AccommodationVo> accommodationVos = new ArrayList<>();
        for (JrAccommodation jrAccommodation : jrAccommodationList) {
            accommodationVos.add(copy(jrAccommodation));
        }
        return accommodationVos;
    }

    //单一复制
    private AccommodationVo copy(JrAccommodation jrAccommodation) {
        AccommodationVo accommodationVo = new AccommodationVo();
        BeanUtils.copyProperties(jrAccommodation,accommodationVo);
        return accommodationVo;
    }


}