package com.dq.jingran.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dq.jingran.entity.JrRecreational;
import com.dq.jingran.entity.JrScenery;
import com.dq.jingran.entity.JrUser;
import com.dq.jingran.service.JrRecreationalService;
import com.dq.jingran.service.JrSceneryService;
import com.dq.jingran.mapper.JrSceneryMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dq.jingran.service.JrStoreService;
import com.dq.jingran.utils.MybatisUtils;
import com.dq.jingran.utils.UserThreadLocal;
import com.dq.jingran.vo.scenery.JrSceneryVo;
import com.dq.jingran.vo.scenery.SceneryVo;
import com.dq.jingran.vo.scenery.SearchSceneryParams;
import com.dq.jingran.vo.store.StoreParams;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * (JrScenery)表服务实现类
 *
 * @author Dq
 * @since 2022-09-27 17:52:14
 */
@Service
public class JrSceneryServiceImpl extends ServiceImpl<JrSceneryMapper, JrScenery> implements JrSceneryService {

    @Autowired
    private JrSceneryMapper jrSceneryMapper;
    @Autowired
    private JrRecreationalService recreationalService;
    @Autowired
    private JrStoreService storeService;

    //获取景点的基本信息
    @Override
    public List<SceneryVo> getBasicInfo() {
        LambdaQueryWrapper<JrScenery> jrSceneryLambdaQueryWrapper = new LambdaQueryWrapper<>();
        jrSceneryLambdaQueryWrapper
                .select(JrScenery::getId,
                        JrScenery::getTitle,
                        JrScenery::getImg,
                        JrScenery::getPrice,
                        JrScenery::getLocation,
                        JrScenery::getViewCount,
                        JrScenery::getCategory,
                        JrScenery::getScore
                );
        List<JrScenery> sceneryList = jrSceneryMapper.selectList(jrSceneryLambdaQueryWrapper);
        List<SceneryVo> sceneryVos = copyList(sceneryList);
        return sceneryVos;
    }

    //随机获取8个景点信息
    @Override
    public List<SceneryVo> getPartInfo() {
        Integer count = jrSceneryMapper.selectCount(null);
        // 随机数起始位置
        int randomCount = (int) (Math.random() * count);
        // 保证能展示8个数据
        if (randomCount > ((count - 8>0? count-8:0))) {
            randomCount = (count - 8>0? count-8:0);
        }
        LambdaQueryWrapper<JrScenery> jrSceneryLambdaQueryWrapper = new LambdaQueryWrapper<>();
        jrSceneryLambdaQueryWrapper
                .select(JrScenery::getId,
                        JrScenery::getTitle,
                        JrScenery::getImg,
                        JrScenery::getPrice,
                        JrScenery::getLocation,
                        JrScenery::getViewCount,
                        JrScenery::getCategory,
                        JrScenery::getScore
                ).last("limit " + String.valueOf(randomCount) + ", 8");
        List<JrScenery> sceneryList = jrSceneryMapper.selectList(jrSceneryLambdaQueryWrapper);
        List<SceneryVo> sceneryVoList = copyList(sceneryList);
        return sceneryVoList;
    }

    //获取景点和他的轮播图
    @Override
    public JrSceneryVo getByAllById(String pid,String userId) {
        /**
         * 1. 查询是否喜欢
         * 2. 查询其他数据
         */
        Boolean state = storeService.judgeIsLike(userId,pid);

        JrScenery jrScenery = jrSceneryMapper.selectById(pid);
        List<JrRecreational> recreationalList=recreationalService.getByParentId(pid);
        JrSceneryVo jrSceneryVo = new JrSceneryVo();
        jrSceneryVo.setScenery(jrScenery);
        jrSceneryVo.setRecreational(recreationalList);
        jrSceneryVo.setIsLike(state);
        return jrSceneryVo;
    }

    //查询收藏列表
    @Override
    public List<SceneryVo> findListByUserFavority(List<String> objectIds) {
        List<JrScenery> sceneryList = jrSceneryMapper.selectBatchIds(objectIds);
        List<SceneryVo> sceneryVos = copyList(sceneryList);

        return sceneryVos;
    }

    //模糊查询数据
    @Override
    public List<SceneryVo> searchItems(SearchSceneryParams sceneryParams) {
        //根据标题
        LambdaQueryWrapper<JrScenery> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(JrScenery::getTitle,sceneryParams.getFuzzyName());
        List<JrScenery> sceneryList = jrSceneryMapper.selectList(queryWrapper);
        List<SceneryVo> sceneryVos = copyList(sceneryList);
        //根据地区
        LambdaQueryWrapper<JrScenery> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(JrScenery::getLocation,sceneryParams.getFuzzyName());
        List<JrScenery> jrSceneries = jrSceneryMapper.selectList(wrapper);
        List<SceneryVo> sceneryPlace = copyList(jrSceneries);
        //统一判断
        if (sceneryVos.size()==0 && sceneryPlace.size()==0){
            return null;
        }
        List<SceneryVo> collect = Stream
                .of(sceneryVos, sceneryPlace)
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());
        return collect;
    }


    private List<SceneryVo> copyList(List<JrScenery> sceneryList) {
        ArrayList<SceneryVo> sceneryVos = new ArrayList<>();
        for (JrScenery jrScenery : sceneryList) {
            sceneryVos.add(copy(jrScenery));
        }
        return sceneryVos;
    }

    private SceneryVo copy(JrScenery jrScenery) {
        SceneryVo sceneryVo = new SceneryVo();
        BeanUtils.copyProperties(jrScenery, sceneryVo);
        return sceneryVo;
    }
}