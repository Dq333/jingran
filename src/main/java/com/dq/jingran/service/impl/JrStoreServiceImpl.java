package com.dq.jingran.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.dq.jingran.entity.JrAccommodation;
import com.dq.jingran.entity.JrFoods;
import com.dq.jingran.entity.JrScenery;
import com.dq.jingran.entity.JrStore;
import com.dq.jingran.exception.ExceptionVo;
import com.dq.jingran.service.JrAccommodationService;
import com.dq.jingran.service.JrFoodsService;
import com.dq.jingran.service.JrSceneryService;
import com.dq.jingran.service.JrStoreService;
import com.dq.jingran.mapper.JrStoreMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dq.jingran.vo.accommodation.AccommodationVo;
import com.dq.jingran.vo.scenery.SceneryVo;
import com.dq.jingran.vo.store.DelStoreParams;
import com.dq.jingran.vo.store.StoreParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (JrStore)表服务实现类
 *
 * @author Dq
 * @since 2022-10-15 18:52:21
 */
@Service
public class JrStoreServiceImpl extends ServiceImpl<JrStoreMapper, JrStore> implements JrStoreService {
    @Autowired
    private JrStoreMapper storeMapper;
    @Autowired
    private JrSceneryService sceneryService;
    @Autowired
    private JrFoodsService foodsService;
    @Autowired
    private JrAccommodationService accommodationService;

    //用户添加搜藏
    @Override
    public void addStore(StoreParams storeParams) {

        LambdaQueryWrapper<JrStore> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(JrStore::getUid,storeParams.getUid())
                .eq(JrStore::getProId,storeParams.getProId())
                .eq(JrStore::getState,storeParams.getState());
        Integer selectCount = storeMapper.selectCount(queryWrapper);
        if (selectCount>=1){
            throw new ExceptionVo(54188,"此用户早TM存了这个商品了 拜托别乱整好吧？");
        }
        JrStore jrStore = new JrStore();
        jrStore.setUid(storeParams.getUid());
        jrStore.setState(storeParams.getState());
        jrStore.setProId(storeParams.getProId());

        storeMapper.insert(jrStore);
    }

    //获取一个状态的所有收藏
    @Override
    public Map<String, Object> listStore(String userId, String state) {
        Map<String, Object> map=null; //扩大map作用域
        LambdaQueryWrapper<JrStore> jrStoreLambdaQueryWrapper = new LambdaQueryWrapper<>();
        jrStoreLambdaQueryWrapper.eq(JrStore::getUid,userId).eq(JrStore::getState,state).select(JrStore::getProId);
        List<JrStore> jrStores = storeMapper.selectList(jrStoreLambdaQueryWrapper);
        if (jrStores.size()==0){
            return map;
        }
        List<String> arrayList = new ArrayList<>();
        for (JrStore jrStore : jrStores) {
            arrayList.add(jrStore.getProId());
        }
        map = copyList(arrayList, state);

        return map;
    }

    //判断用户isLike 某个产品的状态
    @Override
    public Boolean judgeIsLike(String uid, String pid) {
        LambdaQueryWrapper<JrStore> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(JrStore::getUid,uid).eq(JrStore::getProId,pid).last(" limit 1");
        Integer selectCount = storeMapper.selectCount(queryWrapper);
        if (selectCount==1){
            return true;
        }
        return false;
    }

    //删除关系表
    @Override
    public void delStore(DelStoreParams storeParams) {
        LambdaQueryWrapper<JrStore> jrStoreLambdaQueryWrapper = new LambdaQueryWrapper<>();
        jrStoreLambdaQueryWrapper
                .eq(JrStore::getUid,storeParams.getUid())
                .eq(JrStore::getProId,storeParams.getProId());
        Integer selectCount = storeMapper.selectCount(jrStoreLambdaQueryWrapper);
        LambdaQueryWrapper<JrStore> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .eq(JrStore::getUid,storeParams.getUid())
                .eq(JrStore::getProId,storeParams.getProId());
        if (selectCount==1){
            storeMapper.delete(queryWrapper);
        }else {
            throw new ExceptionVo(403,"删除失败，查询匹配关系Error");
        }
    }


    //重载封装
    public Map<String,Object> copyList(List<String> objectIds  , String state){
        Map<String, Object> map = new HashMap<>();
        if (state.equals("1")){ //美食
           List<JrFoods> foods =foodsService.findListByUserFavority(objectIds);
           map.put("foods",foods);
        }
        if (state.equals("2")){ //酒店
            List<AccommodationVo> accommodations  =  accommodationService.findListByUserFavority(objectIds);
            map.put("accommodations",accommodations);
        }
        if (state.equals("3")){ //风景
            List<SceneryVo> sceneryList  =  sceneryService.findListByUserFavority(objectIds);
            map.put("scenerys",sceneryList);
        }
        return map;
    }

}