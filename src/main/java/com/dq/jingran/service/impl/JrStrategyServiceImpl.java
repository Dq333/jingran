package com.dq.jingran.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.dq.jingran.entity.JrStrategy;
import com.dq.jingran.service.JrStrategyService;
import com.dq.jingran.mapper.JrStrategyMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dq.jingran.vo.strategy.StrategyListVo;
import com.dq.jingran.vo.strategy.StrategyVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * (JrStrategy)表服务实现类
 *
 * @author Dq
 * @since 2022-09-30 17:38:05
 */
@Service
public class JrStrategyServiceImpl  extends ServiceImpl<JrStrategyMapper, JrStrategy>  implements JrStrategyService {

    @Autowired
    private JrStrategyMapper jrStrategyMapper;

    //简单查询
    @Override
    public List<StrategyListVo> getDateList() {
        LambdaQueryWrapper<JrStrategy> jrStrategyLambdaQueryWrapper = new LambdaQueryWrapper<>();
        jrStrategyLambdaQueryWrapper.select(JrStrategy::getId,
                JrStrategy::getTitle,
                JrStrategy::getNickname,
                JrStrategy::getUseravatar,
                JrStrategy::getImg1,
                JrStrategy::getImg2,
                JrStrategy::getImg3
                );
        List<JrStrategy> jrStrategies = jrStrategyMapper.selectList(jrStrategyLambdaQueryWrapper);
        List<StrategyListVo> strategyListVos = copyList(jrStrategies);
        return strategyListVos;
    }

    private List<StrategyListVo> copyList(List<JrStrategy> jrStrategies){
        List<StrategyListVo> strategyListVos = new ArrayList<>();
        for (JrStrategy jrStrategy : jrStrategies) {
            strategyListVos.add(copy(jrStrategy));
        }
        return strategyListVos;
    }

    private StrategyListVo copy(JrStrategy jrStrategy) {
        StrategyListVo strategyListVo = new StrategyListVo();
        BeanUtils.copyProperties(jrStrategy,strategyListVo);
        return strategyListVo;
    }
}