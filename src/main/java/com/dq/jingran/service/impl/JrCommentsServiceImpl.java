package com.dq.jingran.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.dq.jingran.entity.JrComments;
import com.dq.jingran.entity.JrUser;
import com.dq.jingran.exception.ExceptionVo;
import com.dq.jingran.params.comments.CommentParam;
import com.dq.jingran.service.JrCommentsService;
import com.dq.jingran.mapper.JrCommentsMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dq.jingran.service.JrUserService;
import com.dq.jingran.vo.space.SpaceCommentsVo;
import com.dq.jingran.vo.space.SpaceUserVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * (JrComments)表服务实现类
 *
 * @author Dq
 * @since 2022-09-24 15:54:04
 */
@Service
public class JrCommentsServiceImpl  extends ServiceImpl<JrCommentsMapper, JrComments>  implements JrCommentsService {

    @Autowired
    private JrCommentsMapper commentsMapper;

    @Autowired
    private JrUserService userService;

    //根据空间id 查询详细的评论信息
    @Override
    public List<SpaceCommentsVo> findAllDataBySpaceId(String id) {
        /**
         * 1. 获取父类id
         * 2. 查询下面的所有子集评论
         * 3. 然后根据子集评论封装Vo对象
         * 4. 在封装Vo对象的同时查询User 然后封装
         */
        LambdaQueryWrapper<JrComments> jrCommentsLambdaQueryWrapper = new LambdaQueryWrapper<>();
        jrCommentsLambdaQueryWrapper.eq(JrComments::getSpaceId,id);
        List<JrComments> commentsList = commentsMapper.selectList(jrCommentsLambdaQueryWrapper);
        List<SpaceCommentsVo> spaceCommentsVos = copyList(commentsList);
        return spaceCommentsVos;
    }

    //添加评论
    @Override
    public void addComment(CommentParam commentParam) {
        if (StringUtils.isEmpty(commentParam.getUserId()) || StringUtils.isEmpty(commentParam.getSpaceId())){
            throw new ExceptionVo(20002,"传入的用户ID或空间ID为空");
        }
        JrComments jrComments = new JrComments();
        jrComments.setUserId(commentParam.getUserId());
        jrComments.setSpaceId(commentParam.getSpaceId());
        jrComments.setReviews(commentParam.getReviews());

        commentsMapper.insert(jrComments);
    }

    //查询所有user 封装为like
    @Override
    public List<SpaceUserVo> findAllLike(String id) {
        LambdaQueryWrapper<JrComments> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(JrComments::getSpaceId,id).select(JrComments::getUserId);
        List<JrComments> commentsList = commentsMapper.selectList(queryWrapper);
        ArrayList<SpaceUserVo> likes = new ArrayList<>();
        for (JrComments jrComments : commentsList) {
            String userId = jrComments.getUserId();
            LambdaQueryWrapper<JrUser> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(JrUser::getId,userId).select(JrUser::getId,JrUser::getUsername);
            JrUser user = userService.getOne(wrapper);
            SpaceUserVo userVo = copyUser(user);
            likes.add(userVo);
        }
        return likes;
    }

    //整体封装Vo集合
    public List<SpaceCommentsVo> copyList(List<JrComments> commentsList){
        ArrayList<SpaceCommentsVo> spaceCommentsVos = new ArrayList<SpaceCommentsVo>();
        for (JrComments jrComments : commentsList) {

            spaceCommentsVos.add(copy(jrComments));
        }
        return spaceCommentsVos;
    }
    //单独判断其他数据然后调用其他方法
    public SpaceCommentsVo copy(JrComments jrComments) {
        SpaceCommentsVo spaceCommentsVo = new SpaceCommentsVo();
        BeanUtils.copyProperties(jrComments,spaceCommentsVo);
        if (jrComments.getId()!=null){
            SpaceUserVo userVo =userService.getSomeInfoToSpace(jrComments.getUserId());
            spaceCommentsVo.setUser(userVo);
        }
        return spaceCommentsVo;
    }


    //============================封装User对象
    public List<SpaceUserVo> copyListUser(List<JrUser> userList){
        ArrayList<SpaceUserVo> spaceUserVos = new ArrayList<>();
        for (JrUser jrUser : userList) {
            spaceUserVos.add(copyUser(jrUser));
        }
        return spaceUserVos;
    }

    private SpaceUserVo copyUser(JrUser jrUser) {
        SpaceUserVo userVo = new SpaceUserVo();
        BeanUtils.copyProperties(jrUser,userVo);
        return userVo;
    }

}