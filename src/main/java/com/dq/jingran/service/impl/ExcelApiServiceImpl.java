package com.dq.jingran.service.impl;

import com.alibaba.excel.EasyExcel;
import com.dq.jingran.entity.excel.*;
import com.dq.jingran.listener.*;
import com.dq.jingran.service.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

/**
 * @project: jingran
 * @ClassName: ExcelApiServiceImpl
 * @author: dq
 * @creat: 2022/9/20 11:08
 */
@Service
public class ExcelApiServiceImpl implements ExcelApiService {
    //利用poi 技术使云端数据导入mysql
    @Override
    public void addTileImg(MultipartFile file, JrRecreationalService jrRecreationalService) {
        try {
            InputStream fileInputStream = file.getInputStream();
            // EasyExcel.read(fileInputStream, TitleImg.class,new ExcelJrRecreationalListener(jrRecreationalService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //水果的添加
    @Override
    public void addFood(MultipartFile file, JrFoodsService foodsService) {
        try {
            InputStream fileInputStream = file.getInputStream();
            EasyExcel.read(fileInputStream, FoodExcel.class,new ExcelFoodListener(foodsService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //导游的添加
    @Override
    public void addGuide(MultipartFile file, JrGuideService guideService) {
        try {
            InputStream fileInputStream = file.getInputStream();
            EasyExcel.read(fileInputStream, GuiderExcel.class,new ExcelGuiderListener(guideService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //节日的添加
    @Override
    public void addFestival(MultipartFile file, JrFestivalService festivalService) {
        try {
            InputStream fileInputStream = file.getInputStream();
            EasyExcel.read(fileInputStream, FestivalExcel.class,new ExcelFestivalListener(festivalService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addHotel(MultipartFile file, JrAccommodationService accommodationService, JrDetailsService detailsService) {
        try {
            InputStream fileInputStream = file.getInputStream();
            EasyExcel.read(fileInputStream, AccommodationExcel.class,new ExcelAccommodationListener(accommodationService,detailsService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addScenery(MultipartFile file, JrSceneryService jrSceneryService) {
        try {
            InputStream fileInputStream = file.getInputStream();
            EasyExcel.read(fileInputStream, SceneryExcel.class,new ExcelSceneryListener(jrSceneryService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addRecreation(MultipartFile file, JrRecreationalService jrRecreationalService) {
        try {
            InputStream fileInputStream = file.getInputStream();
            EasyExcel.read(fileInputStream, RecreationalExcel.class,new ExcelRecreationalListener(jrRecreationalService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addStrategy(MultipartFile file, JrStrategyService jrStrategyService) {
        try {
            InputStream fileInputStream = file.getInputStream();
            EasyExcel.read(fileInputStream, StrategyExcel.class,new ExcelStrategyListener(jrStrategyService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addFoodRel(MultipartFile file, JrFoodRelationService foodRelationService) {
        try {
            InputStream fileInputStream = file.getInputStream();
            EasyExcel.read(fileInputStream, FoodReExcel.class,new ExcelFoodRelListener(foodRelationService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
