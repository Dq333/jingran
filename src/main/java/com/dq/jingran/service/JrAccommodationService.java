package com.dq.jingran.service;

import com.dq.jingran.entity.JrAccommodation;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dq.jingran.vo.accommodation.AccommodationVo;
import com.dq.jingran.vo.accommodation.PublishAccAndDetailVo;

import java.util.List;

/**
 * (JrAccommodation)表服务接口
 *
 * @author Dq
 * @since 2022-09-26 20:55:54
 */
public interface JrAccommodationService  extends IService<JrAccommodation> {

    //获取简单的住宿信息
    List<AccommodationVo> getBasicAccommodationInfo();

    //根据id获取下面的详细信息
    PublishAccAndDetailVo getDetailAccommodationInfoById(String accommodationId,String uid);

    List<AccommodationVo> findListByUserFavority(List<String> objectIds);
}