package com.dq.jingran.service;

import com.dq.jingran.entity.JrStore;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dq.jingran.vo.store.DelStoreParams;
import com.dq.jingran.vo.store.StoreParams;

import java.util.Map;

/**
 * (JrStore)表服务接口
 *
 * @author Dq
 * @since 2022-10-15 18:52:21
 */
public interface JrStoreService  extends IService<JrStore> {

    //添加收藏
    void addStore(StoreParams storeParams);


    //获取列表
    Map<String, Object> listStore(String userId, String state);

    //判断用户是否收藏了此商品
    Boolean judgeIsLike(String pid, String uid);

    //删除关系表
    void delStore(DelStoreParams storeParams);
}