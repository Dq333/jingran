package com.dq.jingran.service;

import com.dq.jingran.entity.JrRecreational;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * (JrRecreational)表服务接口
 *
 * @author Dq
 * @since 2022-09-30 14:32:40
 */
public interface JrRecreationalService  extends IService<JrRecreational> {

    //根据父类id 获取img
    List<JrRecreational> getByParentId(String id);
}