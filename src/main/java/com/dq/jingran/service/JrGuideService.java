package com.dq.jingran.service;

import com.dq.jingran.entity.JrGuide;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * (JrGuide)表服务接口
 *
 * @author Dq
 * @since 2022-09-22 19:19:18
 */
public interface JrGuideService  extends IService<JrGuide> {

}