package com.dq.jingran.service;

import com.dq.jingran.entity.JrDetails;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * (JrDetails)表服务接口
 *
 * @author Dq
 * @since 2022-09-26 20:56:14
 */
public interface JrDetailsService  extends IService<JrDetails> {

    //根据住宿id查询相应的图片
    List<JrDetails> getByAccommodationId(String accommodationId);
}