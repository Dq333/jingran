package com.dq.jingran.service;

import com.dq.jingran.entity.JrFoods;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dq.jingran.vo.food.FoodVo;

import java.util.List;

/**
 * (JrFoods)表服务接口
 *
 * @author Dq
 * @since 2022-09-27 18:13:53
 */
public interface JrFoodsService  extends IService<JrFoods> {

    //获取食物的信息用于显示
    List<JrFoods> getAllInfo(String state);

    //随机获取
    List<JrFoods> getLimitData();

    //查询单独收藏页面
    List<JrFoods> findListByUserFavority(List<String> objectIds);

    //查询食物的详细界面
    FoodVo getFoodDetail(String fId, String uid);
}