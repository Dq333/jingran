package com.dq.jingran.service;

import com.dq.jingran.entity.JrUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dq.jingran.vo.space.SpaceUserVo;
import com.dq.jingran.vo.user.ChangeUserNameParams;
import com.dq.jingran.vo.user.UpdatePassParams;
import com.dq.jingran.vo.user.UserParams;
import org.springframework.web.multipart.MultipartFile;

/**
 * (JrUser)表服务接口
 *
 * @author Dq
 * @since 2022-09-19 17:44:03
 */
public interface JrUserService  extends IService<JrUser> {

    /**
     * 注册用户
     * @param user
     * @return
     */
    String registerUser(UserParams user);

    /**
     * 用户登录
     * @param userParams
     * @return
     */
    String login(UserParams userParams);

    /**
     * 获取用户数据
     * @param token
     * @return
     */
    JrUser checkTokenAndReturnData(String token);

    /**
     * 被空间所调用的方法然后封装Vo对象
     * @param userId
     * @return
     */
    SpaceUserVo getSomeInfoToSpace(String userId);

    /**
     * 修改用户密码
     * @param userParams
     */
    void updatePass(UpdatePassParams userParams);

    /**
     * 上传用户的头像 和昵称
     * @param auth
     * @param file
     * @return
     */
    String upImgToTencentOss(String auth, MultipartFile file);

    /**
     * 修改
     * @param userParams
     */
    void changeName(ChangeUserNameParams userParams);

    /**
     * 获取用户
     * @param token
     * @return
     */
    JrUser getNew(String token);
}