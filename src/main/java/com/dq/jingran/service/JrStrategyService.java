package com.dq.jingran.service;

import com.dq.jingran.entity.JrStrategy;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dq.jingran.vo.strategy.StrategyListVo;
import com.dq.jingran.vo.strategy.StrategyVo;

import java.util.List;

/**
 * (JrStrategy)表服务接口
 *
 * @author Dq
 * @since 2022-09-30 17:38:05
 */
public interface JrStrategyService  extends IService<JrStrategy> {

    /**
     * 获取攻略的简单信息
     * @return
     */
    List<StrategyListVo> getDateList();
}