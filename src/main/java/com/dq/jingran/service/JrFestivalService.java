package com.dq.jingran.service;

import com.dq.jingran.entity.JrFestival;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * (JrFestival)表服务接口
 *
 * @author Dq
 * @since 2022-09-22 19:19:06
 */
public interface JrFestivalService  extends IService<JrFestival> {

    /**
     * 获取节日的所有数据
     * @return
     */
    List<JrFestival> getAllFestivalData();
}