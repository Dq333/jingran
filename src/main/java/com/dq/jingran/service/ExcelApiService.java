package com.dq.jingran.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @project: jingran
 * @ClassName: ExcelApi
 * @author: dq
 * @creat: 2022/9/20 11:08
 */
public interface ExcelApiService {
    /**
     * 把图片url传入数据库
     * @param file
     */
    void addTileImg(MultipartFile file,JrRecreationalService jrRecreationalService);

    /**
     * 把食物转化存入excel
     * @param file
     * @param foodsService
     */
    void addFood(MultipartFile file, JrFoodsService foodsService);

    //excel添加导游
    void addGuide(MultipartFile file, JrGuideService guideService);

    //添加西昌特殊节日
    void addFestival(MultipartFile file, JrFestivalService festivalService);

    //添加住宿
    void addHotel(MultipartFile file, JrAccommodationService accommodationService, JrDetailsService detailsService);

    //添加景点
    void addScenery(MultipartFile file, JrSceneryService jrSceneryService);

    //添加轮播图
    void addRecreation(MultipartFile file, JrRecreationalService jrRecreationalService);

    //添加攻略
    void addStrategy(MultipartFile file, JrStrategyService jrStrategyService);

    //添加食物
    void addFoodRel(MultipartFile file, JrFoodRelationService foodRelationService);
}
