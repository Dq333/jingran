package com.dq.jingran.service;

import com.dq.jingran.entity.JrBanner;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dq.jingran.vo.banner.BannerParam;

import java.util.List;

/**
 * (JrBanner)表服务接口
 *
 * @author Dq
 * @since 2022-09-27 20:43:50
 */
public interface JrBannerService  extends IService<JrBanner> {

    //添加banner图片
    void addBanner(BannerParam bannerParam);

    //查询banner
    List<JrBanner> getBanner();
}