package com.dq.jingran.service;

import com.dq.jingran.entity.JrAccommodation;
import com.dq.jingran.entity.JrScenery;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dq.jingran.vo.scenery.JrSceneryVo;
import com.dq.jingran.vo.scenery.SceneryVo;
import com.dq.jingran.vo.scenery.SearchSceneryParams;
import com.dq.jingran.vo.store.StoreParams;

import java.util.List;

/**
 * (JrScenery)表服务接口
 *
 * @author Dq
 * @since 2022-09-27 17:52:12
 */
public interface JrSceneryService  extends IService<JrScenery> {

    //获取基本信息
    List<SceneryVo> getBasicInfo();

    //随机获取景点信息
    List<SceneryVo> getPartInfo();

    //查询景点和下面的轮播图
    JrSceneryVo getByAllById(String pid,String userId);


    //查询收藏列表
    List<SceneryVo> findListByUserFavority(List<String> objectIds);

    //模糊查询
    List<SceneryVo> searchItems(SearchSceneryParams sceneryParams);
}