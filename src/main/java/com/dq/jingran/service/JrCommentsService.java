package com.dq.jingran.service;

import com.dq.jingran.entity.JrComments;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dq.jingran.params.comments.CommentParam;
import com.dq.jingran.vo.space.SpaceCommentsVo;
import com.dq.jingran.vo.space.SpaceUserVo;

import java.util.List;

/**
 * (JrComments)表服务接口
 *
 * @author Dq
 * @since 2022-09-24 15:54:04
 */
public interface JrCommentsService  extends IService<JrComments> {

    // 根据空间id获取当前空间的所有评论
    List<SpaceCommentsVo> findAllDataBySpaceId(String id);

    //  添加某个空间的评论功能
    void addComment(CommentParam commentParam);

    //查询当前评论的人数
    List<SpaceUserVo> findAllLike(String id);
}