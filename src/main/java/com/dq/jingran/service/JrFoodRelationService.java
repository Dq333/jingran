package com.dq.jingran.service;

import com.dq.jingran.entity.JrFoodRelation;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * (JrFoodRelation)表服务接口
 *
 * @author Dq
 * @since 2022-10-18 14:23:08
 */
public interface JrFoodRelationService  extends IService<JrFoodRelation> {

    //查询相关图片
    List<String> selectOtherImg(String fId);
}