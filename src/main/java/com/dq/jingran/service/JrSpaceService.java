package com.dq.jingran.service;

import com.dq.jingran.entity.JrSpace;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dq.jingran.vo.space.SpaceAndCommendsVo;
import com.dq.jingran.vo.space.upfile.UpSpace;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * (JrSpace)表服务接口
 *
 * @author Dq
 * @since 2022-09-24 15:51:47
 */
public interface JrSpaceService  extends IService<JrSpace> {

    /**
     * 获取空间、评论信息
     * @return
     */
    List<SpaceAndCommendsVo> getAllInfoAndCommend();

    /**
     * 上传空间
     * @param file
     * @param userId
     * @param reviews
     */
    void upSpace(MultipartFile file, String userId,String reviews);
}