package com.dq.jingran.exception;

import com.dq.jingran.vo.R;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @project: jingran
 * @ClassName: exceptionHandler
 * @author: dq
 * @creat: 2022/9/19 17:53
 */
@RestControllerAdvice
public class exceptionHandler {
    /**
     * 异常捕捉
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    public R handleEx(Exception e){
        e.printStackTrace();
        String message = e.getMessage();
        // log.error(message);
        return R.error(message);
    }


    /**
     * dq异常处理
     * @param e
     * @return
     */
    @ExceptionHandler(ExceptionVo.class)
    public R handleEx(ExceptionVo e){
        e.printStackTrace();
        // log.error(message);
        return R.error_Normal().code(e.getCode()).data("msg",e.getMessage());
    }
}
