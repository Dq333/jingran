package com.dq.jingran.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @project: jingran
 * @ClassName: ExceptionVo
 * @author: dq
 * @creat: 2022/9/19 17:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExceptionVo extends RuntimeException{
    private Integer code;
    private String message;
}
