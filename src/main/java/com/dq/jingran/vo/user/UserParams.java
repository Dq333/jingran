package com.dq.jingran.vo.user;

import lombok.Data;

/**
 * @project: jingran
 * @ClassName: UserVo
 * @author: dq
 * @creat: 2022/9/20 9:14
 */
@Data
public class UserParams {
    private String username;
    private String password;
}
