package com.dq.jingran.vo.user;

import lombok.Data;

/**
 * @project: jingran
 * @ClassName: ChangeUserNameParams
 * @author: dq
 * @creat: 2022/10/17 17:51
 */
@Data
public class ChangeUserNameParams {
private String userId;
private String userName;
}
