package com.dq.jingran.vo.user;

import lombok.Data;

/**
 * @project: jingran
 * @ClassName: UpdatePassParams
 * @author: dq
 * @creat: 2022/10/15 15:14
 */
@Data
public class UpdatePassParams {
    private String userId;
    private String oldPassword;
    private String password;
}
