package com.dq.jingran.vo.space;

import lombok.Data;

import java.util.List;

/**
 * @project: jingran
 * @ClassName: SpaceAndCommondsVo
 * @author: dq
 * @creat: 2022/9/24 15:54
 */
@Data
public class SpaceAndCommendsVo {
    private SpaceVo space;
    private Boolean isLike;
    private List<SpaceUserVo> like;
    private List<SpaceCommentsVo> commentsList;
}
