package com.dq.jingran.vo.space;

import lombok.Data;

/**
 * @project: jingran
 * @ClassName: SpaceUserVo
 * @author: dq
 * @creat: 2022/9/24 16:10
 */
@Data
public class SpaceUserVo {
    private String id;
    /**
     * 用户姓名
     */
    private String username;

    private String avatar;
}
