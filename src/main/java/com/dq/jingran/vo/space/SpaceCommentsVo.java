package com.dq.jingran.vo.space;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @project: jingran
 * @ClassName: SpaceCommentsVo
 * @author: dq
 * @creat: 2022/9/24 16:14
 */
@Data
public class SpaceCommentsVo {
    private String id;


    /**
     * 评论的语言
     */
    private String reviews;
    /**
     * 发布的时间
     */
    private Date gmtTime;

    /**
     * 用户信息
     */
    private SpaceUserVo user;
}
