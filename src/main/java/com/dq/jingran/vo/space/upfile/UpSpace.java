package com.dq.jingran.vo.space.upfile;

import lombok.Data;

/**
 * @project: jingran
 * @ClassName: UpSpace
 * @author: dq
 * @creat: 2022/10/16 21:39
 */
@Data
public class UpSpace {
    private String userId;
    private String reviews;
}
