package com.dq.jingran.vo.space;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.Date;

/**
 * @project: jingran
 * @ClassName: SpaceVo
 * @author: dq
 * @creat: 2022/9/24 16:09
 */
@Data
public class SpaceVo {
    private String id;
    /**
     * 发布空间的评论
     */
    private String reviews;
    /**
     * 发布空间时需要的图片
     */
    private String img;
    /**
     * 上传时间
     */
    private Date gmtTime;
    /**
     * 封装对象Vo类
     */
    private SpaceUserVo user;
}
