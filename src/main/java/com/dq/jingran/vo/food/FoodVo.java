package com.dq.jingran.vo.food;

import lombok.Data;

import java.util.List;

/**
 * @project: jingran
 * @ClassName: FoodVo
 * @author: dq
 * @creat: 2022/10/16 15:42
 */
@Data
public class FoodVo {
    private String id;

    private Boolean isLike;

    /**
     * 食物名称（标题）
     */
    private String title;
    /**
     * 打分
     */
    private Double score;

    private String phone;
    private String workTime;
    private String address;

    /**
     * 评论人数
     */
    private Integer commendPerson;
    /**
     * 人均消费
     */
    private Double price;
    /**
     * 特色 （四川第一名菜）
     */
    private String features;
    /**
     * 图片url
     */
    private String img;
    /**
     * 食物推荐语  只是针对水果
     */
    private String command;

    private List<String> otherImg;
}
