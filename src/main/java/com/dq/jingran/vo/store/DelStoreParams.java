package com.dq.jingran.vo.store;

import lombok.Data;

/**
 * @project: jingran
 * @ClassName: DelStoreParams
 * @author: dq
 * @creat: 2022/10/18 15:05
 */
@Data
public class DelStoreParams {
    private String uid;
    private String proId;

}
