package com.dq.jingran.vo.store;

import lombok.Data;

/**
 * @project: jingran
 * @ClassName: storeParams
 * @author: dq
 * @creat: 2022/10/15 18:54
 */
@Data
public class StoreParams {

    private String uid;

    private String proId;
    /**
     * (1 美食 2 酒店 3风景)
     */
    private Integer state;

}
