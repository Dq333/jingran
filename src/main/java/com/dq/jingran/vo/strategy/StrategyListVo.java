package com.dq.jingran.vo.strategy;

import lombok.Data;

/**
 * @project: jingran
 * @ClassName: StrategyListVo
 * @author: dq
 * @creat: 2022/10/2 15:40
 */
@Data
public class StrategyListVo {

    /**
     * 旅游攻略id
     */
    private String  id;
    /**
     * 攻略标题
     */
    private String title;
    /**
     * 用户昵称
     */
    private String nickname;
    /**
     * 用户头像
     */
    private String useravatar;
    /**
     * 图像1
     */
    private String img1;
    /**
     * 图形2
     */
    private String img2;
    /**
     * 图像3
     */
    private String img3;
}
