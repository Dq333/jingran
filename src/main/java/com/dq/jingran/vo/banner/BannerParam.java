package com.dq.jingran.vo.banner;

import lombok.Data;

/**
 * @project: jingran
 * @ClassName: BannerParam
 * @author: dq
 * @creat: 2022/9/27 20:44
 */
@Data
public class BannerParam {
    private Integer sort;
    private String img;
}
