package com.dq.jingran.vo.accommodation;

import lombok.Data;

/**
 * @project: jingran
 * @ClassName: AccommodationVo
 * @author: dq
 * @creat: 2022/9/26 21:33
 */
@Data
public class AccommodationVo {
    private String id;
    /**
     * 住宿的名称
     */
    private String title;
    /**
     * 住宿的价格
     */
    private String price;
    /**
     * 住宿的收藏
     */
    private String collection;
    /**
     * 住宿地址
     */
    private String address;
    /**
     * 平均消费
     */
    private String advantage;
    /**
     * 平均得分
     */
    private Double accommodationScore;
    /**
     * 住宿图片
     */
    private String accommodationImg;
}
