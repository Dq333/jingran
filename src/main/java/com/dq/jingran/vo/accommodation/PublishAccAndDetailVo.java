package com.dq.jingran.vo.accommodation;

import com.dq.jingran.entity.JrAccommodation;
import com.dq.jingran.entity.JrDetails;
import lombok.Data;

import java.util.List;

/**
 * @project: jingran
 * @ClassName: PublishAccAndDetailVo
 * @author: dq
 * @creat: 2022/9/26 21:49
 */
@Data
public class PublishAccAndDetailVo {
    private Boolean isLike;
    /**
     * 住宿信息
     */
    private JrAccommodation accommodation;
    /**
     * 住宿其他信息
     */
    private List<JrDetails> details;
}
