package com.dq.jingran.vo.recreational;

import lombok.Data;

/**
 * @project: jingran
 * @ClassName: RecreationalVo
 * @author: dq
 * @creat: 2022/9/20 16:56
 */
@Data
public class RecreationalVo {
    private String id;
    /**
     * 休闲区的名称
     */
    private String recreationalName;
    /**
     * 休闲地区的img
     */
    private String recreationalImg;
}
