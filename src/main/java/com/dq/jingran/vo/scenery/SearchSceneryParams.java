package com.dq.jingran.vo.scenery;

import lombok.Data;

/**
 * @project: jingran
 * @ClassName: SearchSceneryVo
 * @author: dq
 * @creat: 2022/10/20 14:57
 */
@Data
public class SearchSceneryParams {
    private String fuzzyName;
}
