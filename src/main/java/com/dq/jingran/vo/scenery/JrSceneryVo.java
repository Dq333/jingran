package com.dq.jingran.vo.scenery;

import com.dq.jingran.entity.JrRecreational;
import com.dq.jingran.entity.JrScenery;
import lombok.Data;

import java.util.List;

/**
 * @project: jingran
 * @ClassName: JrSceneryVo
 * @author: dq
 * @creat: 2022/9/30 15:08
 */
@Data
public class JrSceneryVo {
    private Boolean isLike;
    private JrScenery scenery;
    private List<JrRecreational> recreational;
}
