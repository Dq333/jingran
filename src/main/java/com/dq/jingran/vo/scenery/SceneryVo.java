package com.dq.jingran.vo.scenery;

import com.dq.jingran.entity.JrScenery;
import com.dq.jingran.vo.recreational.RecreationalVo;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @project: jingran
 * @ClassName: ScenneryVo
 * @author: dq
 * @creat: 2022/9/20 16:52
 */
@Data
public class SceneryVo {

    private String id;
    /**
     * 具体的旅游景点（琼海）
     */
    private String title;
    /**
     * 琼海景点的img
     */
    private String img;
    /**
     * 景点价格
     */
    private String price;
    /**
     * 景点地址（省 市）
     */
    private String location;
    /**
     * 观看人数
     */
    private Integer viewCount;
    /**
     * 分类
     */
    private String category;
    /**
     * 得分
     */
    private Double score;
}
