package com.dq.jingran.vo.scenery;

import lombok.Data;

/**
 * @project: jingran
 * @ClassName: SceneryParams
 * @author: dq
 * @creat: 2022/9/20 14:05
 */
@Data
public class SceneryParams {
    /**
     * 具体的旅游景点（琼海）
     */
    private String title;
    /**
     * 景点细节介绍
     */
    private String introduce;

    /**
     * 景点的url
     */
    private String img;
}
