/*
 Navicat Premium Data Transfer

 Source Server         : 刘康sql
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : bj-cynosdbmysql-grp-4n1n365s.sql.tencentcdb.com:22891
 Source Schema         : jingran

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 05/11/2022 16:49:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for jr_accommodation
-- ----------------------------
DROP TABLE IF EXISTS `jr_accommodation`;
CREATE TABLE `jr_accommodation`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL COMMENT '住宿的id',
  `title` char(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '住宿的名称',
  `phone` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '电话',
  `collection` int(0) NULL DEFAULT NULL COMMENT '收藏人数',
  `modified` text CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL COMMENT '修饰词',
  `price` int(0) NULL DEFAULT NULL COMMENT '价格',
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '住宿地址',
  `advantage` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '平均消费',
  `introduce` text CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL COMMENT '住宿的简介',
  `accommodation_score` double(10, 2) NULL DEFAULT NULL COMMENT '平均得分',
  `accommodation_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '住宿图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_german2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jr_accommodation
-- ----------------------------
INSERT INTO `jr_accommodation` VALUES ('1574647697462562817', '西昌中心智选假日酒店', '18011585044', 999, '华灯初上，万物升平，数十间豪华的KTV包房隐没在皇家的夜色之中，这里的镜、画、光、饰、挂、摆、陈、色、间等九大娱乐空间的新概念，匠心独具、金雕玉砌、浑然天成。', 333, '西昌航天大道二段', '位于市区，方便观摩市景，提供欧陆式或自助早餐、内部提供商务中心以及饮料和零食自动售货机。\n\n免费停车，靠近西昌青山机场交通方便。', '西昌中心智选假日酒店提供位于西昌的住宿。这家住宿提供的设施包括餐厅、24小时前台、客房服务以及免费WiFi。 这家酒店提供欧陆式或自助早餐。 西昌中心智选假日酒店内部提供商务中心以及饮料和零食自动售货机。 最近的机场是西昌青山机场，距离这家住宿有16公里。 酒店早餐很丰富，味道很不错', 9.20, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E4%B8%AD%E5%BF%83%E6%99%BA%E9%80%89%E5%81%87%E6%97%A5%E9%85%92%E5%BA%97%E5%86%85%E9%83%A81.jpg');
INSERT INTO `jr_accommodation` VALUES ('1574647699102535682', '尚客优连锁四川西昌邛海湿地店', '18011585044', 1024, ' 酒店拥有世界一流的豪华俱乐部、西餐厅、日本料理、粤菜、酒吧、娱乐中心、SPA、宴会厅、大型停车场和世界著名品牌精品店等一系列设施恭候各位贵宾的光临。 3 .皇家国际饭店是融会国际顶尖商务酒店设计理念，装潢艺术构造于一体的四星级豪华酒店。', 246, '西昌海门渔村八区', '“地理位置好，干净，离湿地公园大门步行十分钟，周围安静，离餐饮集中地方很近，可免费停车。', '尚客优连锁四川西昌邛海湿地店位于西昌，仅接待中宾，提供空调客房、24小时前台和免费WiFi。 酒店的每间客房均配有书桌。', 8.60, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E5%B0%9A%E5%AE%A2%E4%BC%98%E8%BF%9E%E9%94%81%E5%9B%9B%E5%B7%9D%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%BA%97%E5%86%85%E9%83%A81.jpg');
INSERT INTO `jr_accommodation` VALUES ('1574647700218220545', '7天连锁酒店西昌航天大道吉祥路店', '18011585044', 359, '酒店拥有豪华套房、商务套房、高级房及标准房114间。酒店客房关注到每一个细节的典雅装饰，尤其是精致前卫的磨砂玻璃幕墙浴室，令居住其中成为美的享受，让商务劳顿烟消云散，使您尽享都市风情。', 190, '西昌吉祥路中段', '酒店距离南桥汽车站有5分钟车程，距离邛海公寓有14分钟车程，距离西昌古城有6分钟车程。酒店房间均配有空调、有线电视和桌子。酒店350米范围内有众多餐厅和美食选择。', '7天连锁酒店西昌航天大道吉祥路店仅接待中国大陆客人入住，提供简易住宿。酒店覆盖免费WiFi。 酒店距离南桥汽车站有5分钟车程，距离邛海公寓有14分钟车程，距离西昌古城有6分钟车程。 酒店房间均配有空调、有线电视和桌子。私人浴室带淋浴，提供拖鞋和免费洗浴用品。 客人可步行或驾车在周边游览。酒店350米范围内有众多餐厅和美食选择。', 8.60, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/7%E5%A4%A9%E8%BF%9E%E9%94%81%E9%85%92%E5%BA%97%E8%A5%BF%E6%98%8C%E8%88%AA%E5%A4%A9%E5%A4%A7%E9%81%93%E5%90%89%E7%A5%A5%E8%B7%AF%E5%BA%97%E5%86%85%E9%83%A81.jpg');
INSERT INTO `jr_accommodation` VALUES ('1574647701652672513', '汉庭西昌月亮湖湿地公园酒店', '18011585042', 679, '酒店设计以金黄色为主色调，弥漫着浓郁的地中海风情，更有来自世界各地的装饰：法国的青铜、意大利的音乐喷泉、法国的水晶灯、国际一流水准的寝室用品、加上富丽堂皇的回廊，金箔的装饰，由内及外无不彰显皇室气派。将您带回到埃及神秘的贵族奢华尊贵的生活。', 325, '西昌胜利东路', '酒店的客房均提供平板电视、空调以及配有淋浴设施和拖鞋的私人浴室。部分客房提供休息区。所有房间为客人提供书桌和电热水壶。除此之外还提供自助餐服务，靠近靠近景区、靠近西昌青山机场，交通方便，内设独立的健康医疗机构', '汉庭西昌月亮湖湿地公园酒店位于西昌。客人可以欣赏市景。 这家酒店的客房均提供平板电视、空调以及配有淋浴设施和拖鞋的私人浴室。部分客房提供休息区。所有房间为客人提供书桌和电热水壶。 汉庭西昌月亮湖湿地公园酒店的前台可提供信息，以帮助客人游览该地区。 最近的机场是西昌青山机场，距离这家住宿有14公里。', 8.80, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E6%B1%89%E5%BA%AD%E8%A5%BF%E6%98%8C%E6%9C%88%E4%BA%AE%E6%B9%96%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E9%85%92%E5%BA%97.jpg');
INSERT INTO `jr_accommodation` VALUES ('1574647703481389058', '7天优品·西昌火把广场邛海湿地公园店', '18011585023', 777, '酒店地理位置得天独厚。酒店毗邻地铁站和主要商务区，距亚洲著名的购物圈—正佳广场、宏城广场和天河城咫尺之遥.', 250, '西昌海滨村', '酒店靠近景区，交通方便，内部住宿及周边环境优良，附近有较多美食区，购物区等', '7天优品·西昌火把广场邛海湿地公园店坐落于西昌，距离西昌有5.1公里，提供带空调的客房。这家住宿距离Chuanxingpu约有8.6公里，距离Daxingchang有13公里，距离Gaocaoba有17公里。住宿配备24小时前台以及免费WiFi。 这家酒店距离Huanglianguan有24公里，距离Hexi有27公里。最近的机场是西昌青山机场，距离7天优品·西昌火把广场邛海湿地公园店有20公里。', 8.20, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/7%E5%A4%A9%E4%BC%98%E5%93%81%C2%B7%E8%A5%BF%E6%98%8C%E7%81%AB%E6%8A%8A%E5%B9%BF%E5%9C%BA%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E5%BA%97%E5%85%A8%E6%99%AF.jpg');
INSERT INTO `jr_accommodation` VALUES ('1574647705255579650', '西昌邛海湿地公园旁·路客精品公寓', '18011582044', 512, ' 酒店装潢风格融中国园林艺术与西方巴洛克文化之精髓，其中云石雕刻，富丽堂皇。酒店面积约为二万余平方米，地上八层，地下一层。', 300, '四川省西昌市邛海区 ', '公寓靠近景区，环境优良配有3卧室公寓配有用餐区、电视、空调以及带冰箱的厨房。附近交通方便', '西昌邛海湿地公园旁·路客精品公寓·00164180位于西昌。这家公寓提供共用厨房和免费WiFi。 这家空调3卧室公寓配有用餐区、电视以及带冰箱的厨房。 最近的机场是西昌青山机场，距离这家公寓有18公里。', 9.00, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E6%97%81%C2%B7%E8%B7%AF%E5%AE%A2%E7%B2%BE%E5%93%81%E5%85%AC%E5%AF%93%C2%B7%E5%86%85%E9%83%A82.jpg');
INSERT INTO `jr_accommodation` VALUES ('1574647707017187330', '逸尚东方酒店(西昌邛海湿地店', '18011575044', 520, '客房内提供免费宽带上网，24小时热水淋浴，空调，电视，电话，有标准的席梦思床具及配套家具。 7 . 酒店提供装饰雅致且现代化舒适设施的客房，客房设有可以观赏到浪漫海景的私人阳台。', 120, '西昌市北碧府', '位于市中心交通方便，人流量多，方便购物，环境优良，体验感良好', '西昌逸尚东方酒店秉持“以人文本，宾客至上”的经营理念，为宾客提供贴心、温馨、高效的优质服务，同时满足宾客从商务到休闲的多元化需求。即便陌生的城市也会有此般亲切，此般温暖，逸尚东方“您”阳光出行的不二之选。', 9.40, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%EF%BC%8C%E5%8C%97%E7%A2%A7%E5%BA%9C%E8%B7%AF%E9%91%AB%E6%BA%90%E9%98%B3%E5%85%891%E5%8F%B7%E6%A5%BC%E5%A4%96%E9%83%A8.jpg');
INSERT INTO `jr_accommodation` VALUES ('1574647709554741249', '西昌I Home 时代驿家', '18011583244', 1314, '店气派超然，雄踞商业及休闲中心地带，俯瞰城市，大家风范，舍我其谁。 11. 四百余间超豪华客房均配有最豪华的布艺、家具和设施，以浓重而不失活泼的色调、奔放且大气的布局、近似自然优美的线条，给每一位客人豪华舒适、至尊至贵的体验。', 90, '四川省西昌市航天大道', '酒店环境优异，设施齐全，靠近景区方便出行，周边市场活跃适合购物以及娱乐游玩，提供三餐', '酒店地处西昌繁华区域，紧临西昌标志性商业综合体~时代广场，商业覆盖有星巴克、小米体验店、大地影院、美食阁等。为您的出行提供“吃喝玩乐购”一站式体验业态，配套成熟，方便无忧。酒店距离邛海、湿地公园约10分钟车程；步行百米可至凉山民族体育场、凉山民族风情园、月亮湖公园。\n西昌I Home 时代驿家是由西昌时代广场打造的长、短租精品北欧风格酒店公寓。“时代驿家”将北欧的浪漫奢华与东方的简约时尚完美融合，拥有58间典雅大气的精品单卧、双卧套房，每一套双卧房均有两个私密独立的客房（1.8m+1.5m大床）以及单独的厨房（享受烹饪乐趣）、客厅（共度欢乐时光）等。', 9.20, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8CI%20Home%20%E6%97%B6%E4%BB%A3%E9%A9%BF%E5%AE%B6%20%E5%A4%96%E9%83%A8.jpg');
INSERT INTO `jr_accommodation` VALUES ('1574647711777722369', '西昌邛海湾柏樾酒店', '18011581144', 621, '白金五星级标准的地中海国际酒店气派超然，雄踞商业及休闲中心地带，俯瞰广州城，大家风范，舍我其谁。四百余间超豪华客房均配有最豪华的布艺、家具和设施，以浓重而不失活泼的色调、奔放且大气的布局、近似自然优美的线条，给每一位客人豪华舒适、至尊至贵的体验。', 210, '四川省西昌市海滨大道', '酒店环境十分优良，服务全面，所有房间为客人提供书桌和电热水壶。除此之外还提供自助餐服务，十分适合度假消遣游玩', '西昌邛海湾柏樾酒店位于西昌市海南乡，坐邛海拥泸山，距市区约10分钟车程。酒店将自然原生态与人文关怀和谐融合，从客房设计到装潢、配套设施到摆件陈设，都极尽为您的感受和需求所想，提供最为舒适、私密的环境，更有专业管家定制服务，为您打造殿堂级的享受。酒店内茶室、棋牌室、健身室、室外游泳池等一应俱全，以尽善尽美的服务为您带来完美的旅居生活体验。拥有阳光、沙滩、海水、湿地和清新的空气，让您一旦入住，即可感受自然与人的和谐喜悦。回望邛海，宛如仙境，不似人间。本店于2019年7月4日至7月16日期间赞助并接待了湖南卫视金鹰卡通栏目组拍摄的大型真人秀节目《童趣大冒险》。该节目于2019年9月15日在湖南卫视金鹰卡通台进行了播放，其节目总时长共计38分钟', 8.80, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E6%B9%BE%E6%9F%8F%E6%A8%BE%E9%85%92%E5%BA%97%20%E5%86%85%E9%83%A81.jpg');
INSERT INTO `jr_accommodation` VALUES ('1574647713946177538', '丽怡酒店(邛海湿地公园店)', '18011585214', 555, '华灯初上，万物升平，数十间豪华的KTV包房隐没在皇家的夜色之中，这里的镜、画、光、饰、挂、摆、陈、色、间等九大娱乐空间的新概念，匠心独具、金雕玉砌、浑然天成。', 120, '四川省西昌市海南街道办事处', '酒店环境十分优良结合原生态环境，服务全面，所有房间为客人提供书桌和电热水壶。除此之外还提供自助餐服务，十分适合度假消遣游玩', '酒店坐落于西昌市邛海泸山风景名胜区南岸核心区--沐歌小镇（4A级风景区）。酒店拥有时尚雅姿的客房及中式自助早餐厅，早餐菜品近40余种。室内整体设计巧妙运用了时尚而充满活力的运动装修风格，配备健身房及自助洗衣房各一间。房间均配备国内一线品牌床垫，数字电视、迷你吧、小型运动健身器材、WiFi全覆盖及特质天然香氛洗护用品。酒店拥有多功能会议室两间，分别可容纳120人大型会议及15人定制高端会议室。先进的视听设备及各项会务设施，是一家集商务办公、休闲娱乐、旅游度假的综合性度假村级酒店。让你享受在此每一刻是我们存在的理由，整洁而舒适的客房配套天然香氛洗护用品可以让您消除运动后的疲惫。酒店周边交通便利毗邻景区是您休闲度假的理想局停之所。', 9.40, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E4%B8%BD%E6%80%A1%E9%85%92%E5%BA%97%28%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E5%BA%97%29%20%E5%A4%96%E9%83%A8.jpg');
INSERT INTO `jr_accommodation` VALUES ('1574647716152381442', '安塔拉酒店(西昌邛海17度店)', '18011585095', 423, '酒店设计以娱乐色为主色调，弥漫着浓郁的地中海风情，更有来自世界各地的装饰：法国的青铜、意大利的音乐喷泉、法国的水晶灯、国际一流水准的寝室用品、加上富丽堂皇的回廊，金箔的装饰，由内及外无不彰显皇室气派。将您带回到埃及神秘的贵族奢华尊贵的生活。', 380, '四川省西昌市航天大道五段', '酒店靠近景区，交通方便，内部住宿及周边环境优良，附近有较多美食区，购物区等', NULL, 8.70, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E5%AE%89%E5%A1%94%E6%8B%89%E9%85%92%E5%BA%97%28%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B717%E5%BA%A6%E5%BA%97%29%20%E5%86%85%E9%83%A81.jpg');
INSERT INTO `jr_accommodation` VALUES ('1574647718354391042', '西昌邛海宾馆', '13711585044', 411, '白金五星级标准的地中海国际酒店气派超然，雄踞商业及休闲中心地带，俯瞰广州城，大家风范，舍我其谁。', 300, '四川省西昌市海滨', '位于市区，方便观摩市景，提供欧陆式或自助早餐、内部提供商务中心以及饮料和零食自动售货机。\n\n免费停车，靠近西昌青山机场交通方便。', '西昌邛海宾馆位于四川省凉山彝族自治州首府西昌市，地处邛海泸山国家4A级风景名胜区，依山傍水，素有凉山“国宾馆”的美誉。\n西昌邛海宾馆以优美的环境、优良的设施、优质的服务、企业文化吸引了众多国内外宾客的下榻。并受到韩国前总统金泳三和国际银行行长沃尔•芬森等国际政要的高度赞誉。\n西昌邛海宾馆占地面积超过12万平方米，建筑面积8万平方米，呈花园式格局，建筑呈别墅式分布。宾馆现拥有国际会展中心、临湖别墅、温泉水疗馆、国际标准网球场、映涛楼、皓月楼、阳光楼、晓风楼、观海楼、晓楼、餐饮中心等一批独立建筑群；可提供各类客房及总统套房三百余间；大小餐厅19个，可同时容纳1500人进餐；各类会议室近20个，可同时提供3000人会议使用。\n西昌邛海宾馆内园林别墅新颖独特、别致典雅。这里集海水、沙滩、阳光、绿色、空气为一体，透过落地玻璃窗，可见海天一色的美景近在咫尺。\n酒店拥有凉山一流的温泉水疗馆，由独具匠心的池群组成，包括SPA房、花瓣浴、阳光水疗、桑拿，让您在这里尽情享乐、松弛身心；餐饮中心特别准备了中西自助餐、民族风味餐、海滩露天酒会、月光烧烤晚会；迷人的花园草坪婚礼，更是为爱人们营造了一个浪漫的殿堂。\n豪华气派、功能齐全的国际会展中心为各种商务会议提供了一个理想的空间。2009年宾馆又推出了集凉山原生态歌舞、神秘的毕摩文化、纯天然、无污染绿色食品和独特烹饪手法为一体的综合性民族精品盛宴——彝皇宴，深受客人青睐。\n这里有五彩的民族歌舞、醇香的杆杆酒，清香的荞麦茶，随时恭侯宾客的到来，让您感受温馨的“玫瑰家园”。', 8.60, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E5%AE%BE%E9%A6%86%20%E5%A4%96%E9%83%A81.jpg');
INSERT INTO `jr_accommodation` VALUES ('1574647720476708865', '西昌邛海西波鹤影酒店', '10911585044', 233, ' 酒店拥有世界一流的豪华俱乐部、西餐厅、日本料理、粤菜、酒吧、娱乐中心、SPA、宴会厅、大型停车场和世界著名品牌精品店等一系列设施恭候各位贵宾的光临。', 156, '四川省西昌市海滨', '酒店的客房均提供平板电视、空调以及配有淋浴设施和拖鞋的私人浴室。部分客房提供休息区。所有房间为客人提供书桌和电热水壶。除此之外还提供自助餐服务，靠近靠近景区、靠近西昌青山机场，交通方便，内设独立的健康医疗机构', '酒店位于邛海新村大湾，地处泸山脚下、邛海之畔的西波鹤影湿地内，环境优美。\n酒店建筑风格独特，与自然湿地浑然一体。院内花草鱼鸟、珍稀植物、亭台楼榭、书画文字错落有致，设计风格处处彰显中国经典园林与现代休闲度假元素完美融合。\n客房用品均采用知名品牌，让您在尽享生活时可凭窗眺望邛海波涛、静谧湖面，朝观日跃邛海、晚看晧月升空。西波鱼庄——由高级厨师主理，依邛海自然造化，纳本地山、水天然食材，巧手烹饪各类本地特色菜品、邛海河鲜、川粤佳肴。\n鹤影茶房——突出传统中式茶道文化，在品茗之际可感受独特天顶鱼池，仰观蓝天白云、群鱼游弋，聆听高山流水、琴歌萦绕。露台亭落——点缀于芳草奇树之间，落英缤纷，提供咖啡、果茶、香茗、小餐，尽显惬意小资格调。\n酒店还为您提供环海自行车和海钓服务，更丰富您的度假休闲生活。无论您儒雅或是小资，无论您是青年才俊或是夕阳相伴，无论您事业有成或正抵力奋进，西昌西波鹤影酒店都会是您休闲、小憩、回味、思索之地。', 9.40, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E8%A5%BF%E6%B3%A2%E9%B9%A4%E5%BD%B1%E9%85%92%E5%BA%97%20%E5%A4%96%E9%83%A81.jpg');
INSERT INTO `jr_accommodation` VALUES ('1574647722624192513', '斯维登酒店(西昌邛海17度店)', '18011585184', 465, '古色古香 雅俗共赏 宽敞明亮 屋舍俨然', 199, '四川省西昌市航天大道', '酒店靠近景区，交通方便，内部住宿及周边环境优良，附近有较多美食区，购物区等，更有24小时厨师服务', '斯维登酒店（西昌邛海17度店）位于航天大道五段邛海17度旅游度假区，背靠邛海湿地公园，在房间内就可以眺望邛海和泸山，视野开阔。对面即是万达广场，同时周边有西昌美食街，火盆烧烤 串串 彝餐等应有尽有。\n每个房型均是落地窗加大阳台，白天可以在阳台端上一杯咖啡沐浴阳关。针对不同人群有轻奢风大床 商务双床房型 loft双卧套房可以做饭，门店提供全套烹饪厨具，让您大展身手。管家式服务，24小时管家随时在岗。', 8.50, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E6%96%AF%E7%BB%B4%E7%99%BB%E9%85%92%E5%BA%97%28%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B717%E5%BA%A6%E5%BA%97%29%20%E5%A4%96%E9%83%A81%E3%80%81.jpg');
INSERT INTO `jr_accommodation` VALUES ('1574647725111414786', '西昌四季假日酒店', '18011225044', 965, '金碧辉煌 高大雄伟 小巧玲珑 别具一格', 208, '四川省西昌市长安西路', '该酒店环境较好有齐全的设备，靠近景区方便出行，周边市场活跃适合购物以及娱乐游玩，提供三餐，非常好 性价比很高 前台服务态度很热情', '位于西昌市新城区，长安西路西客站旁（西昌旅游集散服务中心），前往火车站、青山机场都非常便捷。酒店门口设有大型公交汽车站，可直达4A级景区邛海、泸山、湿地公园等全市所有旅游景点的交通旅游线路；周围围绕当地特色有名餐饮、超市、商务中心、银行、医院等生活设施一应俱全。是一家集吃、住、行、游、购、娱于一体的综合型酒店。酒店配有多种类型简欧型客房，客房设施齐全。酒店一直“以细节彰显品位，以服务赢得感动”，为您提供旅途中的温馨驿站。', 8.80, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E5%9B%9B%E5%AD%A3%E9%85%92%E5%BA%97%20%E5%A4%96%E9%83%A81.jpg');

-- ----------------------------
-- Table structure for jr_banner
-- ----------------------------
DROP TABLE IF EXISTS `jr_banner`;
CREATE TABLE `jr_banner`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'id',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '图片banner',
  `sort` int(0) NULL DEFAULT 0 COMMENT '权重',
  `gmt_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jr_banner
-- ----------------------------
INSERT INTO `jr_banner` VALUES ('1574745069484007425', 'https://qimg4.youxiake.com/upload/202209/23/53c34945640f1141.jpg?imageslim', 1, '2022-09-27 20:57:37');
INSERT INTO `jr_banner` VALUES ('1574745208403550209', 'https://qimg4.youxiake.com/upload/202209/16/8ee79673ee45df64.jpg?imageslim', 1, '2022-09-27 20:58:10');
INSERT INTO `jr_banner` VALUES ('1574745310039924738', 'https://qimg4.youxiake.com/upload/202209/26/af0f4ce5a983316f.jpg?imageslim', 1, '2022-09-27 20:58:34');
INSERT INTO `jr_banner` VALUES ('1574745534523269122', 'https://qimg4.youxiake.com/upload/202209/07/069221a1bdec81a5.jpg?imageslim', 1, '2022-09-27 20:59:27');
INSERT INTO `jr_banner` VALUES ('1574745850341777409', 'https://qimg4.youxiake.com/upload/202108/17/f356089a3a398e32.jpg?imageslim', 1, '2022-09-27 21:00:43');
INSERT INTO `jr_banner` VALUES ('1574745966125539329', 'https://qimg4.youxiake.com/upload/202209/09/dc309fd90feba8f0.jpg?imageslim', 1, '2022-09-27 21:01:10');
INSERT INTO `jr_banner` VALUES ('1574746086510452738', 'https://qimg4.youxiake.com/upload/202209/13/5ad946b79af30c0f.jpg?imageslim', 1, '2022-09-27 21:01:39');
INSERT INTO `jr_banner` VALUES ('1574746241783586818', 'https://qimg4.youxiake.com/upload/202209/23/9f20baf4faff5ab1.jpg?imageslim', 1, '2022-09-27 21:02:16');

-- ----------------------------
-- Table structure for jr_comments
-- ----------------------------
DROP TABLE IF EXISTS `jr_comments`;
CREATE TABLE `jr_comments`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '评论id',
  `user_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户id',
  `space_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '空间的索引条件',
  `reviews` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '评论的语言',
  `gmt_time` datetime(0) NOT NULL COMMENT '发布的时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jr_comments
-- ----------------------------
INSERT INTO `jr_comments` VALUES ('1', '1572980558630350850', '1', '我要去带个我！', '2022-09-25 15:49:17');
INSERT INTO `jr_comments` VALUES ('10', '1', '4', '好羡慕你们。我北京最近天气特别冷。都没办法出门！', '2022-09-27 20:18:45');
INSERT INTO `jr_comments` VALUES ('11', '1574368035418316802', '5', '哎！当初我要有几个伙伴一起旅游也不至于现在没几个好盆友。', '2022-09-26 20:20:43');
INSERT INTO `jr_comments` VALUES ('12', '1572980558630350850', '5', '上面的楼主。你好你好！ 交个朋友吧。嘿嘿', '2022-09-27 20:21:28');
INSERT INTO `jr_comments` VALUES ('13', '1572040081290674178', '5', '对呀对呀，我们一起组个旅游 family 到时候有空我们一起组队旅游。想想就很好', '2022-09-27 20:22:29');
INSERT INTO `jr_comments` VALUES ('14', '1574368035418316802', '6', '。。。。。。。。', '2022-09-26 20:28:21');
INSERT INTO `jr_comments` VALUES ('15', '1574368236791046146', '6', '楼主 我明天带你去偷香肠 带你找回童年的味道。', '2022-09-27 20:29:03');
INSERT INTO `jr_comments` VALUES ('1582734877552328706', '1577180318901104641', '6', '欢迎大家来玩', '2022-10-19 22:06:15');
INSERT INTO `jr_comments` VALUES ('1582744987087454209', '1577180318901104641', '1582744859970682882', '哟西！', '2022-10-19 22:46:26');
INSERT INTO `jr_comments` VALUES ('1582986707335168001', '1577180318901104641', '1582986666264543234', '哈哈哈哈', '2022-10-20 14:46:56');
INSERT INTO `jr_comments` VALUES ('1582986801186914305', '1577180318901104641', '1582986666264543234', '哈哈哈哈', '2022-10-20 14:47:19');
INSERT INTO `jr_comments` VALUES ('1583299188360265730', '1577180318901104641', '1583299085968916482', '确实不错', '2022-10-21 11:28:37');
INSERT INTO `jr_comments` VALUES ('1583319409468948482', '1577180318901104641', '1583297787609530369', '确实！', '2022-10-21 12:48:59');
INSERT INTO `jr_comments` VALUES ('1583336340599128066', '1577180318901104641', '1583336285834100738', '天气很好', '2022-10-21 13:56:15');
INSERT INTO `jr_comments` VALUES ('1583407160126128130', '1577180318901104641', '1583407137741127682', '真好', '2022-10-21 18:37:40');
INSERT INTO `jr_comments` VALUES ('1583413145754451969', '1577180318901104641', '1583412917500428289', '确实', '2022-10-21 19:01:27');
INSERT INTO `jr_comments` VALUES ('16', '1572980558630350850', '6', '大无语！现在都是偷香肠么。想想哥当时  可是几个小伙伴在一个小房子炸土豆。 嘿嘿 真想回到以前呀！', '2022-09-28 20:30:25');
INSERT INTO `jr_comments` VALUES ('2', '1574368205459595266', '1', '带我带我！', '2022-09-24 15:49:37');
INSERT INTO `jr_comments` VALUES ('3', '1574368301148446722', '2', '西昌这个地方的天气确实挺好', '2022-09-24 15:50:01');
INSERT INTO `jr_comments` VALUES ('4', '1574368411907432449', '2', '多久我去找你 然后一起吃串串', '2022-09-24 15:50:26');
INSERT INTO `jr_comments` VALUES ('5', '1572040081290674178', '3', '好呀！好呀！到时候我们2个拼团一起去。', '2022-09-26 20:14:15');
INSERT INTO `jr_comments` VALUES ('6', '1572160547390410754', '3', '上面的姐妹 +1 +1  带个我呀！', '2022-09-27 20:14:47');
INSERT INTO `jr_comments` VALUES ('7', '1574368411907432449', '3', '我也好想去 呜呜~我妈不让我去。', '2022-09-28 20:15:10');
INSERT INTO `jr_comments` VALUES ('8', '1574368301148446722', '4', '我国庆整好要去成都武侯区玩，怎么说，一起吗？', '2022-09-26 20:16:52');
INSERT INTO `jr_comments` VALUES ('9', '1572980558630350850', '4', '我就在成都，我留个联系方式。你们来了我接你们.我的微信号：Dq333', '2022-09-27 20:17:56');

-- ----------------------------
-- Table structure for jr_details
-- ----------------------------
DROP TABLE IF EXISTS `jr_details`;
CREATE TABLE `jr_details`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '图片id',
  `accommodation_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '住宿id',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '图片url',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jr_details
-- ----------------------------
INSERT INTO `jr_details` VALUES ('1574647698053959681', '1574647697462562817', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E4%B8%AD%E5%BF%83%E6%99%BA%E9%80%89%E5%81%87%E6%97%A5%E9%85%92%E5%BA%97%E5%86%85%E9%83%A82.jpg');
INSERT INTO `jr_details` VALUES ('1574647698385309698', '1574647697462562817', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E4%B8%AD%E5%BF%83%E6%99%BA%E9%80%89%E5%81%87%E6%97%A5%E9%85%92%E5%BA%97%E5%86%85%E9%83%A83.jpg');
INSERT INTO `jr_details` VALUES ('1574647698779574274', '1574647697462562817', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E4%B8%AD%E5%BF%83%E6%99%BA%E9%80%89%E5%81%87%E6%97%A5%E9%85%92%E5%BA%97%E5%A4%96%E8%A7%82.jpg');
INSERT INTO `jr_details` VALUES ('1574647699500994562', '1574647699102535682', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E5%B0%9A%E5%AE%A2%E4%BC%98%E8%BF%9E%E9%94%81%E5%9B%9B%E5%B7%9D%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%BA%97%E5%86%85%E9%83%A82.jpg');
INSERT INTO `jr_details` VALUES ('1574647699823955970', '1574647699102535682', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E5%B0%9A%E5%AE%A2%E4%BC%98%E8%BF%9E%E9%94%81%E5%9B%9B%E5%B7%9D%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%BA%97%E5%86%85%E9%83%A83.jpg');
INSERT INTO `jr_details` VALUES ('1574647700541181954', '1574647700218220545', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/7%E5%A4%A9%E8%BF%9E%E9%94%81%E9%85%92%E5%BA%97%E8%A5%BF%E6%98%8C%E8%88%AA%E5%A4%A9%E5%A4%A7%E9%81%93%E5%90%89%E7%A5%A5%E8%B7%AF%E5%BA%97%E5%86%85%E9%83%A82.jpg');
INSERT INTO `jr_details` VALUES ('1574647700935446529', '1574647700218220545', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/7%E5%A4%A9%E8%BF%9E%E9%94%81%E9%85%92%E5%BA%97%E8%A5%BF%E6%98%8C%E8%88%AA%E5%A4%A9%E5%A4%A7%E9%81%93%E5%90%89%E7%A5%A5%E8%B7%AF%E5%BA%97%E5%86%85%E9%83%A83.jpg');
INSERT INTO `jr_details` VALUES ('1574647701266796546', '1574647700218220545', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/7%E5%A4%A9%E8%BF%9E%E9%94%81%E9%85%92%E5%BA%97%E8%A5%BF%E6%98%8C%E8%88%AA%E5%A4%A9%E5%A4%A7%E9%81%93%E5%90%89%E7%A5%A5%E8%B7%AF%E5%BA%97%E5%86%85%E9%83%A84.jpg');
INSERT INTO `jr_details` VALUES ('1574647701971439618', '1574647701652672513', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E6%B1%89%E5%BA%AD%E8%A5%BF%E6%98%8C%E6%9C%88%E4%BA%AE%E6%B9%96%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E9%85%92%E5%BA%97%E5%86%85%E9%83%A81.jpg');
INSERT INTO `jr_details` VALUES ('1574647702365704193', '1574647701652672513', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E6%B1%89%E5%BA%AD%E8%A5%BF%E6%98%8C%E6%9C%88%E4%BA%AE%E6%B9%96%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E9%85%92%E5%BA%97%E5%86%85%E9%83%A82.jpg');
INSERT INTO `jr_details` VALUES ('1574647702759968770', '1574647701652672513', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E6%B1%89%E5%BA%AD%E8%A5%BF%E6%98%8C%E6%9C%88%E4%BA%AE%E6%B9%96%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E9%85%92%E5%BA%97%E5%86%85%E9%83%A83.jpg');
INSERT INTO `jr_details` VALUES ('1574647703095513090', '1574647701652672513', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E6%B1%89%E5%BA%AD%E8%A5%BF%E6%98%8C%E6%9C%88%E4%BA%AE%E6%B9%96%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E9%85%92%E5%BA%97%E5%86%85%E9%83%A84.jpg');
INSERT INTO `jr_details` VALUES ('1574647703808544769', '1574647703481389058', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/7%E5%A4%A9%E4%BC%98%E5%93%81%C2%B7%E8%A5%BF%E6%98%8C%E7%81%AB%E6%8A%8A%E5%B9%BF%E5%9C%BA%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E5%BA%97%E5%86%85%E9%83%A81.jpg');
INSERT INTO `jr_details` VALUES ('1574647704144089089', '1574647703481389058', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/7%E5%A4%A9%E4%BC%98%E5%93%81%C2%B7%E8%A5%BF%E6%98%8C%E7%81%AB%E6%8A%8A%E5%B9%BF%E5%9C%BA%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E5%BA%97%E5%A4%96%E9%83%A81.jpg');
INSERT INTO `jr_details` VALUES ('1574647704534159362', '1574647703481389058', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/7%E5%A4%A9%E4%BC%98%E5%93%81%C2%B7%E8%A5%BF%E6%98%8C%E7%81%AB%E6%8A%8A%E5%B9%BF%E5%9C%BA%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E5%BA%97%E5%A4%A7%E5%8E%85.jpg');
INSERT INTO `jr_details` VALUES ('1574647704861315073', '1574647703481389058', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/7%E5%A4%A9%E4%BC%98%E5%93%81%C2%B7%E8%A5%BF%E6%98%8C%E7%81%AB%E6%8A%8A%E5%B9%BF%E5%9C%BA%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E5%BA%97%E9%99%84%E8%BF%911.jpg');
INSERT INTO `jr_details` VALUES ('1574647705578541058', '1574647705255579650', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E6%97%81%C2%B7%E8%B7%AF%E5%AE%A2%E7%B2%BE%E5%93%81%E5%85%AC%E5%AF%93%E5%86%85%E9%83%A81.jpg');
INSERT INTO `jr_details` VALUES ('1574647705968611329', '1574647705255579650', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E6%97%81%C2%B7%E8%B7%AF%E5%AE%A2%E7%B2%BE%E5%93%81%E5%85%AC%E5%AF%93%E5%86%85%E9%83%A83.jpg');
INSERT INTO `jr_details` VALUES ('1574647706291572738', '1574647705255579650', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E6%97%81%C2%B7%E8%B7%AF%E5%AE%A2%E7%B2%BE%E5%93%81%E5%85%AC%E5%AF%93%E5%86%85%E9%83%A84.jpg');
INSERT INTO `jr_details` VALUES ('1574647706685837313', '1574647705255579650', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E6%97%81%C2%B7%E8%B7%AF%E5%AE%A2%E7%B2%BE%E5%93%81%E5%85%AC%E5%AF%93%E5%A4%96%E9%83%A8.jpg');
INSERT INTO `jr_details` VALUES ('1574647707415646209', '1574647707017187330', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%EF%BC%8C%E5%8C%97%E7%A2%A7%E5%BA%9C%E8%B7%AF%E9%91%AB%E6%BA%90%E9%98%B3%E5%85%891%E5%8F%B7%E6%A5%BC%E5%86%85%E9%83%A8.jpg');
INSERT INTO `jr_details` VALUES ('1574647707797327874', '1574647707017187330', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%EF%BC%8C%E5%8C%97%E7%A2%A7%E5%BA%9C%E8%B7%AF%E9%91%AB%E6%BA%90%E9%98%B3%E5%85%891%E5%8F%B7%E6%A5%BC%E5%86%85%E9%83%A81.jpg');
INSERT INTO `jr_details` VALUES ('1574647708128677889', '1574647707017187330', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%EF%BC%8C%E5%8C%97%E7%A2%A7%E5%BA%9C%E8%B7%AF%E9%91%AB%E6%BA%90%E9%98%B3%E5%85%891%E5%8F%B7%E6%A5%BC%E5%86%85%E9%83%A82.jpg');
INSERT INTO `jr_details` VALUES ('1574647708514553858', '1574647707017187330', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%EF%BC%8C%E5%8C%97%E7%A2%A7%E5%BA%9C%E8%B7%AF%E9%91%AB%E6%BA%90%E9%98%B3%E5%85%891%E5%8F%B7%E6%A5%BC%E5%A4%A7%E5%8E%85.jpg');
INSERT INTO `jr_details` VALUES ('1574647708841709570', '1574647707017187330', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%EF%BC%8C%E5%8C%97%E7%A2%A7%E5%BA%9C%E8%B7%AF%E9%91%AB%E6%BA%90%E9%98%B3%E5%85%891%E5%8F%B7%E6%A5%BC%E9%A4%90%E5%8E%85.jpg');
INSERT INTO `jr_details` VALUES ('1574647709173059585', '1574647707017187330', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%EF%BC%8C%E5%8C%97%E7%A2%A7%E5%BA%9C%E8%B7%AF%E9%91%AB%E6%BA%90%E9%98%B3%E5%85%89%E5%86%85%E9%83%A8.jpg');
INSERT INTO `jr_details` VALUES ('1574647709886091265', '1574647709554741249', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8CI%20Home%20%E6%97%B6%E4%BB%A3%E9%A9%BF%E5%AE%B6%20%E5%86%85%E9%83%A81.jpg');
INSERT INTO `jr_details` VALUES ('1574647710280355841', '1574647709554741249', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8CI%20Home%20%E6%97%B6%E4%BB%A3%E9%A9%BF%E5%AE%B6%20%E5%86%85%E9%83%A82.jpg');
INSERT INTO `jr_details` VALUES ('1574647710666231810', '1574647709554741249', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8CI%20Home%20%E6%97%B6%E4%BB%A3%E9%A9%BF%E5%AE%B6%20%E5%86%85%E9%83%A83.jpg');
INSERT INTO `jr_details` VALUES ('1574647711056302082', '1574647709554741249', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8CI%20Home%20%E6%97%B6%E4%BB%A3%E9%A9%BF%E5%AE%B6%20%E5%86%85%E9%83%A85.jpg');
INSERT INTO `jr_details` VALUES ('1574647711379263490', '1574647709554741249', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8CI%20Home%20%E6%97%B6%E4%BB%A3%E9%A9%BF%E5%AE%B6%20%E5%86%85%E9%83%A85.jpg');
INSERT INTO `jr_details` VALUES ('1574647712113266689', '1574647711777722369', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8CI%20Home%20%E6%97%B6%E4%BB%A3%E9%A9%BF%E5%AE%B6%20%E5%86%85%E9%83%A82.jpg');
INSERT INTO `jr_details` VALUES ('1574647712507531265', '1574647711777722369', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E4%B8%AD%E5%BF%83%E6%99%BA%E9%80%89%E5%81%87%E6%97%A5%E9%85%92%E5%BA%97%E5%86%85%E9%83%A83.jpg');
INSERT INTO `jr_details` VALUES ('1574647712897601538', '1574647711777722369', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E6%B9%BE%E6%9F%8F%E6%A8%BE%E9%85%92%E5%BA%97%20%E5%86%85%E9%83%A84.jpg');
INSERT INTO `jr_details` VALUES ('1574647713233145858', '1574647711777722369', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E6%B9%BE%E6%9F%8F%E6%A8%BE%E9%85%92%E5%BA%97%20%E5%86%85%E9%83%A85.jpg');
INSERT INTO `jr_details` VALUES ('1574647713614827521', '1574647711777722369', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E6%B9%BE%E6%9F%8F%E6%A8%BE%E9%85%92%E5%BA%97%20%E5%86%85%E9%83%A86.jpg');
INSERT INTO `jr_details` VALUES ('1574647714403356674', '1574647713946177538', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E4%B8%BD%E6%80%A1%E9%85%92%E5%BA%97%28%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E5%BA%97%29%20%E5%86%85%E9%83%A82.jpg');
INSERT INTO `jr_details` VALUES ('1574647714722123777', '1574647713946177538', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E4%B8%BD%E6%80%A1%E9%85%92%E5%BA%97%28%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E5%BA%97%29%20%E5%86%85%E9%83%A83.jpg');
INSERT INTO `jr_details` VALUES ('1574647715045085186', '1574647713946177538', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E4%B8%BD%E6%80%A1%E9%85%92%E5%BA%97%28%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E5%BA%97%29%20%E5%86%85%E9%83%A84.jpg');
INSERT INTO `jr_details` VALUES ('1574647715435155457', '1574647713946177538', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E4%B8%BD%E6%80%A1%E9%85%92%E5%BA%97%28%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E5%BA%97%29%20%E5%86%85%E9%83%A85.jpg');
INSERT INTO `jr_details` VALUES ('1574647715825225730', '1574647713946177538', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E4%B8%BD%E6%80%A1%E9%85%92%E5%BA%97%28%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E5%BA%97%29%20%E5%86%85%E9%83%A81.jpg');
INSERT INTO `jr_details` VALUES ('1574647716538257409', '1574647716152381442', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E5%AE%89%E5%A1%94%E6%8B%89%E9%85%92%E5%BA%97%28%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B717%E5%BA%A6%E5%BA%97%29%20%E5%86%85%E9%83%A82.jpg');
INSERT INTO `jr_details` VALUES ('1574647716928327682', '1574647716152381442', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E5%AE%89%E5%A1%94%E6%8B%89%E9%85%92%E5%BA%97%28%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B717%E5%BA%A6%E5%BA%97%29%20%E5%86%85%E9%83%A83.jpg');
INSERT INTO `jr_details` VALUES ('1574647717259677697', '1574647716152381442', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E5%AE%89%E5%A1%94%E6%8B%89%E9%85%92%E5%BA%97%28%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B717%E5%BA%A6%E5%BA%97%29%20%E5%86%85%E9%83%A84.jpg');
INSERT INTO `jr_details` VALUES ('1574647717653942273', '1574647716152381442', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E4%B8%BD%E6%80%A1%E9%85%92%E5%BA%97%28%E9%82%9B%E6%B5%B7%E6%B9%BF%E5%9C%B0%E5%85%AC%E5%9B%AD%E5%BA%97%29%20%E5%86%85%E9%83%A85.jpg');
INSERT INTO `jr_details` VALUES ('1574647717972709377', '1574647716152381442', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E5%AE%89%E5%A1%94%E6%8B%89%E9%85%92%E5%BA%97%28%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B717%E5%BA%A6%E5%BA%97%29%20%E5%86%85%E9%83%A86.jpg');
INSERT INTO `jr_details` VALUES ('1574647718689935361', '1574647718354391042', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E5%AE%BE%E9%A6%86%20%20%E5%86%85%E9%83%A86.jpg');
INSERT INTO `jr_details` VALUES ('1574647719092588546', '1574647718354391042', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E5%AE%BE%E9%A6%86%20%E5%86%85%E9%83%A81.jpg');
INSERT INTO `jr_details` VALUES ('1574647719423938562', '1574647718354391042', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E5%AE%BE%E9%A6%86%20%E5%86%85%E9%83%A82.jpg');
INSERT INTO `jr_details` VALUES ('1574647719751094273', '1574647718354391042', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E5%AE%BE%E9%A6%86%20%E5%86%85%E9%83%A83.jpg');
INSERT INTO `jr_details` VALUES ('1574647720149553153', '1574647718354391042', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E5%AE%BE%E9%A6%86%20%E5%86%85%E9%83%A85.jpg');
INSERT INTO `jr_details` VALUES ('1574647720866779137', '1574647720476708865', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E8%A5%BF%E6%B3%A2%E9%B9%A4%E5%BD%B1%E9%85%92%E5%BA%97%20%E5%A4%96%E9%83%A82.jpg');
INSERT INTO `jr_details` VALUES ('1574647721193934850', '1574647720476708865', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E8%A5%BF%E6%B3%A2%E9%B9%A4%E5%BD%B1%E9%85%92%E5%BA%97%20%E5%86%85%E9%83%A81.jpg');
INSERT INTO `jr_details` VALUES ('1574647721575616514', '1574647720476708865', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E8%A5%BF%E6%B3%A2%E9%B9%A4%E5%BD%B1%E9%85%92%E5%BA%97%20%E5%86%85%E9%83%A82.jpg');
INSERT INTO `jr_details` VALUES ('1574647721902772226', '1574647720476708865', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E8%A5%BF%E6%B3%A2%E9%B9%A4%E5%BD%B1%E9%85%92%E5%BA%97%20%E5%86%85%E9%83%A83.jpg');
INSERT INTO `jr_details` VALUES ('1574647722292842497', '1574647720476708865', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B7%E8%A5%BF%E6%B3%A2%E9%B9%A4%E5%BD%B1%E9%85%92%E5%BA%97%20%E5%86%85%E9%83%A84.jpg');
INSERT INTO `jr_details` VALUES ('1574647723014262785', '1574647722624192513', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E6%96%AF%E7%BB%B4%E7%99%BB%E9%85%92%E5%BA%97%28%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B717%E5%BA%A6%E5%BA%97%29%20%E5%A4%96%E9%83%A82.jpg');
INSERT INTO `jr_details` VALUES ('1574647723328835586', '1574647722624192513', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E6%96%AF%E7%BB%B4%E7%99%BB%E9%85%92%E5%BA%97%28%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B717%E5%BA%A6%E5%BA%97%29%20%E5%86%85%E9%83%A81.jpg');
INSERT INTO `jr_details` VALUES ('1574647723718905858', '1574647722624192513', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E6%96%AF%E7%BB%B4%E7%99%BB%E9%85%92%E5%BA%97%28%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B717%E5%BA%A6%E5%BA%97%29%20%E5%86%85%E9%83%A82.jpg');
INSERT INTO `jr_details` VALUES ('1574647724041867266', '1574647722624192513', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E6%96%AF%E7%BB%B4%E7%99%BB%E9%85%92%E5%BA%97%28%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B717%E5%BA%A6%E5%BA%97%29%20%E5%86%85%E9%83%A83%E2%80%98.jpg');
INSERT INTO `jr_details` VALUES ('1574647724377411586', '1574647722624192513', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E6%96%AF%E7%BB%B4%E7%99%BB%E9%85%92%E5%BA%97%28%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B717%E5%BA%A6%E5%BA%97%29%20%E5%86%85%E9%83%A84.jpg');
INSERT INTO `jr_details` VALUES ('1574647724775870466', '1574647722624192513', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E6%96%AF%E7%BB%B4%E7%99%BB%E9%85%92%E5%BA%97%28%E8%A5%BF%E6%98%8C%E9%82%9B%E6%B5%B717%E5%BA%A6%E5%BA%97%29%20%E5%86%85%E9%83%A85.jpg');
INSERT INTO `jr_details` VALUES ('1574647725501485058', '1574647725111414786', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E5%9B%9B%E5%AD%A3%E9%85%92%E5%BA%97%20%E5%86%85%E9%83%A81.jpg');
INSERT INTO `jr_details` VALUES ('1574647725832835073', '1574647725111414786', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E5%9B%9B%E5%AD%A3%E9%85%92%E5%BA%97%20%E5%86%85%E9%83%A82.jpg');
INSERT INTO `jr_details` VALUES ('1574647726159990785', '1574647725111414786', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E5%9B%9B%E5%AD%A3%E9%85%92%E5%BA%97%20%E5%86%85%E9%83%A83.jpg');
INSERT INTO `jr_details` VALUES ('1574647726554255362', '1574647725111414786', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E5%9B%9B%E5%AD%A3%E9%85%92%E5%BA%97%20%E5%86%85%E9%83%A84.jpg');
INSERT INTO `jr_details` VALUES ('1574647726873022465', '1574647725111414786', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/Hotel/%E5%9B%9B%E5%AD%A3%E9%85%92%E5%BA%97%20%E5%86%85%E9%83%A85.jpg');

-- ----------------------------
-- Table structure for jr_festival
-- ----------------------------
DROP TABLE IF EXISTS `jr_festival`;
CREATE TABLE `jr_festival`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '节日id',
  `titile` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '节日主题',
  `introduce` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '节日的介绍',
  `img` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '节日的图片',
  `start_time` char(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '举办时间',
  `end_time` char(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '结束时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jr_festival
-- ----------------------------
INSERT INTO `jr_festival` VALUES ('1573241786051358721', '彝族年', '彝族年是流行于四川省凉山彝族自治州的民俗节日，国家级非物质文化遗产之一。\n\n彝族年，彝语称“库史”，是集祭祀祖先、游艺竞技、餐饮娱乐、服饰制度等诸多民俗事项为一体的祭祀和庆贺性民俗节日。分布于川、滇、黔、桂广大彝族地区。在每年的11月20日左右择吉日举行。凉山彝族年，主要通行于布拖、美姑、昭觉、甘洛、喜德等17个县（市），其中以昭觉、美姑、布拖、喜德等县彝族年具有代表性，富有特色 。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/festival/%E5%BD%9D%E6%97%8F%E5%B9%B4.jpg', '11月20日', '11月26日');
INSERT INTO `jr_festival` VALUES ('1573241786579841026', '火把节', '火把节（彝族火把节），流行于云南、贵州、四川等彝族地区的传统节日，国家级非物质文化遗产之一。\n火把节的起源与人们对火的崇拜有关，其目的是期望用火驱虫除害，保护庄稼生长。火把节期间，各村寨以干松木和松明子扎成大火把竖立寨中，各家门前竖起小火把，入夜点燃，村寨一片通明；同时人们手持小型火把绕行田间、住宅一周，将火把、松明子插于田间地角。青年男女在寨中大火把周围弹唱、跳舞，彻夜不息。节日期间，还有赛马、斗牛、射箭、摔跤、拔河、荡秋千等娱乐活动，并开设贸易集市。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/festival/%E7%81%AB%E6%8A%8A%E8%8A%82.jpg', '农历六月二十四', '农历六月二十四');
INSERT INTO `jr_festival` VALUES ('1573241786902802434', '跳公节', '跳公节是彝族对击败外族侵略取得胜利的一种庆祝活动。庆祝活动历史至少有上千年，后人一直沿袭下来以示纪念。 跳公节即跳弓节，彝语称为“嘈契”，意为“跳弓舞”，也称“孔够”，意为“快快乐乐，祈祷祝福”。跳弓节是居住在那坡县桂滇交界地区彝族人民一年之中最隆重的节日。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/festival/%E8%B7%B3%E5%85%AC%E8%8A%82.jpg', '农历四月初七', '农历四月初九');
INSERT INTO `jr_festival` VALUES ('1573241787297067010', '补年节', '补年节是居住在四川、云南、贵州等地的彝族居民，在新年后的农历二月初十和十一日，还要过一次年节，彝语称这个节为“麻龙火”。\n\n在祭祀活动中，以祭龙规模最大。祭龙选在二、三、四月中的一个龙日，以村寨为单位每人自带一碗米、一小块盐，由老人备香火，在龙树下集体祭祀；云南彝族则选择正月的第一个龙日进行祭龙，祭祀后大家席地而坐，不分长幼，饭自带，肉共食，是一种大规模的集会。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/festival/%E8%A1%A5%E5%B9%B4%E8%8A%82.jpg', '农历二月初十', '农历二月初十');
INSERT INTO `jr_festival` VALUES ('1573241787624222722', '花脸节', '花脸节流传于丘北县境内，于每年农历二月初八举行，历时3天。节日期间，凡彝家村寨男女青年均欢聚一堂，杀猪宰鸡，备办酒肉饭菜，先敬神灵，再以最好的酒肉请老人先吃，尔后众人才团团围坐，互相敬酒、吃肉。饭后，就到村头寨尾，用墨汁、锅烟灰互相抹脸，一直抹到满脸满身见黑，笑够累够方休。花脸节即由此得名。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/festival/%E8%8A%B1%E8%84%B8%E8%8A%82.jpg', '农历二月初八', '农历二月初十');
INSERT INTO `jr_festival` VALUES ('1573241788010098690', '二月八', '每年农历二月初八日是巍山彝族人民的年节，节日隆重而热烈。各村寨都要举行活动，其中“栅大路”、“祭密枯”是共同的活动。这天，各村寨都要砍些树枝把进入村寨的各条路口封栅起来，不准外人进入村寨。祭密枯活动在本民族内举行，并且不准妇女参加。活动过程中，都说彝族话，不讲汉语。活动先是叫“地脉”接“密枯”，然后举行祭祀密枯活动，再由阿毕（彝族宗教人员）和童子以及青壮男人守密枯树三天三夜，而打歌是各村寨必不可少的活动内容。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/festival/%E4%BA%8C%E6%9C%88%E5%85%AB.jpg', '农历二月初八', '农历二月初十');
INSERT INTO `jr_festival` VALUES ('1573241788345643009', '密士节', '密枝节也叫“祭密枝”。彝族民间传统节日。密枝节 流行于滇南彝族地区。路南的撒尼人一般在农历十一月的头一个属鼠日到属马日举行，历时七天。节日一天，民间祭祀神职人员要把密林中的祭祀场地打扫干净，在神树下布置好神坛和神门。节日的第一天，天刚亮，祭祀人员在总管见集中，然后相司扛一根竹竿在前面领路，毕摩摇着神铃跟在其后，大队人员携带家什、食物，赶着牲畜走在最后。进入密林中的祭祀场地后，人们开始忙碌起来，有的杀牲，有的烧火做饭以及饭前举行祭神仪式，祭祀以村寨为单位，人们跪在神坛前，由毕摩念经祈求神灵保佑寨子里的人平安。祭拜神灵后，人们要吃祭饭。下午，在林中摔跤娱神。晚饭后，与神灵道别而归。在七天的祭祀活动中，人们不下地干活，男子可以上山打猎，妇女在家做针线活。第一天的祭祀最隆重。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/festival/%E5%AF%86%E6%9E%9D%E8%8A%82.webp', '农历十一月的头一个属鼠日', '农历十一月的第一个属马日');
INSERT INTO `jr_festival` VALUES ('1573241788748296193', '沙户比节', '“沙户比”是彝语译音，意为小春尝新。云南巍山县龙街一带彝族的传统节日。每年小麦成熟后的农历四月中旬。届时，家家户户用麦面和糖，糯米糍粑蘸上蜂蜜献祖。已出嫁的姑娘，这天要带上糍粑回娘家，一是与亲友共贺小春丰收，同时也是请老人、亲友们尝自家的新米，分享丰收的快乐。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/festival/%E6%B2%99%E6%88%B7%E6%AF%94%E8%8A%82.png', '四月中旬', '四月中下旬');

-- ----------------------------
-- Table structure for jr_food_relation
-- ----------------------------
DROP TABLE IF EXISTS `jr_food_relation`;
CREATE TABLE `jr_food_relation`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '食物图片',
  `food_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jr_food_relation
-- ----------------------------
INSERT INTO `jr_food_relation` VALUES ('1582259505644482562', '1573240789300203522', 'https://bkimg.cdn.bcebos.com/pic/72f082025aafa40f4bfb86d1f02b144f78f0f736b322');
INSERT INTO `jr_food_relation` VALUES ('1582259508676964354', '1573240789300203522', 'https://bkimg.cdn.bcebos.com/pic/91529822720e0cf3d8e153d60346f21fbe09aa8c');
INSERT INTO `jr_food_relation` VALUES ('1582259509134143489', '1573240789300203522', 'https://bkimg.cdn.bcebos.com/pic/55e736d12f2eb938c9d767dfdc628535e5dd6f60');
INSERT INTO `jr_food_relation` VALUES ('1582259509524213761', '1573240791934226434', 'https://bkimg.cdn.bcebos.com/pic/8644ebf81a4c510fd9f907850412322dd42a28340f52');
INSERT INTO `jr_food_relation` VALUES ('1582259509918478337', '1573240791934226434', 'https://bkimg.cdn.bcebos.com/pic/ca1349540923dd54564eb7e8b542a4de9c82d158e252');
INSERT INTO `jr_food_relation` VALUES ('1582259510375657473', '1573240791934226434', 'https://bkimg.cdn.bcebos.com/pic/8d5494eef01f3a2927fc65ce9e25bc315c607cfd');
INSERT INTO `jr_food_relation` VALUES ('1582259510765727746', '1573240792336879618', 'https://bkimg.cdn.bcebos.com/pic/060828381f30e92406bdf89540086e061d95f77d');
INSERT INTO `jr_food_relation` VALUES ('1582259511159992321', '1573240792336879618', 'https://bkimg.cdn.bcebos.com/pic/e7cd7b899e510fb3738a1c33d533c895d0430c86');
INSERT INTO `jr_food_relation` VALUES ('1582259511550062593', '1573240792336879618', 'https://bkimg.cdn.bcebos.com/pic/c2cec3fdfc039245b4c88bab8b94a4c27d1e2510');
INSERT INTO `jr_food_relation` VALUES ('1582259511998853122', '1573240792664035330', 'https://pics5.baidu.com/feed/cf1b9d16fdfaaf51e609cc0454cebfe7f21f7ac2.jpeg');
INSERT INTO `jr_food_relation` VALUES ('1582259512397312001', '1573240792664035330', 'https://pics2.baidu.com/feed/1c950a7b02087bf48874fff42b497c2510dfcfb3.jpeg');
INSERT INTO `jr_food_relation` VALUES ('1582259512783187970', '1573240792664035330', 'https://pics4.baidu.com/feed/960a304e251f95cad7c8123b1b8d5437660952a5.jpeg');
INSERT INTO `jr_food_relation` VALUES ('1582259513173258242', '1573240793054105602', 'https://bkimg.cdn.bcebos.com/pic/18d8bc3eb13533fa828ba3553982ea1f4134970aed35');
INSERT INTO `jr_food_relation` VALUES ('1582259513634631681', '1573240793054105602', 'https://bkimg.cdn.bcebos.com/pic/d1160924ab18972bd40766a092846c899e510fb39c0a');
INSERT INTO `jr_food_relation` VALUES ('1582259514024701954', '1573240793054105602', 'https://bkimg.cdn.bcebos.com/pic/79f0f736afc379317abd31ffecc4b74542a9110a');
INSERT INTO `jr_food_relation` VALUES ('1582259514410577921', '1573240793381261314', 'https://bkimg.cdn.bcebos.com/pic/42166d224f4a20a4ce90a3d998529822720ed026');
INSERT INTO `jr_food_relation` VALUES ('1582259514859368450', '1573240793381261314', 'https://bkimg.cdn.bcebos.com/pic/f11f3a292df5e0febaedc97d5c6034a85fdf72d0');
INSERT INTO `jr_food_relation` VALUES ('1582259515253633025', '1573240793381261314', 'https://bkimg.cdn.bcebos.com/pic/0eb30f2442a7d933bc5da40fad4bd11372f001d0');
INSERT INTO `jr_food_relation` VALUES ('1582259515647897602', '1573240793712611329', 'https://bkimg.cdn.bcebos.com/pic/2f738bd4b31c870177b2b45e2d7f9e2f0708ff06');
INSERT INTO `jr_food_relation` VALUES ('1582259516037967873', '1573240793712611329', 'https://bkimg.cdn.bcebos.com/pic/4ec2d5628535e5dd4b0144347ec6a7efce1b6228');
INSERT INTO `jr_food_relation` VALUES ('1582259516490952705', '1573240793712611329', 'https://bkimg.cdn.bcebos.com/pic/6609c93d70cf3bc75d440e07d900baa1cc112a91');
INSERT INTO `jr_food_relation` VALUES ('1582259516885217282', '1573240794098487297', 'https://bkimg.cdn.bcebos.com/pic/d31b0ef41bd5ad6eddc4beb74f812edbb6fd52669611');
INSERT INTO `jr_food_relation` VALUES ('1582259517279481857', '1573240794098487297', 'https://bkimg.cdn.bcebos.com/pic/a044ad345982b2b7e6f535e731adcbef76099bae');
INSERT INTO `jr_food_relation` VALUES ('1582259517669552130', '1573240794098487297', 'https://bkimg.cdn.bcebos.com/pic/d52a2834349b033b5bb5c3bf278221d3d539b6001708');
INSERT INTO `jr_food_relation` VALUES ('1582259518135119874', '1573240794480168961', 'https://bkimg.cdn.bcebos.com/pic/1ad5ad6eddc451da65e4dcbcb9fd5266d0163253');
INSERT INTO `jr_food_relation` VALUES ('1582259518529384450', '1573240794480168961', 'https://bkimg.cdn.bcebos.com/pic/810a19d8bc3eb13548c1a700a81ea8d3fd1f4402');
INSERT INTO `jr_food_relation` VALUES ('1582259518919454722', '1573240794480168961', 'https://bkimg.cdn.bcebos.com/pic/b64543a98226cffc1e17933c774b5d90f603728dbca1');
INSERT INTO `jr_food_relation` VALUES ('1582259519309524994', '1573240794811518978', 'https://bkimg.cdn.bcebos.com/pic/9f2f070828381f30df386dd3a6014c086e06f0a3');
INSERT INTO `jr_food_relation` VALUES ('1582259519703789569', '1573240794811518978', 'https://bkimg.cdn.bcebos.com/pic/962bd40735fae6cdc761bbba0db30f2443a70f17');
INSERT INTO `jr_food_relation` VALUES ('1582259520156774401', '1573240794811518978', 'https://bkimg.cdn.bcebos.com/pic/a2cc7cd98d1001e9dd63a809b60e7bec54e7975d');
INSERT INTO `jr_food_relation` VALUES ('1582259520551038977', '1573240795134480386', 'https://img0.baidu.com/it/u=1357157989,2426780709&fm=253&fmt=auto&app=138&f=JPEG?w=620&h=413');
INSERT INTO `jr_food_relation` VALUES ('1582259520945303554', '1573240795134480386', 'https://img2.baidu.com/it/u=638320855,1225226614&fm=253&fmt=auto&app=138&f=JPEG?w=499&h=280');
INSERT INTO `jr_food_relation` VALUES ('1582259521339568130', '1573240795134480386', 'https://img1.baidu.com/it/u=3998013171,2628599851&fm=253&fmt=auto&app=138&f=JPEG?w=749&h=500');
INSERT INTO `jr_food_relation` VALUES ('1582259521792552961', '1573240795520356353', 'https://img1.baidu.com/it/u=1182441542,2165869675&fm=253&fmt=auto&app=138&f=JPEG');
INSERT INTO `jr_food_relation` VALUES ('1582259522170040322', '1573240795520356353', 'https://img1.baidu.com/it/u=1832933766,3092719504&fm=253&fmt=auto&app=138&f=JPEG');
INSERT INTO `jr_food_relation` VALUES ('1582259522618830850', '1573240795520356353', 'https://img2.baidu.com/it/u=2698566848,2034355693&fm=253&fmt=auto&app=138&f=JPEG');
INSERT INTO `jr_food_relation` VALUES ('1582259523008901122', '1573240795834929154', 'https://bkimg.cdn.bcebos.com/pic/f7246b600c338744ebf871a89745cef9d72a61590a86');
INSERT INTO `jr_food_relation` VALUES ('1582259523403165698', '1573240795834929154', 'https://bkimg.cdn.bcebos.com/pic/4034970a304e251f95ca8192e9d4de177f3e6709f505');
INSERT INTO `jr_food_relation` VALUES ('1582259523801624578', '1573240795834929154', 'https://bkimg.cdn.bcebos.com/pic/9358d109b3de9c82d1582ba4cbcb970a19d8bc3ee5cf');
INSERT INTO `jr_food_relation` VALUES ('1582259524262998018', '1573240796229193729', 'https://bkimg.cdn.bcebos.com/pic/f9dcd100baa1cd11728ba6167258dffcc3cec3fd87f8');
INSERT INTO `jr_food_relation` VALUES ('1582259524648873986', '1573240796229193729', 'https://bkimg.cdn.bcebos.com/pic/6a600c338744ebf81d681035daf9d72a6059a79a');
INSERT INTO `jr_food_relation` VALUES ('1582259525043138561', '1573240796229193729', 'https://bkimg.cdn.bcebos.com/pic/cc11728b4710b912e2d65258cefdfc0392452204');
INSERT INTO `jr_food_relation` VALUES ('1582259525433208833', '1573240796560543745', 'https://bkimg.cdn.bcebos.com/pic/00e93901213fb80e7bec3e4ccd9b382eb9389b503e5f');
INSERT INTO `jr_food_relation` VALUES ('1582259525886193665', '1573240796560543745', 'https://bkimg.cdn.bcebos.com/pic/342ac65c10385343fbf2697ffd5aa77eca8065382211');
INSERT INTO `jr_food_relation` VALUES ('1582259526284652545', '1573240796560543745', 'https://bkimg.cdn.bcebos.com/pic/21a4462309f790529822b0866eb8c0ca7bcb0a467f7d');
INSERT INTO `jr_food_relation` VALUES ('1582259526670528513', '1573240796950614018', 'https://img2.baidu.com/it/u=3733402545,3977125734&fm=253&fmt=auto&app=138&f=JPEG');
INSERT INTO `jr_food_relation` VALUES ('1582259527064793090', '1573240796950614018', 'https://img2.baidu.com/it/u=2549513025,4024129023&fm=253&fmt=auto&app=138&f=JPEG');
INSERT INTO `jr_food_relation` VALUES ('1582259527521972225', '1573240796950614018', 'https://img0.baidu.com/it/u=4058460838,4244493044&fm=253&fmt=auto&app=138&f=JPEG');
INSERT INTO `jr_food_relation` VALUES ('1582259527916236801', '1573240797281964033', 'https://img2.baidu.com/it/u=1272395302,1640759130&fm=253&fmt=auto&app=138&f=JPEG');
INSERT INTO `jr_food_relation` VALUES ('1582259528306307073', '1573240797281964033', 'https://img2.baidu.com/it/u=457654421,3173935834&fm=253&fmt=auto&app=138&f=JPEG');
INSERT INTO `jr_food_relation` VALUES ('1582259528759291906', '1573240797281964033', 'https://img2.baidu.com/it/u=457654421,3173935834&fm=253&fmt=auto&app=138&f=JPEG');
INSERT INTO `jr_food_relation` VALUES ('1582259529145167874', '1573240797676228610', 'https://img1.baidu.com/it/u=3213223774,1426448974&fm=253&fmt=auto&app=138&f=JPEG');
INSERT INTO `jr_food_relation` VALUES ('1582259529539432449', '1573240797676228610', 'https://img0.baidu.com/it/u=58718454,816223854&fm=253&fmt=auto&app=138&f=JPEG');
INSERT INTO `jr_food_relation` VALUES ('1582259529933697025', '1573240797676228610', 'https://img2.baidu.com/it/u=1245769555,898386984&fm=253&fmt=auto&app=138&f=JPEG');

-- ----------------------------
-- Table structure for jr_foods
-- ----------------------------
DROP TABLE IF EXISTS `jr_foods`;
CREATE TABLE `jr_foods`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL COMMENT '食物id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL COMMENT '食物名称（标题）',
  `score` double(10, 2) NULL DEFAULT NULL COMMENT '打分',
  `commend_person` int(0) NULL DEFAULT NULL COMMENT '评论人数',
  `price` double(30, 2) NOT NULL COMMENT '人均消费',
  `phone` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '电话',
  `work_time` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '营业时间',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '附近门店',
  `features` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '特色 （四川第一名菜）',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL COMMENT '图片url',
  `command` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL COMMENT '食物推荐语  只是针对水果',
  `state` int(0) NOT NULL DEFAULT 1 COMMENT '状态（0为水果 ，1为食物）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_german2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jr_foods
-- ----------------------------
INSERT INTO `jr_foods` VALUES ('1572870393666998274', '丑橘', 4.10, 100, 14.00, NULL, NULL, NULL, '汁水甜美', 'http://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/%E4%B8%91%E6%A9%98.jpg', '丑拒又名不知火，不知火（学名：Citrus reticulata \'Bu Zhi Huo\'）是芸香科、柑橘属、柑橘种下的杂柑品种。果实呈倒卵形，果皮黄橙色，果面粗糙，易剥皮，有碰柑香味，单果重200～280克，果汁糖度在13%以上，最高达17%，次年2～3月成熟。花果同株，两代同堂，一年而生。当年3～4月开花，10月上旬开始着色，12月上旬完全着色，果实留树储藏，到次年3～4月又同时开花结果，果肉清脆化渣，风味较好。<br>不知火果肉富含维生素C和人体所需的微量元素A及碳水化合物、胡萝卜素、钙、磷、铁等，口味甜度适中，营养价值比同类水果高出很多，可以生津止渴，化痰理气，具有着高营养值，不知火的皮晒干后可以入药，放在冰箱里可以除臭。', 0);
INSERT INTO `jr_foods` VALUES ('1572870394182897666', '大枣', 4.50, 24, 13.00, NULL, NULL, NULL, '外观独特', 'http://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/%E5%A4%A7%E6%9E%A3.jpg', '枣（学名：Ziziphus jujuba Mill.），别称枣子，大枣、刺枣，贯枣。鼠李科枣属植物，落叶小乔木，稀灌木，高达10余米，树皮褐色或灰褐色，叶柄长1-6毫米，或在长枝上的可达1厘米，无毛或有疏微毛，托叶刺纤细，后期常脱落。花黄绿色，两性，无毛，具短总花梗，单生或密集成腋生聚伞花序。核果矩圆形或是长卵圆形，长2-3.5厘米，直径1.5-2厘米，成熟后由红色变红紫色，中果皮肉质、厚、味甜。种子扁椭圆形，长约1厘米，宽8毫米。<br>枣含有丰富的维生素C、维生素P，除供鲜食外，常可以制成蜜枣、红枣、熏枣、黑枣、酒枣、牙枣等蜜饯和果脯，还可以作枣泥、枣面、枣酒、枣醋等，为食品工业原料。', 0);
INSERT INTO `jr_foods` VALUES ('1572870394564579330', '早红宝石', 4.50, 54, 32.00, NULL, NULL, NULL, '汁水甜美', 'http://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/%E6%97%A9%E7%BA%A2%E5%AE%9D%E7%9F%B3.jpg', '早红宝石樱桃是大果樱桃，一般的果实重量都有10克左右，最大的果实可以达到18克左右，可以说是樱桃中的“巨婴”，核小肉多，口感极佳，非常受欢迎。', 0);
INSERT INTO `jr_foods` VALUES ('1572870394958843906', '核桃', 4.50, 200, 13.00, NULL, NULL, NULL, '补脑神器', 'http://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/%E6%A0%B8%E6%A1%83.jpg', '核桃（拉丁学名：Juglans regia L.，别名：胡桃），胡桃科胡桃属植物。[1]分布于中亚、西亚、南亚和欧洲。核桃树一般高达3~5米，树皮灰白色，幼枝先端具细柔毛，树冠广阔，树皮幼时灰绿色，小枝无毛，具有光泽，羽状复叶长25~50厘米，椭圆状卵形至椭圆形；基部圆或楔形，奇数羽状复叶长25~30厘米，雄柔荑花序长5~10厘米，雌花1~3朵聚生，花期5月，果实椭圆形。它喜光，耐寒，适应多种土壤生长，喜肥沃湿润的沙质壤土。', 0);
INSERT INTO `jr_foods` VALUES ('1572870395281805314', '桑葚', 4.30, 520, 18.00, NULL, NULL, NULL, '汁水甜美', 'http://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/%E6%A1%91%E8%91%9A.jpg', '桑（拉丁名：Morus alba L.）是桑科，桑属 落叶乔木或灌木，高可达15米。树体富含乳浆，树皮黄褐色。叶卵形至广卵形，叶端尖，叶基圆形或浅心脏形，边缘有粗锯齿，有时有不规则的分裂。叶面无毛，有光泽，叶背脉上有疏毛。雌雄异株，5月开花，葇荑花序。果熟期6-7月，聚花果卵圆形或圆柱形，黑紫色或白色。喜光，幼时稍耐阴。喜温暖湿润气候，耐寒。耐干旱，耐水湿能力强,味道较甜。', 0);
INSERT INTO `jr_foods` VALUES ('1572870395613155329', '梨', 4.20, 111, 15.00, NULL, NULL, NULL, '汁水甜美', 'http://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/%E6%A2%A8.jpg', '梨，通常品种是一种落叶乔木或灌木，极少数品种为常绿，属于被子植物门双子叶植物纲蔷薇科苹果亚科。叶片多呈卵形，大小因品种不同而各异。花为白色，或略带黄色、粉红色，有五瓣。果实形状有圆形的，也有基部较细尾部较粗的，即俗称的“梨形”；不同品种的果皮颜色大相径庭，有黄色、绿色、黄中带绿、绿中带黄、黄褐色、绿褐色、红褐色、褐色，个别品种亦有紫红色；野生梨的果径较小，在1到4厘米之间，而人工培植的品种果径可达8厘米，长度可达18厘米。<br>含有多种维生素和纤维素，不同种类的梨味道和质感都完全不同。梨既可生食，也可蒸煮后食用。在医疗功效上，梨可以通便秘，利消化，在民间，梨还有一种疗效，把梨去核，放入冰糖，蒸煮过后食用还可以止咳；除了作为水果食用以外，梨还可以作观赏之用。', 0);
INSERT INTO `jr_foods` VALUES ('1572870396003225601', '橙子', 4.00, 376, 20.00, NULL, NULL, NULL, '夏日解暑', 'http://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/%E6%A9%99%E5%AD%90.jpg', '橙子（学名：Citrus sinensis 英语：orange），是芸香科柑橘属植物橙树的果实，亦称为黄果、柑子、金环、柳丁。适合在天气较热的品尝，水分充足！', 0);
INSERT INTO `jr_foods` VALUES ('1572870396334575618', '沙棘', 3.80, 70, 45.00, NULL, NULL, NULL, '口感奇特', 'http://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/%E6%B2%99%E6%A3%98.jpg', '沙棘（拉丁学名：Hippophae rhamnoides Linn.），是一种胡颓子科、沙棘属落叶性灌木，其特性是耐旱、抗风沙，可以在盐碱化土地上生存，因此被广泛用于水土保持。中国西北部大量种植沙棘，用于沙漠绿化。沙棘果实中维生素C含量高，素有维生素C之王的美称。沙棘是植物和其果实的统称。植物沙棘为胡颓子科沙棘属，是一种落叶性灌木。<br>沙棘果实营养丰富，据测定其果实中含有多种维生素、脂肪酸、微量元素、亚油素、沙棘黄酮、超氧化物等活性物质和人体所需的各种氨基酸。用沙棘叶可制作保健茶。', 0);
INSERT INTO `jr_foods` VALUES ('1572870396716257281', '甘蔗', 4.80, 276, 12.00, NULL, NULL, NULL, '汁水丰富', 'http://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/%E7%94%98%E8%94%97.jpg', '甘蔗是一种一年生或多年生热带和亚热带草本植物，属C4作物。甘圆柱形茎直立、分蘖、丛生、有节，节上有芽；节间实心，外被有蜡粉，有紫、红或黄绿色等；叶子丛生，叶片有肥厚白色的中脉；大型圆锥花序顶生，小穗基部有银色长毛，长圆形或卵圆形颖果细小。<br>现代医学研究表明，甘蔗中含有丰富的糖分、水分，此外，还含有对人体新陈代谢非常有益的各种维生素、脂肪、蛋白质、有机酸、钙、铁等物质。甘蔗不但能给食物增添甜味，而且还可以提供人体所需的营养和热量。含蛋白质、脂肪、糖类、钙、磷、铁、天门冬素、天门冬氨酸、谷氨酸、丝氨酸、丙氨酸、缬氨酸、亮氨酸、正这氨酸、赖氨酸、羟丁氨酸、谷氨酰胺、脯氨酸、酪氨酸、胱氨酸、苯丙氨酸、γ-氨基丁酸等多种氨基酸，延胡索酸、琥珀酸、甘醇酸、苹果酸、柠檬酸、草酸等有机酸及维生素B1、B2、B6、C。榨去汁的甘蔗渣中，含有对小鼠艾氏癌和肉瘤180有抑制作用的多糖类。', 0);
INSERT INTO `jr_foods` VALUES ('1572870397051801602', '盐源苹果', 4.40, 329, 18.00, NULL, NULL, NULL, '汁水丰富', 'http://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/%E7%9B%90%E6%BA%90%E8%8B%B9%E6%9E%9C.jpg', '西昌得天独厚的光热自然条件和原生态无污染的自然环境，培育了“盐源苹果”具有无公害、色泽厚重、香味浓郁、肉质细脆、汁多爽口，个大形正，果面光洁、可溶性固形物含量高和成熟上市早九大独具高原苹果品质特点。', 0);
INSERT INTO `jr_foods` VALUES ('1572870397383151617', '石榴', 4.50, 400, 10.00, NULL, NULL, NULL, '籽籽饱满', 'http://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/%E7%9F%B3%E6%A6%B4.jpg', '石榴在成熟后变成大型而多室、多子的浆果，每室内有多数子粒；外种皮肉质，呈鲜红、淡红或白色，多汁，甜而带酸，即为可食用的部分；内种皮为角质，也有退化变软的，即软籽石榴。并且石榴适宜口干舌燥者、腹泻者、扁桃体发炎者，但便秘、尿道炎患者、糖尿病、实热积滞者禁食。', 0);
INSERT INTO `jr_foods` VALUES ('1572870397769027586', '纸皮核桃', 4.50, 21, 7.00, NULL, NULL, NULL, '补脑神器', 'http://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/%E7%BA%B8%E7%9A%AE%E6%A0%B8%E6%A1%83.jpg', '纸皮核桃因皮薄如纸、易取整仁而得名，又名露仁核桃，无性系，分布海拔1700～2400米，营养丰富。中国纸皮核桃主要产地分部在新疆和云南，又以新疆阿克苏的产品为佳。纸皮核桃中含有丰富的蛋白质、钙、磷、铁等矿物质和微量元素及维生素, 具有极高的营养价值, 因而深受广大人民的欢迎。近年来, 核桃消费量逐年上升, 我国在西昌市的核桃产量也在不断增加。', 0);
INSERT INTO `jr_foods` VALUES ('1572870398096183297', '苹果', 4.50, 310, 14.00, NULL, NULL, NULL, '汁水丰富', 'http://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/%E8%8B%B9%E6%9E%9C.jpg', '普通苹果是很常见的水果，不仅口感甘甜，还十分的有营养，也很容易被人体所吸收。', 0);
INSERT INTO `jr_foods` VALUES ('1572870398490447873', '草莓', 5.00, 280, 27.00, NULL, NULL, NULL, '汁水丰富', 'http://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/%E8%8D%89%E8%8E%93.jpg', '草莓喜温凉气候，草莓根系生长温度5-30℃，适温15-22℃，茎叶生长适温为20-30℃，因此西昌得天独厚具有大面积种植的能力。<br>又因为草莓营养价值丰富，被誉为是“水果皇后”，含有丰富的维生素C、维生素A、维生素E、维生素PP、维生素B1、维生素B2、胡萝卜素、鞣酸、天冬氨酸、铜、草莓胺、果胶、纤维素、叶酸、铁、钙、鞣花酸与花青素等营养物质。', 0);
INSERT INTO `jr_foods` VALUES ('1572870398817603585', '莫利樱桃', 3.10, 123, 35.00, NULL, NULL, NULL, '籽籽饱满', 'http://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/%E8%8E%AB%E5%88%A9%E6%A8%B1%E6%A1%83.jpg', '该果实平均单果重8.6克，肾脏形。果梗中短，很少裂果。果皮鲜红色，完熟时淡紫红色，有光泽。果肉较硬，粉红色，肥厚多汁，风味酸甜可口，品质上，离核。可食率92.98%，可溶性固形物含量17.68%，总糖12.1%，总酸0.66%。并且该品种因具有早熟、果大、色艳、质优等特点，所以被选为综合性状优良的早熟甜樱桃品种。', 0);
INSERT INTO `jr_foods` VALUES ('1572870399153147906', '青刺果', 3.30, 321, 28.00, NULL, NULL, NULL, '汁水丰富', 'http://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/%E9%9D%92%E5%88%BA%E6%9E%9C.jpg', '青刺果，是一种多年生的稀有木本油料植物，其油含有棕榈酸、硬脂酸、油酸、亚油酸、亚麻酸、氨基酸、花生稀酸。是一种较理想的绿色保健食品，同时还可以作为化妆品及其它用品。青刺果素是从无污染、原生态的高原野生青刺果中萃取的浓缩植物精油。', 0);
INSERT INTO `jr_foods` VALUES ('1573240789300203522', '开水白菜', 3.40, 500, 35.00, '13538983878', '早上10 -- 晚上11', '四川饭店（海韵路）', '特色老字号', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E5%BC%80%E6%B0%B4%E7%99%BD%E8%8F%9C.jpg', '开水白菜，是目前川菜的真正头把交椅，为不辣川菜的典型代表。更是入选国宴，为川菜正名：不是所有的川菜，都只为接地气而生。相传，开水白菜是由颇受慈禧赏识的川菜名厨黄敬临在清宫御膳房创制的。黄敬临当厨时，不少人贬损川菜“只会麻辣，粗俗土气”，为了破谣立证，他冥思苦想多时并经由百番尝试，终于开先河地创出了“开水白菜”这道菜中极品，把极繁和极简归至化境，一扫川菜积郁百年的冤屈。', 1);
INSERT INTO `jr_foods` VALUES ('1573240791934226434', '蚂蚁上树', 4.10, 532, 27.00, '13539449413', '早上10 -- 下午2:00', '西城区西单西绒线胡同51号', '特色老字号', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E8%9A%82%E8%9A%81%E4%B8%8A%E6%A0%91.jpg', '蚂蚁上树（Sauteed Vermicelli with minced Pork），又名肉末粉条，是四川省的特色传统名菜之一。因肉末贴在粉丝上，形似蚂蚁爬在树枝上而得名。这道菜具体的历史，已不可考。通常由粉丝（或者粉条）、肉末为主料，辅以胡萝卜、姜、葱、豆瓣酱等辅料制作而成。成菜后，口味清淡，爽滑美味，色泽红亮，食之别有风味。据说，蚂蚁上树这道菜的菜名，跟关汉卿笔下的窦娥有关。窦娥用小块猪肉切末后，加粉丝炒好后给婆婆吃，婆婆除了赞叹好吃，还给这道炒粉丝起了个形象的名：“蚂蚁上树”。', 1);
INSERT INTO `jr_foods` VALUES ('1573240792336879618', '东坡肘子', 4.30, 621, 52.00, '13411191110', '早上10 -- 晚上11', '便利蜂(西绒线胡同店)', '特色美食', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E4%B8%9C%E5%9D%A1%E8%82%98%E5%AD%90.jpg', '东坡肘子，四川省眉山市特产，国家地理标志产品。\n东坡肘子有肥而不腻、粑而不烂的特点，色、香、味、形俱佳 ，东坡肘子汤汁乳白，雪豆粉白，猪肘肥软适口，原汁原味，香气四溢。\n2013年12月，东坡肘子成功获批国家地理标志保护产品称号。', 1);
INSERT INTO `jr_foods` VALUES ('1573240792664035330', '麻婆豆腐', 4.10, 555, 12.00, '13533139349', '早上10 -- 下午2:00', '中国联通(西绒线胡同店)', '特色老字号', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E9%BA%BB%E5%A9%86%E8%B1%86%E8%85%90.jpg', '要说中国菜出国最多的，恐怕除了番茄炒蛋以外就应该是这道麻婆豆腐了。美、英、法、日、澳等国，皆能看到它的身影。陈麻婆在发明这道菜时，本是以下饭、实惠为初衷，可能她自己都没想到，这道菜如今竟已经踏入了国际的圈子。2018年9月，麻婆豆腐被评为四川十大经典名菜。', 1);
INSERT INTO `jr_foods` VALUES ('1573240793054105602', '鱼香肉丝', 4.20, 754, 22.00, '13570555168', '早上10 -- 下午2:00', '西单西绒线胡同10号楼4号', '特色风味小吃', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E9%B1%BC%E9%A6%99%E8%82%89%E4%B8%9D.jpg', '十大川菜代表之一。没有一丝鱼材，但却充满鱼味，做工要求较高，集色香味俱全，历史更是可以追溯至上个世纪初。2018年9月，鱼香肉丝被评为四川十大经典名菜', 1);
INSERT INTO `jr_foods` VALUES ('1573240793381261314', '蒜泥白肉', 4.40, 777, 19.00, '13434321319', '07：50——午市结束（早班）16：30——晚市结束（晚班）', '晨光文具(西绒线店)', '特色风味小吃', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E8%92%9C%E6%B3%A5%E7%99%BD%E8%82%89.jpg', '蒜泥白肉是一道中国传统菜品，属于川菜菜系，制作原料主要有蒜泥、肉等，口味鲜美，营养丰富，食时用筷拌合，随着热气，一股酱油、辣椒油和大蒜组合的香味直扑鼻端，使人食欲大振。蒜味浓厚，肥而不腻。', 1);
INSERT INTO `jr_foods` VALUES ('1573240793712611329', '芙蓉鸡片', 4.50, 1270, 25.00, '13503059898', '上班时间：09：30——午市收市（早班）16：30——晚市收市（晚班）', '阿斯牛牛风情餐厅(民族大道店)', '特色老字号', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E8%8A%99%E8%93%89%E9%B8%A1%E7%89%87.jpg', '芙蓉鸡片，是一道鲁菜名菜，成名后淮扬菜、川菜等菜系都有该菜品。通常都是以鸡脯肉、鸡蛋等食材制作而成。成菜后，肉片色泽洁白，软嫩滑香，形如芙蓉。', 1);
INSERT INTO `jr_foods` VALUES ('1573240794098487297', '跷脚牛肉', 4.70, 1500, 48.00, '13527782868', '8点半～13点半', '冕宁小丁饭店(西昌店)', '正宗国料榜', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E8%B7%B7%E8%84%9A%E7%89%9B%E8%82%89.jpg', '跷脚牛肉是四川省乐山市的一道特色传统名菜，属于川菜系；其制作原料主要有牛肉，牛舌，牛肝，牛脊髓，牛蹄筋等。色香味美，入口顺滑，鲜美无比。', 1);
INSERT INTO `jr_foods` VALUES ('1573240794480168961', '酸辣鸭血', 3.80, 354, 28.00, '17688878884', '4点半～9点', '冕林土菜馆(海滨北路店)', '正宗国料榜', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E9%85%B8%E8%BE%A3%E9%B8%AD%E9%9B%AA.jpg', '酸辣鸭血是一道中国名菜，属于川菜。色泽红润，鸭血旺鲜嫩，酸辣爽口。', 1);
INSERT INTO `jr_foods` VALUES ('1573240794811518978', '简阳羊肉汤', 4.70, 532, 35.00, '13113319141', '07：50——午市结束（早班）16：30——晚市结束（晚班）', '冕宁小丁饭店(西昌店)', '特色风味小吃', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E7%AE%80%E9%98%B3%E7%BE%8A%E8%82%89%E6%B1%A4.jpg', '简阳羊肉汤是成都简阳特有的美食，其汤鲜，味美，香气宜人，是上等的补气养生的汤类美食。简阳羊肉汤的独特，主要得益于简阳山羊品种的不断改良和羊肉汤制作工艺的不断创新。简阳大耳羊由美国努比羊与简阳土羊杂交形成。独特的品种优势和良好的自然生存条件，铸就了其特有的细嫩肉质。以此作为主要原料的简阳羊肉汤，再加上被列为成都市非物质文化遗产的简阳羊肉汤烹饪技艺，自然汤鲜味美，香气四溢，让人赞不绝口，留恋忘返。\n也正是凭借这独特的味道，2008年简阳羊肉汤才在几万个参赛菜品中脱颖而出，入选全国30道奥运健康美食，成为其中唯一以羊肉为主料的汤类菜品。', 1);
INSERT INTO `jr_foods` VALUES ('1573240795134480386', '宫爆鸡丁', 4.80, 491, 28.00, '13113343431', '上班时间：09：30——午市收市（早班）16：30——晚市收市（晚班）', '西昌美丽山水大酒店(餐饮部)', '特色风味小吃', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E5%AE%AB%E7%88%86%E9%B8%A1%E4%B8%81.jpg', '选用鸡胸肉、花生米、黄瓜以及各种辅料所烹饪而成的宫保鸡丁，无疑是川菜里的一张响亮名片。它最大的特点早就被当成了顺口溜：红而不辣、辣而不猛、香辣味浓、肉质滑脆。2018年底，宫保鸡丁被评定为十大经典川菜。', 1);
INSERT INTO `jr_foods` VALUES ('1573240795520356353', '麻辣水煮鱼', 4.60, 743, 48.00, '13250179555', '8点半～13点半', '西昌美丽山水大酒店(餐饮部)', '热门水煮类', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E9%BA%BB%E8%BE%A3%E6%B0%B4%E7%85%AE%E9%B1%BC.jpg', '麻辣水煮鱼是川渝地区的一道色香味俱全的名肴，属于川菜。此菜肉质细嫩，麻辣不燥，鲜香醇厚，回味无穷。\n原料选新鲜生猛活鱼，又充分发挥辣椒御寒、益气养血功效，烹调出来的肉质一点也不会变韧，口感滑嫩，油而不腻。既去除了鱼的腥味，又保持了鱼的鲜嫩。满目的辣椒红亮养眼，辣而不燥，麻而不苦。', 1);
INSERT INTO `jr_foods` VALUES ('1573240795834929154', '辣子鸡', 4.30, 629, 32.00, '13113333007', '4点半～9点', '鸡毛店(一环路东一段店)', '热门凉拌类', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E8%BE%A3%E5%AD%90%E9%B8%A1.jpg', '辣子鸡（汉语拼音字母： là zǐ jī、汉语注音符号：ㄌㄚˋ ㄗˇ ㄐㄧˉ）是一道经典的家常菜，一般以整鸡为主料，加上葱、干辣椒、花椒、盐、胡椒、味精等多种材料精制而成，虽然是同一道菜，各地制作也各有特色。\n辣子鸡因各地的不同制作方法也有不同的特色，深受各地人们的喜爱。此菜成菜色泽棕红油亮，麻辣味浓。', 1);
INSERT INTO `jr_foods` VALUES ('1573240796229193729', '水煮肉片', 4.10, 1800, 26.00, '13202067788', '早上5点半～13点半', '彝珍野菌川菜酒楼', '特色风味小吃', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E6%B0%B4%E7%85%AE%E8%82%89%E7%89%87.jpg', '水煮肉片（Poached spicy slices of pork）是以猪里脊肉为主料的一道地方新创名菜，起源于四川地区-四川省-自贡市，发扬于西南，属于川菜中著名的家常菜。因肉片未经划油，以水煮熟故名水煮肉片。水煮肉片肉味香辣，软嫩，易嚼。\n吃时肉嫩菜鲜 ，汤红油亮，麻辣味浓，最宜下饭，为家常美食之一；特色是“麻、辣、鲜、香”', 1);
INSERT INTO `jr_foods` VALUES ('1573240796560543745', '清蒸江团', 4.20, 400, 40.00, '13144451913', '早6:00—10:00 午11:00—午14:00', '定西市陇西县长安路与交通路交汇处', '热门水煮类', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E6%B8%85%E8%92%B8%E6%B1%9F%E5%9B%A2.jpg', '作为上河帮派川菜的重要组成，乐山贡献了一道经典的清蒸江团。江团鱼、火腿作为原料，采用清蒸的烹饪方式，成品肥美细嫩，汤清味鲜。2018年9月，清蒸江团成为四川十大经典菜之一。', 1);
INSERT INTO `jr_foods` VALUES ('1573240796950614018', '凉拌菜', 4.90, 2000, 14.00, '18122000009', '早上5点半～13点半', '蜀焱火锅(文峰店)', '特色风味小吃', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E5%87%89%E6%8B%8C%E8%8F%9C.jpg', '凉拌菜，是将初步加工和焯水处理后的原料，经过添加红油、酱油、蒜粒等配料制作而成的菜肴。根据红油的分类一般可分为香辣、麻辣、五香三大类。\n凉拌菜其历史文化却深远得多，可追溯到周朝和先秦时期。每一道凉菜，吃的不仅仅是食物的本身，调味料才是灵魂所在。糖、香油、醋、盐、辣椒油等调味的多或少，赋予了每一道凉菜不同的味道。吃前将各种食材连同酱汁拌均匀，酸、辣、甜、麻香味儿在口腔中散发开来，醒胃又养生。', 1);
INSERT INTO `jr_foods` VALUES ('1573240797281964033', '素菜拼盘', 3.70, 287, 22.00, '18922141913', '早6:00—10:00 午11:00—午14:00', '川渝食府(恒宜居店)', '正宗本帮菜', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E7%B4%A0%E8%8F%9C%E6%8B%BC%E7%9B%98.jpg', '海带、香菇、黄瓜、木耳等素菜的凉拌组合', 1);
INSERT INTO `jr_foods` VALUES ('1573240797676228610', '韭菜炒蛋', 4.40, 521, 12.00, '18933919413', '午11:00—午14:00 晚17:00—22:00', '好口福川菜馆', '正宗本帮菜', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/food/eat/%E9%9F%AD%E8%8F%9C%E7%82%92%E8%9B%8B.jpg', '韭菜中含有大量的维生素和矿物质，鸡蛋也是含有丰富营养的食材，这两种食材搭配在一起，真的是美味又营养。韭菜的含水量很高（85%），能量较低。它是铁、钾和维生素A的上等来源，也是维生素C的一般来源。韭菜还有刺激食欲的作用。韭菜含有较多的粗纤维，能增进胃肠蠕动，有效预防习惯性便秘和肠癌，有“洗肠草”之称。\n韭菜含有挥发性精油及含硫化合物，具有促进食欲的作用。同时，可能有助于血脂的调节，适量进食对高血压、冠心病、高血脂等有一定益处。它含有硫化合物，具有一定杀菌消炎作用。', 1);

-- ----------------------------
-- Table structure for jr_guide
-- ----------------------------
DROP TABLE IF EXISTS `jr_guide`;
CREATE TABLE `jr_guide`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '1' COMMENT 'id',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `sex` int(0) NOT NULL COMMENT '性别  0为女  1为男生',
  `age` int(0) NULL DEFAULT NULL COMMENT '年龄',
  `phone` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机电话号吗',
  `exp_year` int(0) NULL DEFAULT NULL COMMENT '工龄',
  `img` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '个人图片',
  `introduce` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '个人介绍',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jr_guide
-- ----------------------------
INSERT INTO `jr_guide` VALUES ('1574342482911625217', '赵宇', 1, 32, '18011585049', 4, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/guider/a.jpg', '责任心强，路熟经验高，广受好评');
INSERT INTO `jr_guide` VALUES ('1574342485340127233', '姜文', 1, 35, '18011585041', 4, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/guider/x.jpg', '责任心强，路熟经验高，广受好评');
INSERT INTO `jr_guide` VALUES ('1574342485667282945', '李娜', 0, 30, '18011582349', 3, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/guider/15.jpg', '人美声甜，讲解清晰，经验丰富，广受好评');
INSERT INTO `jr_guide` VALUES ('1574342485986050050', '李特', 0, 29, '18011515049', 2, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/guider/12jpg.jpg', '通情热心，讲解全面，有良好的带队经验，能及时处理突发状况');
INSERT INTO `jr_guide` VALUES ('1574342486317400066', '王欢', 0, 33, '18011385049', 4, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/guider/4jpg.jpg', '通情热心，讲解全面，有良好的带队经验，能及时处理突发状况');
INSERT INTO `jr_guide` VALUES ('1574342486644555777', '杜刚', 1, 34, '13882063714', 3, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/guider/8.png', '通情热心，讲解全面，有良好的带队经验，能及时处理突发状况');
INSERT INTO `jr_guide` VALUES ('1574342486975905793', '王铭', 1, 44, '13882163714', 5, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/guider/f.jpg', '热情心细，工作认真，讲解能力一流，为模范导游');
INSERT INTO `jr_guide` VALUES ('1574342487235952641', '龙鑫', 0, 35, '13882263714', 4, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/guider/10.png', '热情心细，工作认真，讲解能力一流，为模范导游');
INSERT INTO `jr_guide` VALUES ('1574342487554719746', '云韵', 0, 27, '13882063914', 2, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/guider/11.png', '人美声甜，讲解清晰，经验丰富，广受好评');
INSERT INTO `jr_guide` VALUES ('1574342487877681154', '蒋欣', 0, 26, '18011584049', 2, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/guider/5.jpg', '人美声甜，讲解清晰，经验丰富，广受好评');
INSERT INTO `jr_guide` VALUES ('1574342488213225474', '马克', 1, 30, '18011581049', 2, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/guider/16.jpg', '责任心强，路熟经验高，广受好评');
INSERT INTO `jr_guide` VALUES ('1574342488544575490', '王通', 0, 32, '18011584249', 2, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/guider/13.jpg', '责任心强，路熟经验高，广受好评');
INSERT INTO `jr_guide` VALUES ('1574342488871731201', '余其一', 1, 30, '18011584019', 3, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/guider/1.jpg', '责任心强，路熟经验高，广受好评');
INSERT INTO `jr_guide` VALUES ('1574342489194692610', '郑鹏', 1, 29, '18012584049', 2, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/guider/14.jpg', '责任心强，路熟经验高，广受好评');
INSERT INTO `jr_guide` VALUES ('1574342489530236930', '王蒙', 0, 29, '18011474049', 3, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/guider/2.png', '责任心强，路熟经验高，广受好评');
INSERT INTO `jr_guide` VALUES ('1574342489857392641', '徐欣', 1, 27, '18011324049', 2, 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/guider/7.png', '责任心强，路熟经验高，广受好评');

-- ----------------------------
-- Table structure for jr_recreational
-- ----------------------------
DROP TABLE IF EXISTS `jr_recreational`;
CREATE TABLE `jr_recreational`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL COMMENT '景点休闲区id',
  `recreational_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL COMMENT '休闲区的图片url',
  `senery_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '景点id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_german2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jr_recreational
-- ----------------------------
INSERT INTO `jr_recreational` VALUES ('1575744008240762881', 'https://dimg04.c-ctrip.com/images/0301u120008iih0l8E172_C_750_420_Q80.jpg', '1574765015358365698');
INSERT INTO `jr_recreational` VALUES ('1575744010899951618', 'https://dimg04.c-ctrip.com/images/0300i120008igs4x8D77D_C_750_420_Q80.jpg', '1574765015358365698');
INSERT INTO `jr_recreational` VALUES ('1575744011222913026', 'https://dimg04.c-ctrip.com/images/0303y120009lgl6d01EE4_C_750_420_Q80.jpg', '1574765015358365698');
INSERT INTO `jr_recreational` VALUES ('1575744011545874433', 'https://dimg04.c-ctrip.com/images/0302o120009lglei61BE7_C_750_420_Q80.jpg', '1574765015358365698');
INSERT INTO `jr_recreational` VALUES ('1575744011868835842', 'https://dimg04.c-ctrip.com/images/0301b120008k81hjc2F96_C_750_420_Q80.jpg', '1574765015677132802');
INSERT INTO `jr_recreational` VALUES ('1575744012191797250', 'https://dimg04.c-ctrip.com/images/0301b120008k81hjc2F96_C_750_420_Q80.jpg', '1574765015677132802');
INSERT INTO `jr_recreational` VALUES ('1575744012514758657', 'https://dimg04.c-ctrip.com/images/0301h120008k81fn922DA_C_750_420_Q80.jpg', '1574765015677132802');
INSERT INTO `jr_recreational` VALUES ('1575744012774805505', 'https://dimg04.c-ctrip.com/images/03007120008k81alq32C8_C_750_420_Q80.jpg', '1574765015677132802');
INSERT INTO `jr_recreational` VALUES ('1575744013101961218', 'https://dimg04.c-ctrip.com/images/0305f120008p9wo9dF612_C_750_420_Q80.jpg', '1574765016004288514');
INSERT INTO `jr_recreational` VALUES ('1575744013424922625', 'https://dimg04.c-ctrip.com/images/03057120008hon6fw2A26_C_750_420_Q80.jpg', '1574765016004288514');
INSERT INTO `jr_recreational` VALUES ('1575744013752078337', 'https://dimg04.c-ctrip.com/images/0306v120008hookvt3E2B_C_750_420_Q80.jpg', '1574765016004288514');
INSERT INTO `jr_recreational` VALUES ('1575744014075039746', 'https://dimg04.c-ctrip.com/images/0306g120008hon97cF4A7_C_750_420_Q80.jpg', '1574765016004288514');
INSERT INTO `jr_recreational` VALUES ('1575744014402195458', 'https://dimg04.c-ctrip.com/images/0302q120009mgtkck7069_C_750_420_Q80.jpg', '1574765016323055617');
INSERT INTO `jr_recreational` VALUES ('1575744014725156865', 'https://dimg04.c-ctrip.com/images/0301u120009d10jij5CCA_C_750_420_Q80.jpg', '1574765016323055617');
INSERT INTO `jr_recreational` VALUES ('1575744015048118273', 'https://dimg04.c-ctrip.com/images/0301u120009d0zyp47156_C_750_420_Q80.jpg', '1574765016323055617');
INSERT INTO `jr_recreational` VALUES ('1575744015371079682', 'https://dimg04.c-ctrip.com/images/03073120009d0y1nf6F78_C_750_420_Q80.jpg', '1574765016323055617');
INSERT INTO `jr_recreational` VALUES ('1575744015689846785', 'https://dimg04.c-ctrip.com/images/0304h12000832amxmF51D_C_750_420_Q80.jpg', '1574765016650211330');
INSERT INTO `jr_recreational` VALUES ('1575744016017002498', 'https://dimg03.c-ctrip.com/images/fd/tg/g2/M01/88/…/CghzgVWwvmWAYSRIAAaC9S5KHAk868_C_750_420_Q80.jpg', '1574765016650211330');
INSERT INTO `jr_recreational` VALUES ('1575744016348352513', 'https://dimg03.c-ctrip.com/images/fd/tg/g2/M02/89/…/CghzgFWwvgKAZ0ehAAt-Z-nFq5E625_C_750_420_Q80.jpg', '1574765016650211330');
INSERT INTO `jr_recreational` VALUES ('1575744016667119617', 'https://dimg03.c-ctrip.com/images/fd/tg/g2/M07/88/…/CghzgVWwwXWAetCmABr9RDZrdCQ369_C_750_420_Q80.jpg', '1574765016650211330');
INSERT INTO `jr_recreational` VALUES ('1575744016994275329', 'https://dimg04.c-ctrip.com/images/03027120008bi7ipfDA08_C_750_420_Q80.jpg', '1574765016968978434');
INSERT INTO `jr_recreational` VALUES ('1575744017317236737', 'https://dimg04.c-ctrip.com/images/03013120008a4rngcDC3F_C_750_420_Q80.jpg', '1574765016968978434');
INSERT INTO `jr_recreational` VALUES ('1575744017640198146', 'https://dimg04.c-ctrip.com/images/0306p120008bi7nda8C78_C_750_420_Q80.jpg', '1574765016968978434');
INSERT INTO `jr_recreational` VALUES ('1576816466861277186', 'http://gallery.youxiake.com/Public/Data/upload/productimg/201803/09/5aa200faacf73.jpg?imageslim', '1574765011000483841');
INSERT INTO `jr_recreational` VALUES ('1576816469189115906', 'http://gallery.youxiake.com/Public/Data/upload/productimg/202103/31/60643344ae0d7.jpg?imageslim', '1574765011000483841');
INSERT INTO `jr_recreational` VALUES ('1576816469520465921', 'https://gallery.youxiake.com/product/line/202107/13/02f59fa1f1051b86.jpg?imageslim', '1574765011000483841');
INSERT INTO `jr_recreational` VALUES ('1576816469839233026', 'https://gallery.youxiake.com/product/line/202107/13/ac713e58d96669e1.jpg?imageslim', '1574765011000483841');
INSERT INTO `jr_recreational` VALUES ('1576816470162194434', 'http://gallery.youxiake.com/Public/Data/upload/productimg/202103/31/606432d15e5e2.jpg?imageslim', '1574765011000483841');
INSERT INTO `jr_recreational` VALUES ('1576816470485155841', 'http://gallery.youxiake.com/Public/Data/upload/productimg/201710/24/59eeaa9f4fd9e.jpg?imageslim', '1574765011453468673');
INSERT INTO `jr_recreational` VALUES ('1576816470812311553', 'http://gallery.youxiake.com/Public/Data/upload/productimg/201806/28/5b34a438b8756.jpg?imageslim', '1574765011453468673');
INSERT INTO `jr_recreational` VALUES ('1576816471139467266', 'http://gallery.youxiake.com/Public/Data/upload/productimg/201806/28/5b34a49eb0095.jpg?imageslim', '1574765011453468673');
INSERT INTO `jr_recreational` VALUES ('1576816471462428673', 'http://gallery.youxiake.com/Public/Data/upload/productimg/201806/28/5b34a73c63ec9.jpg?imageslim', '1574765011453468673');
INSERT INTO `jr_recreational` VALUES ('1576816471730864130', 'http://gallery.youxiake.com/Public/Data/upload/productimg/201806/28/5b34ab3dc610c.jpg?imageslim', '1574765011453468673');
INSERT INTO `jr_recreational` VALUES ('1576816472053825538', 'http://gallery.youxiake.com/Public/Data/upload/productimg/202007/28/5f1f87d2da855.jpg?imageslim', '1574765011776430081');
INSERT INTO `jr_recreational` VALUES ('1576816472380981249', 'http://gallery.youxiake.com/Public/Data/upload/productimg/202007/28/5f1fe6098c1af.jpg?imageslim', '1574765011776430081');
INSERT INTO `jr_recreational` VALUES ('1576816472699748354', 'http://gallery.youxiake.com/Public/Data/upload/productimg/202007/28/5f1f887ae477e.jpg?imageslim', '1574765011776430081');
INSERT INTO `jr_recreational` VALUES ('1576816473026904065', 'http://gallery.youxiake.com/Public/Data/upload/productimg/201703/02/58b7eb7f03bfc.jpg?imageslim', '1574765011776430081');
INSERT INTO `jr_recreational` VALUES ('1576816473354059778', 'http://gallery.youxiake.com/Public/Data/upload/productimg/202007/28/5f1fe6623ec31.jpg?imageslim', '1574765011776430081');
INSERT INTO `jr_recreational` VALUES ('1576816473622495234', 'https://youimg1.c-ctrip.com/target/100o1f000001g4bk303FB_C_480_375.jpg', '1574765012103585794');
INSERT INTO `jr_recreational` VALUES ('1576816473949650946', 'https://youimg1.c-ctrip.com/target/0102x1200009ri1o786F9_C_480_375.jpg', '1574765012103585794');
INSERT INTO `jr_recreational` VALUES ('1576816474264223746', 'https://youimg1.c-ctrip.com/target/100f1h000001hl0ok964C_C_480_375.jpg', '1574765012103585794');
INSERT INTO `jr_recreational` VALUES ('1576816474582990850', 'https://youimg1.c-ctrip.com/target/100b1f000001gfnozC5E7_C_480_375.jpg', '1574765012103585794');
INSERT INTO `jr_recreational` VALUES ('1576816474914340865', 'https://youimg1.c-ctrip.com/target/10031d000001ellm307D5_C_480_375.jpg', '1574765012103585794');
INSERT INTO `jr_recreational` VALUES ('1576816475249885186', 'https://img1.qunarzz.com/travel/d6/1706/f7/866f3a1e54b014b5.jpg_r_640x479x70_ccd28332.jpg', '1574765012430741505');
INSERT INTO `jr_recreational` VALUES ('1576816475509932033', 'https://img1.qunarzz.com/travel/poi/1806/b4/64cf36bdf1265d37.jpg_180x180x70_39b54504.jpg', '1574765012430741505');
INSERT INTO `jr_recreational` VALUES ('1576816475832893442', 'https://img1.qunarzz.com/travel/d5/1807/e9/a46e1015760988b5.jpg_180x180x70_9d71280b.jpg', '1574765012430741505');
INSERT INTO `jr_recreational` VALUES ('1576816476164243457', 'https://img1.qunarzz.com/travel/d7/1512/13/a2aceba416ae52f7.jpg_180x180x70_0861884c.jpg', '1574765012430741505');
INSERT INTO `jr_recreational` VALUES ('1576816476495593474', 'https://img1.qunarzz.com/travel/d2/1605/e4/704d9a3959b627f7.jpg_180x180x70_dcd507dc.jpg', '1574765012430741505');
INSERT INTO `jr_recreational` VALUES ('1576816476764028930', 'https://gallery.youxiake.com/product/line/202205/18/e7636a21bb915c32.jpg?imageslim', '1574765012762091522');
INSERT INTO `jr_recreational` VALUES ('1576816477086990337', 'https://gallery.youxiake.com/product/line/202106/02/1e47632a5887d432.jpg?imageslim', '1574765012762091522');
INSERT INTO `jr_recreational` VALUES ('1576816477409951746', 'http://gallery.youxiake.com/Public/Data/upload/productimg/201812/21/5c1ca086c8328.jpg?imageslim', '1574765012762091522');
INSERT INTO `jr_recreational` VALUES ('1576816477745496065', 'http://gallery.youxiake.com/Public/Data/upload/productimg/201812/21/5c1ca087285a7.jpg?imageslim', '1574765012762091522');
INSERT INTO `jr_recreational` VALUES ('1576816478064263169', 'https://img1.qunarzz.com/travel/d4/1611/29/9857583800a519b5.jpg_r_640x388x70_c97b37ea.jpg', '1574765012762091522');
INSERT INTO `jr_recreational` VALUES ('1576816478387224577', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/e8a9fc8d53e56bd441dc94edf08e152c.jpg_r_640x426x70_75d1b739.jpg', '1574765013089247233');
INSERT INTO `jr_recreational` VALUES ('1576816478714380289', 'https://img1.qunarzz.com/travel/d4/1611/29/9857583800a519b5.jpg_r_640x388x70_c97b37ea.jpg', '1574765013089247233');
INSERT INTO `jr_recreational` VALUES ('1576816479041536002', 'https://img1.qunarzz.com/travel/d8/1706/e2/33c7b5edc673d9b5.jpg_r_640x426x70_d6c14edb.jpg', '1574765013089247233');
INSERT INTO `jr_recreational` VALUES ('1576816479309971457', 'https://youimg1.c-ctrip.com/target/fd/tg/g5/M06/D3/95/CggYr1b5-cWAG-cYAD9huP_1GWc004_D_10000_1200.jpg?proc=autoorient', '1574765013089247233');
INSERT INTO `jr_recreational` VALUES ('1576816479641321474', 'https://x0.ifengimg.com/cmpp/fck/2019_43/abc5ff29b234b2f_w3888_h2592.jpg', '1574765013089247233');
INSERT INTO `jr_recreational` VALUES ('1576816479964282881', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-mapi/img/e8e5a94eeb99f30e30ad9afe587727a8.jpg_640x350x70_de672f15.jpg', '1574765013408014338');
INSERT INTO `jr_recreational` VALUES ('1576816480295632898', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-mapi/img/e8648cd6f4a0c1361c6734093f67207a.jpg_r_640x426x70_6b18d8e5.jpg', '1574765013408014338');
INSERT INTO `jr_recreational` VALUES ('1576816480626982914', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-mapi/img/a8955d3232221626cb9d209ddac7f322.jpg_r_640x426x70_69477ff9.jpg', '1574765013408014338');
INSERT INTO `jr_recreational` VALUES ('1576816480941555714', 'https://gallery.youxiake.com/product/line/202209/28/1664333763182.jpg?imageslim', '1574765013408014338');
INSERT INTO `jr_recreational` VALUES ('1576816481205796865', 'https://picx.zhimg.com/v2-313398f62f766c317bb3cf15ad2abf25_1440w.jpg?source=172ae18b', '1574765013408014338');
INSERT INTO `jr_recreational` VALUES ('1576816481537146882', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/6c5503951ffe746182a8abfbd98ffbd1.jpg_640x350x70_278b86af.jpg', '1574765013743558657');
INSERT INTO `jr_recreational` VALUES ('1576816482132738050', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/717edbc694ac368bb91361214e976ad0.jpg_r_640x426x70_fb3a3fc1.jpg', '1574765013743558657');
INSERT INTO `jr_recreational` VALUES ('1576816482849964033', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/688efaa9212aeaaf0cfbf4311ea65930.jpg_r_640x480x70_20d3bc9c.jpg', '1574765013743558657');
INSERT INTO `jr_recreational` VALUES ('1576816483567190018', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/94bd800186a71578f2f92f64bcb05405.jpg_r_640x480x70_2acd916c.jpg', '1574765013743558657');
INSERT INTO `jr_recreational` VALUES ('1576816483898540034', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/6c5503951ffe746182a8abfbd98ffbd1.jpg_r_640x480x70_9ad30f45.jpg', '1574765013743558657');
INSERT INTO `jr_recreational` VALUES ('1576924039698493441', 'https://www.yaspace.cn/public/uploads/image/20210710/60e978be2b023.png', '1574765014066520065');
INSERT INTO `jr_recreational` VALUES ('1576924042080858114', 'https://img1.qunarzz.com/travel/d7/1607/46/9a44f219252c489a.jpg_r_1360x1360x95_6e7d9d64.jpg', '1574765014066520065');
INSERT INTO `jr_recreational` VALUES ('1576924047848026114', 'http://gallery.youxiake.com/Public/Data/upload/productimg/202004/24/5ea23d3c95e6b.jpg?imageslim', '1574765014708248577');
INSERT INTO `jr_recreational` VALUES ('1576924048275845121', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/a285944480549b08077b2b8cd33cecc2.jpg_r_640x480x70_45497761.jpg', '1574765014708248577');
INSERT INTO `jr_recreational` VALUES ('1576924051295744002', 'http://gallery.youxiake.com/Public/Data/upload/productimg/202004/01/5e8407bbc7ce0.jpg?imageslim', '1574765014708248577');
INSERT INTO `jr_recreational` VALUES ('1576924051731951617', 'https://tse3-mm.cn.bing.net/th/id/OIP-C.g0ank-KyzmjCPuambwhTjgHaE9?w=286&h=191&c=7&r=0&o=5&dpr=1.6&pid=1.7', '1574765014708248577');
INSERT INTO `jr_recreational` VALUES ('1576924052059107330', 'https://tse2-mm.cn.bing.net/th/id/OIP-C.FsMtrgaPTIg-yB9E4Ma1gAHaE8?w=254&h=180&c=7&r=0&o=5&dpr=1.6&pid=1.7', '1574765014708248577');
INSERT INTO `jr_recreational` VALUES ('1576924054483415041', 'https://gallery.youxiake.com/product/line/202202/28/6c4865f82979b88d.jpg?imageslim', '1574765015035404289');
INSERT INTO `jr_recreational` VALUES ('1576924054919622658', 'http://gallery.youxiake.com/Public/Data/upload/productimg/202101/28/601225e3d59c0.jpg?imageslim', '1574765015035404289');
INSERT INTO `jr_recreational` VALUES ('1576924058031796225', 'http://gallery.youxiake.com/Public/Data/upload/productimg/202101/28/601227b0418b2.jpg?imageslim', '1574765015035404289');
INSERT INTO `jr_recreational` VALUES ('1576924058501558273', 'http://gallery.youxiake.com/Public/Data/upload/productimg/202101/28/601229629c246.jpg?imageslim', '1574765015035404289');
INSERT INTO `jr_recreational` VALUES ('1576924058824519682', 'http://gallery.youxiake.com/Public/Data/upload/productimg/202101/28/60122bb8f0474.jpg?imageslim', '1574765015035404289');

-- ----------------------------
-- Table structure for jr_scenery
-- ----------------------------
DROP TABLE IF EXISTS `jr_scenery`;
CREATE TABLE `jr_scenery`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL COMMENT '旅游景点id',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL COMMENT '具体的旅游景点（琼海）',
  `introduce` text CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL COMMENT '景点细节介绍',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '琼海景点的img',
  `price` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '景点价格',
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '景点地址（省 市）',
  `view_count` int(0) NULL DEFAULT NULL COMMENT '观看人数',
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '分类',
  `score` double(10, 2) NULL DEFAULT NULL COMMENT '得分',
  `travel_day` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '旅游天数',
  `go_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '开始地方',
  `together_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '集合地方',
  `breakup_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '分开的地方',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_german2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jr_scenery
-- ----------------------------
INSERT INTO `jr_scenery` VALUES ('1574765011000483841', '稻城亚丁', '稻城亚丁风景区位于四川省甘孜藏族自治州稻城县香格里拉镇亚丁村境内，主要由“仙乃日、央迈勇、夏诺多吉”三座神山和周围的河流、湖泊和高山草甸组成。它的景致保持着在地球上近绝迹的纯粹，因其独特的地貌和原生态的自然风光，被誉为“香格里拉之魂”和“最后的香格里拉”，被国际友人誉为“水蓝色星球上的最后一片净土”，是摄影爱好者的天堂。进入稻城我们就会看到一片片的红色草甸，继续往里行进就会看到亚丁，那是人们心中的一片净土。蓝色的天空映衬着雪山、湖泊、村庄，一切都变得如此的宁静。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/scenery/%E7%A8%BB%E5%9F%8E%E4%BA%9A%E4%B8%811.jpg', '270', '四川甘孜', 2201, '环线·徒步', 4.80, '2-3天', '四川成都', '四川甘孜', '四川甘孜');
INSERT INTO `jr_scenery` VALUES ('1574765011453468673', '色达佛学院', '在重重的群山环绕之中，沟里的僧舍，以佛学院的大经堂为中心，密密麻麻搭满了四面的山坡，不计其数的绛红色小木屋，延绵无数，一眼望不到头。这些红色小木屋，是一万多名僧侣的住所。人们通常将这里统称为色达佛学院。色达佛学院，全称是色达县喇荣寺五明佛学院，误称是五明佛学院（很多大寺庙都有五明佛学院），坐落在四川省甘孜藏族自治州色达县境县城东南方约20公里处。是由晋美彭措法王1980年创办，在短短数年间从藏区一个山谷深处迅速崛起，一跃成为世界上最大的藏传佛学院。三万多出家僧众在此修行居住。佛学院戒律十分严格，男众女众的僧舍泾渭分明。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/scenery/%E8%89%B2%E8%BE%BE%E4%BD%9B%E5%AD%A6%E9%99%A2.jpg', '20', '四川甘孜', 2159, '古城·佛教', 4.70, '2-3天', '四川成都', '四川甘孜', '四川甘孜');
INSERT INTO `jr_scenery` VALUES ('1574765011776430081', '九寨沟', '九寨沟位于四川省阿坝藏族羌族自治州九寨沟县漳扎镇，因九个藏族村寨而得名，被联合国教科文组织列为世界自然遗产。以“九寨六绝”——翠湖、叠瀑、彩林、雪峰、藏情、蓝冰而闻名。 九寨沟的景色之美，被誉为“美得不似人间” ：这里动植物资源丰富，种类繁多，原始森林遍布，栖息着大熊猫等十多种稀有和珍贵野生动物；远望雪峰林立，白雪皑皑。近看碧水澄澈，清可见底；再加上藏家木楼、晾架经幡、栈桥、磨房、藏族习俗及神话传说构成的人文景观，使得九寨沟四季皆美，仿若童话。这里也是国内影视剧的热门取景地。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/scenery/%E4%B9%9D%E5%AF%A8%E6%B2%9F.jpg', '90', '四川阿坝', 3267, '游山·玩水', 5.00, '3天左右', '四川成都', '四川阿坝', '四川阿坝');
INSERT INTO `jr_scenery` VALUES ('1574765012103585794', '春熙路', '春熙路是成都最古老的商业街道之一，在“中国商业街排行榜”上名列第三。如今的春熙路依旧热闹非凡，串起了正科甲巷、署袜中街等形成了城中最旺的商圈，这里汇聚众多品牌专卖店，大部分店面常年都有折扣。除去购物，春熙路也是成都人悠闲娱乐的集中地，这里聚集了成都的几大老字号美食店，可以一口气吃遍成都小吃。被业内誉为中国特色商业街。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/scenery/%E6%98%A5%E7%86%99%E8%B7%AF.jpg', '0', '四川成都', 4179, '古街·风情', 4.70, '2-3天', '四川成都', '四川成都', '四川成都');
INSERT INTO `jr_scenery` VALUES ('1574765012430741505', '宽窄巷子', ' 宽窄巷子位于四川省成都市青羊区长顺街附近，由宽巷子、窄巷子、井巷子平行排列组成，全为青黛砖瓦的仿古四合院落，这里也是成都遗留下来的较成规模的清朝古街道，与大慈寺、文殊院一起并称为成都三大历史文化名城保护街区。 宽巷子集中了整个街区最多最完整的老建筑，多数的旧时门脸都保存完好。在这里可以品碗茶，吃正宗川菜，体验老成都的风土人情。窄巷子既有清末民初的建筑，也有早期西式洋楼，是最能体现宽窄美学的地方；是以西式饮食文化、艺术休闲、健康生活为主的成都休闲生活品味区。井巷子是典型的民俗成都缩影，除了引人入胜的砖文化墙，也汇聚了成都特色小吃、民俗玩意儿，展现了地道老成都生活的独有味道。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/scenery/%E5%AE%BD%E7%AA%84%E5%B7%B7.jpg', '0', '四川成都', 3778, '深巷·怡景', 4.50, '2天左右', '四川成都', '四川成都', '四川成都');
INSERT INTO `jr_scenery` VALUES ('1574765012762091522', '都江堰', '都江堰位于四川省成都市都江堰市城西，坐落在成都平原西部的岷江上，由分水鱼嘴、飞沙堰、宝瓶口等部分组成，两千多年来一直发挥着防洪灌溉的作用，使成都平原成为水旱从人、沃野千里的\"天府之国\"，至今灌区已达30余县市、面积近千万亩，是全世界迄今为止，年代最久、唯一留存、仍在一直使用、以无坝引水为特征的宏大水利工程，凝聚着中国古代劳动人民勤劳、勇敢、智慧的结晶。都江堰被誉为“世界水利文化的鼻祖”，有“青城天下幽”之称的青城山，是中国道教发祥地； 青城山·都江堰已成功列入《世界遗产名录》，都江堰市被评为“中国历史文化名城”、“中国优秀旅游城市”； 建于战国时期的都江堰，历经两千多年风雨仍发挥着越来越巨大的作用，千年无坝古堰堪称人与自然和谐统一。 穿越两千年的时光，依然屹立眼前，依然造福人类，这是到都江堰市不得不前往瞻仰的一景。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/scenery/%E9%83%BD%E6%B1%9F%E5%A0%B0.jpg', '90', '四川成都', 3646, '大江·美景', 4.50, '1天', '四川成都', '四川成都', '四川成都');
INSERT INTO `jr_scenery` VALUES ('1574765013089247233', '青城山', '青城山位于成都市都江堰市西南方向，是中国道教的发祥地之一。东汉时期，道教创始人张道陵看中了青城山静谧幽深、灵净高远的气质，其便成为了道教的“第五洞天”、“天下第五名山”。直到今天，山上的天师洞、建福宫、上清宫、祖师殿、老君阁、朝阳洞等数十座道教宫观仍完好保存，与山间遮天蔽日的古树一起，见证着两千年来的岁月风霜。一个“幽”字，精准地概括出了青城山的神韵。杜甫曾有诗云：“丈人祠西佳气浓，绿云拟住最高峰”，极赞青城山幽静的绿意。这里的川西植被原始而丰富，富含氧离子的空气清新而湿润，绝对有充分的理由让你逃离都市喧嚣，躲避近40℃的酷暑，期盼能隐居在此，清凉一夏。', 'http://gallery.youxiake.com/Public/Data/upload/productimg/201807/23/5b55a08a382eb.jpg?imageslim', '110', '四川成都', 2859, '避暑·山庄', 4.60, '1天', '四川成都', '四川成都', '四川成都');
INSERT INTO `jr_scenery` VALUES ('1574765013408014338', '三星堆遗址概述', '三星堆古遗址位于四川省广汉市西北的鸭子河南岸，分布面积12平方千米，距今已有5000至3000年历史，是迄今在西南地区发现的范围最大、延续时间最长、文化内涵最丰富的古城、古国、古蜀文化遗址。现有保存最完整的东、西、南城墙和月亮湾内城墙。三星堆遗址被称为20世纪人类最伟大的考古发现之一，昭示了长江流域与黄河流域一样，同属中华文明的母体，被誉为“长江文明之源”。其中出土的文物是宝贵的人类文化遗产，在中国的文物群体中，属最具历史、科学、文化、艺术价值和最富观赏性的文物群体之一。在这批古蜀秘宝中，有高2.62米的青铜大立人、有宽1.38米的青铜面具、更有高达3.95米的青铜神树等，均堪称独一无二的旷世神品。而以金杖为代表的金器，以满饰图案的边璋为代表的玉石器，亦多属前所未见的稀世之珍。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/scenery/%E4%B8%89%E6%98%9F%E5%A0%86.jpeg', '82', '四川广汉', 2816, '名胜·古迹', 4.70, '2天', '四川成都', '四川广汉', '四川广汉');
INSERT INTO `jr_scenery` VALUES ('1574765013743558657', '大熊猫基地', '成都大熊猫繁育研究基地，是中国政府实施大熊猫等濒危野生动物迁地保护工程的主要研究基地之一，国家AAAA级旅游景区。是我国乃至全球知名的集大熊猫科研繁育、保护教育、教育旅游、熊猫文化建设为一体的大熊猫等珍稀濒危野生动物保护研究机构。成都基地在开展大熊猫繁育研究的同时，建立了大熊猫博物馆、熊猫活体与环境展示场、园区说明牌、互动讲解站、熊猫魅力剧场以及互联网站等宣传教育设施。这里是了解大熊猫的最好去处。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/scenery/%E5%A4%A7%E7%86%8A%E7%8C%AB%E5%9F%BA%E5%9C%B0.jpg', '58', '四川成都', 3559, '熊猫·动物', 4.50, '1天', '四川成都', '四川成都', '四川成都');
INSERT INTO `jr_scenery` VALUES ('1574765014066520065', '锦江剧场', '锦江剧场是成都的川剧艺术中心，位于成都市锦江区华兴正街54号，前身为悦来茶园，始建于1908年（光绪三十四年），至今已有近100年的历史。是国内第一家以川剧命名、以戏剧演出为主的大型综合性文化娱乐场所，包括锦江剧场、悦来茶园、川剧艺术博物馆等。锦江剧场建于川剧的发祥地“悦来茶园”旧址，是一座集川西古典风格外貌和现代内装，以川剧命名的艺术中心，也是社会公认的“川剧窝子”。从建成起，就由当时著名的川剧班社“三庆会”在此驻场演出。杨素兰、康子林、肖楷成、唐广体等著名艺人在此演出了大批精彩剧目。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/scenery/%E9%94%A6%E6%B1%9F%E5%89%A7%E5%9C%BA.jpg', '280', '四川成都', 4786, '川剧·风范', 4.70, '1天', '四川成都', '四川成都', '四川成都');
INSERT INTO `jr_scenery` VALUES ('1574765014708248577', '乐山大佛', '乐山大佛景区位于乐山市郊，岷江、青衣江、大渡河三江汇流处，与乐山城隔江相望。景区集聚了乐山山水人文景观的精华，属峨眉山国家级风景区范围，是闻名遐迩的风景旅游胜地。 \n\n凌云山紧傍岷江，上有凌云寺，建于唐代。依山开凿大佛一座，通高71米，脚背宽8.5米，为当今世界第一大佛。大佛为唐代开元名僧海通和尚创建，历时90载完成。大佛为一尊弥勒座像，雍容大度，气魄雄伟，被诗人誉为“山是一尊佛，佛是一座山”。 \n\n近年，发现了以乌尤山、凌云山、龟城山构成的乐山巨形睡佛景观，隔江望去，酷似一巨大佛像仰卧于三江之上，卧佛直线长1300多米。巨型卧佛的发现，为大佛景区更添魅力。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/scenery/%E4%B9%90%E5%B1%B1%E5%A4%A7%E4%BD%9B.jpg', '90', '四川乐山', 3685, '古迹·美景', 4.40, '1-3小时', '四川成都', '四川乐山', '四川乐山');
INSERT INTO `jr_scenery` VALUES ('1574765015035404289', '海螺沟', '海螺沟位于四川省甘孜藏族自治州东南部泸定县境内，贡嘎山东坡，是青藏高原东缘的极高山地。海螺沟位于贡嘎雪峰脚下，以低海拔现代冰川著称于世。海螺沟是中国目前最容易观赏、接近，景区配套设施最为完善的景区。其中海螺沟一号冰川是贡嘎山地区五大冰川中规模最大、海拔最低的一条。冰川上的大冰瀑布高1080 米，宽1100 米，是我国至今发现的最高最大冰瀑布。除了冰川美景之外，温泉也是海螺沟的一大特色，温泉位于海螺沟二号营地，每年冬天是温泉最火热的时候。另外位于海螺沟旁边的燕子沟和雅家梗分布着大量红石滩，他们是在雪山之下的红色瀑布，强烈的颜色反差十分美丽。', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/scenery/%E6%B5%B7%E8%9E%BA%E6%B2%9F.jpg', '90', '四川甘孜', 2540, '雪山·美景', 4.80, '1天', '四川成都', '四川甘孜', '四川甘孜');
INSERT INTO `jr_scenery` VALUES ('1574765015358365698', '【向野房车 • 莲宝叶则】', '孟屯河谷+莲宝叶则两大景致露营/草原美学下午茶/彩林毕棚沟/古尔沟华美达温泉酒店/九曲黄河第一湾日落          <游侠客旗下房车品牌>2-5人/一车即走/户外游牧美学/两晚精致野趣露营/小众网红秘境打卡，周边游提前1天，国内游提前3天，出境游提前3-5天，APP和短信群发出团通知', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/scenery/%E6%88%BF%E8%BD%A6%E6%97%85%E8%A1%8Cjpg.jpg', '5080', '四川', 263, '房车·旅行', 4.60, '6天', '四川成都', '四川', '四川');
INSERT INTO `jr_scenery` VALUES ('1574765015677132802', '自驾牛背山', '2022全新策划，每天发团，10人成行；穿越云海，遇见云海之上的贡嘎群山。2天1晚牛背山自驾越野之旅！', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/scenery/%E8%87%AA%E9%A9%BE%E6%B8%B8.jpg', '980', '四川', 150, '星空·云海', 4.50, '2天', '四川成都', '四川', '四川');
INSERT INTO `jr_scenery` VALUES ('1574765016004288514', '达瓦更扎·云海日出', '9.27恢复发团，打卡网红地达瓦更扎/神木垒森林/跷碛湖，登亚洲第二大360°观景平台，观壮美云海日出，赠神木垒门票+藏家小火锅', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/scenery/%E4%BA%91%E6%B5%B7%E6%97%A5%E5%87%BA.jpg', '268', '四川', 1447, '云海·日出', 4.60, '2天', '四川成都', '四川', '四川');
INSERT INTO `jr_scenery` VALUES ('1574765016323055617', '雪山之巅• 贡嘎摄影', '2022年，邂逅冷嘎措-子梅垭口，等待蜀山之巅星空倒影；守候鱼子西，打卡新晋网红机位 4日山野摄影之旅！', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/scenery/%E9%9B%AA%E5%B1%B1%E4%B9%8B%E5%B7%85.png', '2180', '四川', 520, '云海·星空', 4.70, '4天', '四川成都', '四川', '四川');
INSERT INTO `jr_scenery` VALUES ('1574765016650211330', '轻装 • 贡嘎穿越4日', '想徒步的朋友可以报名[蜀山秘境徒步]\n<蜀山之巅>冷嘎措边守望 云层之中漫步 轻装体验贡嘎', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/scenery/%E8%BD%BB%E8%A3%85%20%E2%80%A2%20%E8%B4%A1%E5%98%8E%E7%A9%BF%E8%B6%8A4%E6%97%A5.jpg', '2080', '四川', 99, '贡嘎·雪山', 4.50, '4天', '四川成都', '四川', '四川');
INSERT INTO `jr_scenery` VALUES ('1574765016968978434', '宝藏小城·寻味乐山', '10.1/10.2国庆！探索宝藏小城-乐山，打榜苏稽美食，寻迹嘉州记忆，三江汇流观大佛，一起去偷得浮生半日闲！', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/scenery/%E5%AE%9D%E8%97%8F%E5%B0%8F%E5%9F%8E.jpg', '108', '四川', 218, '风景·美食', 4.50, '2天', '四川成都', '四川', '四川');

-- ----------------------------
-- Table structure for jr_space
-- ----------------------------
DROP TABLE IF EXISTS `jr_space`;
CREATE TABLE `jr_space`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '空间特有id',
  `user_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发布者信息',
  `reviews` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '发布空间的评论',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发布空间时需要的图片',
  `gmt_time` datetime(0) NULL DEFAULT NULL COMMENT '上传时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jr_space
-- ----------------------------
INSERT INTO `jr_space` VALUES ('1', '1572160547390410754', '放假了 国庆节我们会去爬青城山，有伴一起吗？在线征集。', 'https://p1-q.mafengwo.net/s17/M00/1E/87/CoUBXl-yGwiAOZwuAAtnSznS9Cc117.JPG', '2022-09-24 15:48:08');
INSERT INTO `jr_space` VALUES ('1582744859970682882', '1577180318901104641', '卡哇伊！！！！', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/space/2022/10/19/e26b6000c4d24b6e8598655e872b3b42.jpg', '2022-10-19 22:45:55');
INSERT INTO `jr_space` VALUES ('1582986487075487746', '1577180318901104641', '实验室', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/space/2022/10/20/cf79a42b4b214d27b728c0a68558ec55.jpg', '2022-10-20 14:46:04');
INSERT INTO `jr_space` VALUES ('1582986666264543234', '1577180318901104641', 'dq喜欢敲代码', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/space/2022/10/20/37b84946976e4ae48d2fe05ebab3f84c.jpg', '2022-10-20 14:46:46');
INSERT INTO `jr_space` VALUES ('1583027220620820482', '1583025955371270145', '终于和菈妮老婆结婚了！', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/space/2022/10/20/6f0b7e4feb9546e9b39bfdbd6fc16c1a.png', '2022-10-20 17:27:55');
INSERT INTO `jr_space` VALUES ('1583297787609530369', '1577180318901104641', '晚上风景好美啊！！', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/space/2022/10/21/740eb9e9db46455bb9dc46bb651d31d4.jpg', '2022-10-21 11:23:03');
INSERT INTO `jr_space` VALUES ('1583299085968916482', '1577180318901104641', '今天天气真好！！！', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/space/2022/10/21/420b5669a83b46249ae908b85b4bd40e.jpg', '2022-10-21 11:28:13');
INSERT INTO `jr_space` VALUES ('1583336285834100738', '1577180318901104641', '今天是一个晴天', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/space/2022/10/21/c2ee38ceffda4775b8946a6184f689c0.jpg', '2022-10-21 13:56:02');
INSERT INTO `jr_space` VALUES ('1583407137741127682', '1577180318901104641', '哈哈哈哈', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/space/2022/10/21/d1206dd294eb497bbe3ab1142772d077.jpg', '2022-10-21 18:37:35');
INSERT INTO `jr_space` VALUES ('1583412917500428289', '1583411457417068546', '第一次用app，好激动', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/space/2022/10/21/a709513e03914bc584e6382fd5df36b2.gif', '2022-10-21 19:00:33');
INSERT INTO `jr_space` VALUES ('2', '1572040081290674178', '西昌这个地方天气怎么样？我觉得特别适合长期居住。', 'https://p1-q.mafengwo.net/s17/M00/1E/8C/CoUBXl-yGwuAGjmTABFI8kl9XIs216.JPG', '2022-09-24 15:51:17');
INSERT INTO `jr_space` VALUES ('3', '1574368035418316802', '西昌+泸沽湖4日3晚跟团游', 'https://p1-q.mafengwo.net/s17/M00/20/0D/CoUBXl-yHFeACCU3AAnbuL3Ds7s072.jpg', '2022-09-26 20:08:19');
INSERT INTO `jr_space` VALUES ('4', '1574368205459595266', '有人国庆和我一起琼海玩吗？', 'https://p1-q.mafengwo.net/s17/M00/1E/14/CoUBXl-yGqeAQq96AAQ92ZV_0T0052.JPG', '2022-09-26 20:09:45');
INSERT INTO `jr_space` VALUES ('5', '1574368301148446722', '我想去旅游,但我爸妈不让我出去。哎无奈~~~', 'https://p1-q.mafengwo.net/s17/M00/1E/22/CoUBXl-yGq-AfB-NABuYo7gMx50051.jpg', '2022-09-26 20:10:49');
INSERT INTO `jr_space` VALUES ('6', '1574368356110606338', '现在生活多好呀！每周带着自己孩子去游泳！嘿嘿。', 'https://p1-q.mafengwo.net/s17/M00/1E/83/CoUBXl-yGwOAQl60AAp5vDKxOys920.JPG', '2022-09-26 20:12:41');

-- ----------------------------
-- Table structure for jr_store
-- ----------------------------
DROP TABLE IF EXISTS `jr_store`;
CREATE TABLE `jr_store`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `uid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pro_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT NULL COMMENT '(1 美食 2 酒店 3风景)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jr_store
-- ----------------------------
INSERT INTO `jr_store` VALUES ('1582349949542567937', '1582349602883342337', '1573240795520356353', 1);
INSERT INTO `jr_store` VALUES ('1582349972833538049', '1582349602883342337', '1573240795834929154', 1);
INSERT INTO `jr_store` VALUES ('1582350066739810305', '1582349602883342337', '1574647701652672513', 2);
INSERT INTO `jr_store` VALUES ('1582734200197398529', '1577180318901104641', '1574765013408014338', 3);
INSERT INTO `jr_store` VALUES ('1582734252101910529', '1577180318901104641', '1574647697462562817', 2);
INSERT INTO `jr_store` VALUES ('1582744477462740994', '1577180318901104641', '1573240795834929154', 1);
INSERT INTO `jr_store` VALUES ('1582884581242093570', '1582882656178192386', '1574765016004288514', 3);
INSERT INTO `jr_store` VALUES ('1582985034877419522', '1582984184398397442', '1574765016004288514', 3);
INSERT INTO `jr_store` VALUES ('1582986263829463041', '1577180318901104641', '1574765015358365698', 3);
INSERT INTO `jr_store` VALUES ('1582986669133447170', '1577180318901104641', '1574647716152381442', 2);
INSERT INTO `jr_store` VALUES ('1583025980134440961', '1583025955371270145', '1574765011000483841', 3);
INSERT INTO `jr_store` VALUES ('1583332720751198210', '1577180318901104641', '1573240794811518978', 1);
INSERT INTO `jr_store` VALUES ('1583406611838320641', '1577180318901104641', '1573240796229193729', 1);
INSERT INTO `jr_store` VALUES ('1583406696198356994', '1577180318901104641', '1573240795134480386', 1);
INSERT INTO `jr_store` VALUES ('1584718569736306690', '1584718385098850305', '1574765015035404289', 3);
INSERT INTO `jr_store` VALUES ('1584718673700519938', '1584716999233699841', '1574765012430741505', 3);
INSERT INTO `jr_store` VALUES ('1585174194962366465', '1584716999233699841', '1574765013089247233', 3);
INSERT INTO `jr_store` VALUES ('1587113950436720642', '1584718385098850305', '1574765016968978434', 3);
INSERT INTO `jr_store` VALUES ('1587709382007259137', '1584718385098850305', '1574765011453468673', 3);
INSERT INTO `jr_store` VALUES ('1588102348391645186', '1584716999233699841', '1574765011453468673', 3);

-- ----------------------------
-- Table structure for jr_strategy
-- ----------------------------
DROP TABLE IF EXISTS `jr_strategy`;
CREATE TABLE `jr_strategy`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '旅游攻略id',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '攻略标题',
  `nickname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称',
  `useravatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户头像',
  `gotime` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '出发时间',
  `tag` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签',
  `friend` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '同行',
  `feeling` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '感受',
  `article` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '正文',
  `total_day` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '总共多少天',
  `total_picture` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '总共多少张图片',
  `img1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图像1',
  `img2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图形2',
  `img3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图像3',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jr_strategy
-- ----------------------------
INSERT INTO `jr_strategy` VALUES ('1575786799201300482', '四天三晚闺蜜游南丹，有趣目的地等你来探索', '文武贝余', 'https://img1.qunarzz.com/ucenter/portrait/1812/73/a7319a142e011272.jpg_r_48x48_a4a2f3d1.jpg', '2022/09/26出发', '文艺范', '三五好友', '南丹，说真的，我是第一次听说这个地名', '南丹，说真的，我是第一次听说这个地名，去网络搜索一番，才知其奇妙，原来这里藏着许多鲜为人知的目的地，有“中国白裤瑶之乡”，也有世界最大天然藏酒洞，也有中国最美泥巴房酒店，中国炼丹第一山，现在就跟着我的游记看一看这个地方到底有多奇妙吧！', '5', '5', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/4e310efa7c9390639aeb916f79615f4d.jpg_640x480x70_a2fdb67f.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/d6499b716bec3b1b42528ad7585266a1.jpg_640x480x70_6967fb29.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/af273018e0179d65aed157fdde3615ce.jpg_640x480x70_5f0b1390.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786801734660098', '【国庆去哪儿玩】邂逅美丽多彩青城，开启3天2夜深度之旅', '行走的迈克尔', 'https://qcommons.qunar.com/headshot/headshotsById/9140918.png?s&ssl=true', '2022/09/15出发', '短途派', '三五好友', '说说这次旅行', '在北方有这样一座城市，少有人对它过深入性的了解。它拥有着悠久的历史跟光辉灿烂的文化，也是国家历史文化名城，更是华夏文明的发祥地之一。只有当你踏上这座城市之后，你才会发现这座城市竟然藏着这么多宝藏的地方，这就是“青城”-呼和浩特。呼和浩特是蒙古语音译，意为“青色的城”，它是昭君出塞之地，也是游牧文明和农耕文明交汇、碰撞、融合之城。', '11', '11', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/9c218647f1f375b9913dcf6e4b62539f.jpg_r_640x427x70_0f8c5990.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/848f886b21c8407b4c6dd58c29812347.jpg_r_640x427x70_5aa81b85.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/6e1a37cb971effdb95b57ec28c50137c.jpg_r_640x426x70_d37a4910.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786802128924673', '初见平遥古城与乔家大院——千年前的穿梭与信仰', 'mimik', 'https://qcommons.qunar.com/headshot/headshotsById/641917985.png?s&ssl=true', '2022/09/08出发', '干货', '三五好友', '【写在最开始】', '当你以为山西很多煤，其实它还真是煤矿大亨， 山西三分之一的地下都埋着煤；当你以为山西爱吃醋，啥都要上醋，其实它还真的是好好吃，醇度够，香味儿足；当你以为山西只吃面，其实它还真的是吃不腻，种类繁多，能天天吃面半个月都不重样；这都是你以为的山西，其实，它还有更加不为人知的一面。一说起旅游，许多人会说起 西藏 、 成都 、 青岛 ……却唯独没有 山西 。一提起 山西 ，你们会说“哦？煤老板么？”哪怕只是玩笑话，却让每个山西人心里有些别扭。每一个人对故乡的热爱都不应该被亵渎，被玩笑。况且， 山西 真的只有煤么？来到山西后才知道果然应了那句“而世之奇伟、瑰怪，非常之观，常在于险远”。最终决定走趟 山西 —— 平遥 和 大同 ，感受一下千年光阴前的信仰。', '8', '8', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/bd45a3dc3283a6f7a1285fd825011ac7.jpg_r_640x835x70_7044d0f6.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/9442b60bab502b6ad9f0649e834f9315.jpg_r_640x446x70_540e4356.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/7870e4aa9d32645bc31aa0a640d6bfde.jpg_r_640x403x70_c8e1b5ae.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786802518994945', '金秋九月游湖南，吃喝玩乐走遍“长张郴”，干货攻略已为你准备好', '在路上的小羊', 'https://qcommons.qunar.com/headshot/headshotsById/155760450.png?s&ssl=true', '2022/09/16出发', '文艺范', '独自一人', '不管你是否喜欢旅游，对于“湖南”这个名字都一定不会陌生。从娱乐角度来说，它有国内做得最好的地方综艺；从美食角度来说，它有不输川菜的极品香辣；从人文历史来说，湖南早在二千多年前就是中原重地；更加出圈的是：湖南还有三个超级大热门—长沙、张家界、郴州！', '长沙，绝对是近两年来最为火爆的“网红旅游城”，就算是炎热的七八月，也阻挡不了游客的热情，这是一座凌晨三四点钟大街上都满是游人的城市，可以见得它有多么的受人欢迎。而张家界自不用多说，这是一个20年前就火到国外的知名旅游地，在这里建立了《第一个国家级森林公园》，武陵源、天门山也是我国第一批5A级景区，好玩的地方可真不少。可能说到“郴州”大家相对会陌生一点，但说到郴州境内的景点高椅岭、仰天湖、东江湖，想必你就瞬间会有印象了，这些人间绝色好风光，同样不会让大家失望～所以，这一次我们就趁着九月的金秋美景来到湖南，走上一趟“长张郴”的环线之旅，好吃好喝好玩的，这趟行程不会差～', '16', '16', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/f6980f4ed86751310724c3bae0cc719f.jpg_r_640x347x70_9a1ead4e.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/7206c317aba18e81a273947782292ba3.jpg_r_640x773x70_fc2145b3.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/dbb0ecdba31673771a6195b47443b71f.jpg_r_640x739x70_0c43f674.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786802850344962', '偷得浮生三日闲，览胜武夷山水间', '小猪xxhh', 'https://qcommons.qunar.com/headshot/headshotsById/156037578.png?s&ssl=true', '2022/09/02出发', '臻品', '家庭', '[前言]', '位于福建西北部的武夷山是世界自然与文化双重遗产，不仅自然风光迷人，而且人文景点众多，我们对它神往已久。好不容易凑了一个三天的小长假，武夷山在今年终于被我们成功拔草。', '9', '9', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/da8036727e9d0a85afd7fb0710701041.jpg_r_640x427x70_9373ff13.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/508ccb2766929bf65d83535593542f6c.jpg_r_640x427x70_445e3514.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/154f3682c337d4ae849e7d8c71a990f6.jpg_r_640x427x70_59e35263.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786803236220929', '从海的那头到山的这头，红色遵义的东茶、西酒、南寨、北奇', '番茄看世界', 'https://qcommons.qunar.com/headshot/headshotsById/3083368.png?s&ssl=true', '2022/09/01出发', '臻品', '三五好友', '说说这次旅行', '一半的人想参观会址，一半的人想买茅台酒，这是我对 遵义 的第一印象。走在 遵义 街头最大的感受就是，到处都有红军的影子，“ 遵义 会议”、“四渡 赤水 ”真的太出名了。从海的那头到山的这头，从 福建 到 遵义 ，1300公里，航程只需两个多小时，而当年红九军团却跋山涉水整整走了三个半月。的确，红色就是 遵义 的标签，但是 遵义 之旅只有红色吗？其实 遵义 好玩的好吃的地方还真的是很多的。', '15', '15', 'https://qcommons.qunar.com/headshot/headshotsById/3083368.png?s&ssl=true', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/793104af1385d07e26fb2ef34f2cda7a.jpeg_r_640x341x70_b035ca7b.jpeg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/a84c2652b8e0d43f309e0fb84049dedc.jpg_r_640x382x70_a0ca0231.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786803622096898', '夏日逃跑计划丨云南小环线六天五晚自驾游', '躺平生活家', 'https://qcommons.qunar.com/headshot/headshotsById/130477360.png?s&ssl=true', '2022/07/30出发', '干货', '三五好友', '你有多久没有抬头望一眼蓝天，', '昭通古城，是朱提文化的见证者之一。昭通为云南三大文化发源地之一。作为曾经的“小昆明”。古街宽敞质朴，以清代建筑为主体又包含民国、中西合璧建筑形式的多元建筑风格，古街跟现代的商业街一样是和人们的生活需求相关，各种商业形式应有尽有，老街巷子里就是当地人古城人的住所。', '22', '22', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/84bda3ef313d65fdeb8e20ba08d77ae1.jpg_640x350x70_c8faaf5b.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/4aa7c8697326d341487eef4eb68797bb.jpg_r_640x456x70_0de59a0d.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/7476cf390269e242e8f0f101c91a5c21.jpg_r_640x480x70_6592f053.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786803949252609', '夏末旅行深入聊城四日游，感受别样的聊城美', '阿萌Mona-', 'https://qcommons.qunar.com/headshot/headshotsById/184949984.png?s&ssl=true', '2022/08/26出发', '美图', '三五好友', '抓住夏天的小尾巴，不如来场聊城旅行吧！', '聊城已有5000余年的历史，是国家历史文化名城，代表农耕文明的黄河文化与代表商业文明的运河文化在这里交相辉映。中国古典文学名著《水浒传》、《金 瓶 梅》、《聊斋志异》等书中的许多故事都是发生在聊城。前几年就和朋友来过一次聊城，虽是匆匆两日，但对这里念念不忘，最近趁着天气凉快又和朋友安排了一场聊城旅行。这次是一场聊城的深度旅行，去了东昌府区、临清市、东阿县、冠县、阳谷县等地，四天三晚深入的了解了一下聊城的风光与历史、城市与人文，下面给大家介绍一下我们的行程～', '12', '12', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/c35b1736d4acde7a09d741aeb90d355c.jpg_r_640x480x70_aae05866.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/83efb7aaa036ab9e755804aa5950025c.jpg_r_640x480x70_1938e717.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/1794d1fe83ad019641b15496bd247ff9.jpg_r_640x480x70_a41ee3b6.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786804347711489', '一碗绍兴酒，一场水城梦，绍兴四日漫行记', '麻小薯', 'https://qcommons.qunar.com/headshot/headshotsById/10228698.png?s&ssl=true', '2022/09/13出发', '文艺范', '三五好友', '提起绍兴，我的第一反应就是一壶上好的黄酒。绍兴，犹如镶嵌在江南的一颗明珠，是古镇，亦是水城。绍兴作为鲁迅的故乡，城市里总带着那么一股浓浓的文学气息，而绍兴又因陈年的黄酒，又弥漫着浓浓的酒香。时隔三年再回到绍兴，这座水乡古镇似乎依旧保有它的那份古朴纯真。', '我带着一份久违的江南情，再次来到绍兴，在乌篷船上感受这个水城陈年的味道。自然与人文，在这里融合得恰到好处。绍兴就像一壶佳酿的好酒，品得越久，越是沉醉。百草园的皂荚树，孔乙己捻起的茴香豆，三味书屋两百年的腊梅。在这次充实的四天三晚行程中，我几乎跑遍了绍兴每一个区，从古城到柯桥，从诸暨到新昌，每一站都让我感受到绍兴的与众不同。', '14', '14', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/6b100c7644d67ccb6d8f678372e130b7.jpg_640x350x70_7c339326.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/a14776b32eb6d8ee534839e2d078c62e.jpg_r_640x426x70_6ad8167b.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/df555e7d05c5da580f20241c6ec48f80.jpg_r_640x963x70_375de818.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786804670672898', '莽山国家森林公园&五指峰，堪称小张家界', '极客行天下', 'https://qcommons.qunar.com/headshot/headshotsById/158179659.png?s&ssl=true', '2022/09/03出发', '美图', '三五好友', '跟我走吧，天亮就出发', '莽山可看作由两个景区组成：森林公园和五指峰。森林公园的面积较大，包含将军寨、 天台 山和猴王寨三个景点。五指峰既与森林公园相连，又是一个独立的景区。莽山可看作由两个景区组成：森林公园和五指峰。从居住地出发，驱车两个半小时，终于来到森林公园入口，也就是所谓的西门。莽山设有东西二门，初次到访的小伙伴会被它搞晕，本文最后专门做了一个攻略。这里的酒店旅馆还是挺多的，餐馆也不少，在售票处购买了门票后，看看已是中午时分，先找地方解决了午餐，稍作休整，开始进山打卡。', '13', '13', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/ff496d1df397de8882fb657a1099193b.jpg_640x350x70_1cb43f6c.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/71e9d9beead9a68fc7a5ec60a3c1431e.jpg_r_640x480x70_0eea9d68.jpg', 'https://tr-osdcp.qunarzz.com/tr-osd-tr-space/img/e7e2ce8069cdf20b24721582341a754a.jpg_r_640x480x70_87e3398b.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786805064937473', '成都 重庆 《双城记》不容错过的精彩四川之旅', '紫澄', 'https://qcommons.qunar.com/headshot/headshotsById/2732840.png?s&ssl=true', '2017/01/30出发', '干货', '三五好友', '旅行的意义', '为什么旅行？每个人都有各自的理由，而我的理由，其实很简单。我喜欢在路上的感觉，喜欢在陌生的城市、在陌生的街道、在陌生的山水间、在陌生的人群中，不断的去寻找去发现以及对未知的不确定，更甚至于也许只是单纯的喜欢在路上寻找那个未知的自己。', '13', '13', 'https://img1.qunarzz.com/travel/d8/1803/2c/a66d92c6c65464b5.jpg_640x350x70_287c8573.jpg', 'https://img1.qunarzz.com/travel/d1/1803/3a/a70995fc0270beb5.jpg_r_640x895x70_79e1011c.jpg', 'https://img1.qunarzz.com/travel/d5/1803/3e/56f90843688fffb5.jpg_r_640x457x70_546991ba.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786805463396354', '九寨黄龙大环线——爱在旅途，永不褪色', 'hnpg3511', 'https://qcommons.qunar.com/headshot/headshotsById/193976746.png?s&ssl=true', '2016/07/22出发', '文艺范', '家庭', '又到七月暑期，又到家庭出行时。这篇游记有800张照片，文字也长，看完不容易。但我写游记，是为了记录家人的行走，而不是取悦任何人。照片做了一些后期的，也只是作了我自认为是较轻微的处理，主要是影调和层次的调整，内心还是希望能还原真实的所见', '去年的七月，我们一家三口，十二天，从青海到西藏，这是这么多年来，我们第一次家庭长途旅行。由那次旅行结束时起，我们就共同期待着第二年的暑期旅行。', '50', '50', 'https://img1.qunarzz.com/travel/d1/1610/d3/21b441a67b8d19b5.jpg_640x350x70_25ea805b.jpg', 'https://img1.qunarzz.com/travel/d4/1610/c5/c8ccf9ead2fed4b5.jpg_r_640x426x70_20da8111.jpg', 'https://img1.qunarzz.com/travel/d9/1610/16/2889ea5d25040ab5.jpg_r_640x426x70_2eb7f4ab.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786805786357762', '成都重庆，吃货来了就不肯走的天堂~', '小妖_忍', 'https://img1.qunarzz.com/travel/d0/1510/e8/7374708243f469.jpg_640x350x70_6b4c370a.jpg', '2015/02/05出发', '文艺范', '闺蜜', '此行的心愿：', '成都，看看萌物大熊猫，去青城山求仙问道，吃火锅，吃小吃，吃各种好吃哒~~其实冬天这个季节并不是去成都重庆的最佳时节，不过同伴只有这个时间有假期，想想也无不可，去成都重庆当然最主要的就是吃吃吃~青城山往上爬还是挺累的，身体特好或时间充裕的可以尝试，否则也可以选择缆车上山，步行下山，会轻松些。', '27', '27', 'https://img1.qunarzz.com/travel/d0/1510/e8/7374708243f469.jpg_640x350x70_6b4c370a.jpg', 'https://img1.qunarzz.com/travel/d1/1502/3f/c7911f1013a99e.jpg_r_640x480x70_907f769c.jpg', 'https://img1.qunarzz.com/travel/d4/1502/89/d925ee3d9ebf50.jpg_r_640x480x70_a54daf72.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786806189010945', '一路向西——川西五日游', '风叔', 'https://qcommons.qunar.com/headshot/headshotsById/136244514.png?s&ssl=true', '2014/11/06出发', '文艺范', '三五好友', '成都', '这家的火锅还不错。可惜我是南方人，吃不得辣。春熙路步行街，大概相当于广州的北京路。听说很多美女。可惜那天天气略冷，又不是周末，行人比较少。锦里，去成都必去的步行街之一。其实都是卖吃的玩的。', '19', '19', 'https://img1.qunarzz.com/travel/d7/1411/ba/ba617dd8cab7c6ad213a9cdb.jpg_r_640x426x70_a2a0cd0b.jpg', 'https://img1.qunarzz.com/travel/d7/1411/ca/ca1100655d44d899213a9cdb.jpg_r_640x426x70_29789028.jpg', 'https://img1.qunarzz.com/travel/d5/1411/63/63fc1a6c66952389213a9cdb.jpg_r_640x426x70_01fa2d21.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786806516166657', '回眸一瞬,爱上你', '去哪儿用户', 'https://qcommons.qunar.com/headshot/headshotsById/166038227.png?s&ssl=true', '2022/02/03出发', '情侣', '家庭', '三年弹指一挥间,我们的喜乐,定格在彼此的生命里,', '三年前的春天,在汽车站,看到了一个男孩,高高的,带着鸭舌帽,一身休闲装,背着背包,抬头仰望天空的时候,一张秀气的脸上架着深蓝色的眼镜,阳关洒在你的身上,好美的一幅画面,虽然人来人往,我还是被你吸引着,眼睛里的你,好看的像一幅画,那是我们第一次相遇,相见.三年了,我们彼此的心都曾为改变,愿我们一起手牵手,走完属于我们的浪漫.....', '14', '14', 'https://img1.qunarzz.com/travel/d2/1503/33/07e31abda730fc.jpg_640x350x70_cca0591e.jpg', 'https://img1.qunarzz.com/travel/d5/1503/66/7cdf9e2842fe54.jpg_r_640x426x70_41786cfc.jpg', 'https://img1.qunarzz.com/travel/d8/1503/b2/901011f8d834ad.jpg_r_640x426x70_bc7771a9.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786806839128066', '梦一场不如走一场。', 'MEETING0928', 'https://qcommons.qunar.com/headshot/headshotsById/151944752.png?s&ssl=true', '2015/01/19出发', '美食', '三五好友', '世界上没有不快乐的人，只有不快乐的心。', '途中的旅行，不只是欣赏途中的风景而已，更是寻找一个远方的你。途中有开心 有激动 有震撼 有低落 有失望 有落寞 有害怕 有惶恐 还有太多太多的感受，五味杂陈。但人生不也是这样的吗？谁没有尝试过。（希望这篇游记能给即将出发的你有一些好的帮助或参考。）', '24', '24', 'https://img1.qunarzz.com/travel/d9/1502/67/c422018982c7ac.jpg_640x350x70_ce46bf66.jpg', 'https://img1.qunarzz.com/travel/d9/1502/17/3efee466a4ca66.jpg_r_640x479x70_db8ae163.jpg', 'https://img1.qunarzz.com/travel/d1/1502/80/0410ecfd2c6d34.jpg_r_640x479x70_a7390b73.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786807237586946', '一对成都驴的尼泊尔行走', 'qunar用户', 'https://qcommons.qunar.com/headshot/headshotsById/812167.png?s&ssl=true', '2014/10/26出发', '干货', '三五好友', '还没想过哪里玩？救急攻略，拎包就走。', '带父母从泰国跟团回来，马上计划我们自己的尼泊尔之行。周五给淘宝寄出我们的证件及照片，周三就收到了签证。于是买了周日27日昆明转机吉隆坡28日下午到达加德满都的去程和11日加转机吉隆坡14日吉隆坡飞成都的回程。全程亚航。票价刚刚好3500人民币美人。于是十天内完成所有步骤，26日赶成都到昆明的火车出发了。昆明的飞机到达吉隆坡是27日晚上9点多，出口领行李前去旁边凭飞离的机票行程单办理过境签。我们俩决定在机场等待28日11点起飞。所以，我可以负责任的给大家推荐两餐饭，一是台湾的小吃，在超市不远；一是披萨。超级好吃，地道。', '17', '17', 'https://img1.qunarzz.com/travel/d5/1411/4f/4f9ca51cf38d1efe213a9cdb.jpg_640x350x70_e7711787.jpg', 'https://img1.qunarzz.com/travel/d6/1411/d1/d176c504ace80205213a9cdb.jpg_r_640x853x70_04190b2c.jpg', 'https://img1.qunarzz.com/travel/d3/1411/49/495d628af3a0bf55213a9cdb.jpg_r_640x853x70_7456f6d0.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786807560548354', '秋游“人间天堂”九寨沟', '00虾虾00', 'https://qcommons.qunar.com/headshot/headshotsById/155004362.png?s&ssl=true', '2014/10/17出发', '文艺范', '独自一人', '当游客散去，我们在黑夜里悄悄潜入了新世界', 'Long long ago，我就萌生了要去九寨沟的想法。可是一直碍于上九寨的路需要很长时间或者直飞很贵等等的考虑原因，一直都未行程。今年实在憋得受不了了！就毅然出发了，一次旅行、一种心情、一个故事：春时嫩芽点绿，瀑流轻快；夏来绿荫围湖，莺飞燕舞；秋至红叶铺山，彩林满目；冬来雪裹山峦，冰瀑如玉。我选择秋，金灿灿的季节，进入童话的世界。', '12', '12', 'https://img1.qunarzz.com/travel/d3/1412/bf/bf3fe91e33657b85cdb.jpg_640x350x70_752788ae.jpg', 'https://img1.qunarzz.com/travel/d2/1412/49/49585e38713bc970cdb.jpg_r_640x480x70_8f5edba2.jpg', 'https://img1.qunarzz.com/travel/d8/1412/a5/a5f7cbd268d6136ccdb.jpg_r_640x423x70_a5ce5ab3.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786807954812929', '我准备了一个秋天，只为和你见一面', '去哪儿用户', 'https://qcommons.qunar.com/headshot/headshotsById/154348690.png?s&ssl=true', '2015/01/01出发', '干货', '三五好友', '#我的奇妙旅伴 #我的退休生活 #给你看看我的旅行攻略 #就近旅行快乐就行 #十一安全出游进行时  ', '“北国风光，千里冰封，万里雪飘。望长城内外，惟余莽莽；大河上下，顿失滔滔。山舞银蛇，原驰蜡象，欲与天公试比高。晴日，看红装素裹，分外妖娆。江山如此多娇，引无数英雄竞折腰。惜秦皇汉武，略输文采；唐宗宋祖，稍逊风骚。一代天骄，成吉思汗，只识弯弓射大雕。俱往矣，数风流人物，还看今朝。”', '25', '25', 'https://img1.qunarzz.com/travel/d1/1501/73/bef867da883af3.jpg_640x350x70_a519028b.jpg', 'https://img1.qunarzz.com/travel/d0/1501/94/0799012a2ef5a.jpg_r_640x339x70_82b48319.jpg', 'https://img1.qunarzz.com/travel/d6/1501/dc/c815e966506774.jpg_r_640x469x70_e59c90e5.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786808277774338', '最美的自己在路上', 'blth3741', 'https://qcommons.qunar.com/headshot/headshotsById/138793976.png?s&ssl=true', '2014/10/03出发', '文艺范', '三五好友', '说说这次旅行', '久在樊笼里，复得返自然。美醉了——毕棚沟！中午还在家呼呼大睡呢，小伙伴打电话说一起去毕棚沟，我再次确认：“现在？”赶紧起床洗漱，说走就走！带着小棉袄就直接出发了……带着期待与兴奋，说走就走！中午时分，匆匆收拾完毕，一路通畅，正在得意之时，遇到堵车，我们低估了中国人口的数量（哈哈），一路上走走停停，没有市侩的喧嚣，没有喧哗的放浪……', '19', '19', 'https://img1.qunarzz.com/travel/poi/1705/58/dd8b09fff2fe8537.jpg_r_640x853x70_1570677d.jpg', 'https://img1.qunarzz.com/travel/d1/1410/05/d14b0832832ddd37ffffffffc8d65eac.jpg', 'https://img1.qunarzz.com/travel/poi/1705/f1/5fdba8ee236b5137.jpg_r_640x853x70_fffa017b.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786808672038914', '茂县松坪沟之旅', '去哪儿用户', 'https://qcommons.qunar.com/headshot/headshotsById/154647305.png?s&ssl=true', '2014/07/12出发', '文艺范', '三五好友', '说说这次旅行', '前段时间有朋友去到了松坪沟游玩，回来感慨很多，还有很多遗憾。我就问他遗憾什么，他说他去的不是时候，要是在去的下个星期去就好了，因为他们会有一年一度的转山节，很盛大很热闹。他这么一说，再看看他拍的照片，我就开始心里长草了。后来朋友们时间正好合适，我就在他们转山节的那几天去了。    我们一行5人，两男三女，是自己开的车去的，因为朋友们说那边很好走，而且路况也很好，所以我们就完全不用找个专职司机了。我们是在转山节的前二天到达的，第一晚就在茂县县城里的酒店开房睡的，准备明天一早去松坪沟。', '19', '19', 'https://img1.qunarzz.com/travel/d4/201408/02/1f6ef335748a8794c8d65eac.jpeg_640x350x70_07ac9cc6.jpeg', 'https://img1.qunarzz.com/travel/d9/201408/02/793d15bfec058202c8d65eac.jpg_r_640x387x70_646b36d6.jpg', 'https://img1.qunarzz.com/travel/d1/201408/02/a134ce264d70f916c8d65eac.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786808995000322', '康定-木格措', 'HuXin_2014', 'https://qcommons.qunar.com/headshot/headshotsById/146661172.png?s&ssl=true', '2016/02/06出发', '文艺范', '家庭', '说说这次旅行', '老公是康定的，回去住宿是非常方便的，和老公在一起五年啦，这才是第二次去！我也是挺服啦自己的！风景真的挺美的！但我们是过年回去的，县城好多铺子都关啦门、只有去景区好一点！注意自己和家人是否有高原反应！可以带点红景天、路线查好、如果遇到天气不好建议走石棉到泸定、这样路况会安全点！二郎山翻过去就是甘孜州泸定县，但真的很神奇，我们去的时候山这边飘小雪，二郎山隧道一过去就是晴天☀️，真的是太赞啦，回来也有经过二郎山，还看见云海！', '9', '9', 'https://img1.qunarzz.com/travel/d2/1603/38/21e5f47febafacf7.jpg_640x350x70_d8b735b5.jpg', 'https://img1.qunarzz.com/travel/d7/1602/5a/cc670b5713252df7.jpg_r_640x853x70_ad5492d2.jpg', 'https://img1.qunarzz.com/travel/d3/1602/4c/51662ccb5f6388f7.jpg_r_640x853x70_9cc68476.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786809389264897', '20天独行西藏', 'ditz0047', 'https://qcommons.qunar.com/headshot/headshotsById/223403757.png?s&ssl=true', '2016/09/22出发', '文艺范', '独自一人', '独游川藏记', '有时候想要去的地方很多很多，达到目的却又那么那么少……回想这次说走就走的旅行：独自骑行西藏，挑战自己极限，独自一人面对一切的勇气！10月22号和朋友一路开着我的“将军”（一辆Polo小汽车）出发成都，路途1000多公里，2天后到达成都，朋友在成都办完事，然后一路来往雅安、汶川，穿过二朗山路（老的318国道）一路到达康定古城。在古老的康定县城匆匆而过，充满藏族风情的建筑让我想多驻足欣赏，但是没有太多的时间，只留下淡淡的痕迹，美丽的康定夜景……', '21', '21', 'https://img1.qunarzz.com/travel/d8/1703/a6/a6bcd59ce60089b5.jpg_640x350x70_53131297.jpg', 'https://img1.qunarzz.com/travel/d1/1703/97/bc67e34efd941eb5.jpg_r_640x479x70_66401872.jpg', 'https://img1.qunarzz.com/travel/d8/1703/c1/6afb6317f8fae7b5.jpg_r_640x479x70_65edba35.jpg');
INSERT INTO `jr_strategy` VALUES ('1575786809775140865', '通天路-达瓦更扎-日出云海星空', '去哪儿用户', 'https://qcommons.qunar.com/headshot/headshotsById/202384223.png?s&ssl=true', '2016/12/24出发', '文艺范', '三五好友', '说说这次旅行', '众所周知，四川西部多山，而且是吸引眼球、令人向往的大山名山———横断山脉！但正因为它们横断东西，相对高度落差极大，要想全方位欣赏，似乎只能乘飞机俯瞰航拍。近年来随着驴界先驱的开拓，号称360度观景平台相继亮相了：牛背山、四人同、轿顶山、达瓦更扎。其中牛背山当属翘楚，名气之大，好像谁没有登过牛背山在江湖上都没法混下去了！但随着牛背山封山开发，达瓦更扎后来居上，人们蜂拥而至，争相在\"亚洲第二大观景平台\"上一睹风采！圣诞节周末，和驴友相约跟随户外奔向那里，想着为2016年举行一个闭幕仪式！', '24', '24', 'https://img1.qunarzz.com/travel/d5/1702/91/1c07d2c84e550b5.png_640x350x70_6ff08d09.png', 'https://img1.qunarzz.com/travel/poi/1806/9a/fcdb438a09719337.jpg_r_640x476x70_aad27e62.jpg', 'https://img1.qunarzz.com/travel/poi/1806/c7/8cb677ca95b42337.jpg_r_640x425x70_efedac49.jpg');

-- ----------------------------
-- Table structure for jr_user
-- ----------------------------
DROP TABLE IF EXISTS `jr_user`;
CREATE TABLE `jr_user`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL COMMENT '用户id',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL COMMENT '用户姓名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL COMMENT '用户密码',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '用户头像（给予默认头像）',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '用户注册时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '用户修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_german2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jr_user
-- ----------------------------
INSERT INTO `jr_user` VALUES ('1', 'dq', '7fa8282ad93047a4d6fe6111c93b308a', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/avater/2022/10/15/425627b82b5143b5aaaec22ff472a17b.png', '2022-09-20 09:13:26', '2022-10-15 17:48:44');
INSERT INTO `jr_user` VALUES ('1572040081290674178', '杜杜', 'e10adc3949ba59abbe56e057f20f883e', 'https://tse4-mm.cn.bing.net/th/id/OIP-C.JPaFw0vH2f6Qy44aUfZ4jgAAAA', '2022-09-20 09:48:57', '2022-10-21 16:27:11');
INSERT INTO `jr_user` VALUES ('1572160547390410754', '甜甜', '63a9f0ea7bb98050796b649e85481845', 'https://img2.woyaogexing.com/2022/10/05/9b72ca5c97e18e60!400x400.jpg', '2022-09-20 17:47:38', '2022-09-20 17:47:38');
INSERT INTO `jr_user` VALUES ('1572980558630350850', '依依', 'f008d36ed2e6a91fd7a3cec80876ce75', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/avater/THR51(7Y%24%5DU7E_H7LDNTOY8.jpg', '2022-09-23 00:06:04', '2022-09-23 00:06:04');
INSERT INTO `jr_user` VALUES ('1574368035418316802', 'Patricia Harris', '1e132a21f4fc634c99780a5f63b5151c', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/avater/avatar.png', '2022-09-26 19:59:25', '2022-09-26 19:59:25');
INSERT INTO `jr_user` VALUES ('1574368205459595266', 'Mark Lee', '920bf847818f0ac23746a1d7f94d797b', 'https://img2.woyaogexing.com/2022/10/09/cf29bd0ed43cda44!400x400.jpg', '2022-09-26 20:00:05', '2022-09-26 20:00:05');
INSERT INTO `jr_user` VALUES ('1574368236791046146', 'Nancy Garcia', '8beb49e2cf4cd72d9de08bf5d864635d', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/avater/%E7%9D%A1%E8%A7%89avatar.jpg', '2022-09-26 20:00:13', '2022-09-26 20:00:13');
INSERT INTO `jr_user` VALUES ('1574368301148446722', '林婉如', '8bc3fd4ecddeae5ddb7ce03d4804fe93', 'https://img2.woyaogexing.com/2022/10/09/b3265efa3ca8a0f4!400x400.jpg', '2022-09-26 20:00:28', '2022-09-26 20:00:28');
INSERT INTO `jr_user` VALUES ('1574368356110606338', '佩奇不偷猪', '8bc3fd4ecddeae5ddb7ce03d4804fe93', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/avater/7%29~~MC7%28PU00Q3HCKL%7D0S%40T.jpg', '2022-09-26 20:00:41', '2022-09-26 20:00:41');
INSERT INTO `jr_user` VALUES ('1574368411907432449', '林子', '6eae0033a91a6393a6cd90068fbc6044', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/avater/YAGJWLQEE6CFG5~%40CM0ZC2X.jpg', '2022-09-26 20:00:54', '2022-09-26 20:00:54');
INSERT INTO `jr_user` VALUES ('1575750178954559489', 'hkp', 'e10adc3949ba59abbe56e057f20f883e', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/avater/2022/10/19/a749543b77634c20a82f178dd36481ca.jpg', '2022-09-30 15:31:33', '2022-10-19 12:03:27');
INSERT INTO `jr_user` VALUES ('1577180318901104641', '可惜不是你', '96e79218965eb72c92a549dd5a330112', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/avater/2022/10/21/5e067528476b4560a69990826b492d3e.jpg', '2022-10-04 14:14:25', '2022-10-22 19:42:13');
INSERT INTO `jr_user` VALUES ('1581971610189402113', 'hhh', '81dc9bdb52d04dc20036dbd8313ed055', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/%E5%A4%A7%E7%99%BD%E7%86%8A.jpg', '2022-10-17 19:33:18', '2022-10-17 19:33:18');
INSERT INTO `jr_user` VALUES ('1581972926227779586', 'liukang', '202cb962ac59075b964b07152d234b70', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/%E5%A4%A7%E7%99%BD%E7%86%8A.jpg', '2022-10-17 19:38:32', '2022-10-17 19:40:30');
INSERT INTO `jr_user` VALUES ('1581987189667450882', '2211', '3323fe11e9595c09af38fe67567a9394', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/%E5%A4%A7%E7%99%BD%E7%86%8A.jpg', '2022-10-17 20:35:13', '2022-10-17 20:35:13');
INSERT INTO `jr_user` VALUES ('1582882656178192386', '向同学', '0884d3007f1937b76b2e6548a17f36a5', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/%E5%A4%A7%E7%99%BD%E7%86%8A.jpg', '2022-10-20 07:53:28', '2022-10-20 07:53:28');
INSERT INTO `jr_user` VALUES ('1582984184398397442', '小强', '96e79218965eb72c92a549dd5a330112', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/%E5%A4%A7%E7%99%BD%E7%86%8A.jpg', '2022-10-20 14:36:55', '2022-10-20 14:41:32');
INSERT INTO `jr_user` VALUES ('1583025955371270145', '璃仙', '16a3cf5756713388498936f31d058375', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/avater/2022/10/20/51f2b7b9aef847d49e321b53fe6b1e25.jpg', '2022-10-20 17:22:54', '2022-10-20 17:25:10');
INSERT INTO `jr_user` VALUES ('1583392956027551746', 'liukang', 'e10adc3949ba59abbe56e057f20f883e', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/%E5%A4%A7%E7%99%BD%E7%86%8A.jpg', '2022-10-21 17:41:13', '2022-10-21 17:41:13');
INSERT INTO `jr_user` VALUES ('1583411457417068546', 'taio', '646c19d73cd010cf6cfbd90312bc86ae', 'http://img.duoziwang.com/2017/10/170905467917.jpg', '2022-10-21 18:54:44', '2022-10-21 18:55:06');
INSERT INTO `jr_user` VALUES ('1583450448849846273', '佳豪', '96e79218965eb72c92a549dd5a330112', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/%E5%A4%A7%E7%99%BD%E7%86%8A.jpg', '2022-10-21 21:29:41', '2022-10-21 21:29:41');
INSERT INTO `jr_user` VALUES ('1583785843928121346', 'eleven', '621242cfce487bc07e9a53b5f1599972', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/%E5%A4%A7%E7%99%BD%E7%86%8A.jpg', '2022-10-22 19:42:25', '2022-10-22 19:42:25');
INSERT INTO `jr_user` VALUES ('1584491982022033409', '七夜', '16a3cf5756713388498936f31d058375', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/%E5%A4%A7%E7%99%BD%E7%86%8A.jpg', '2022-10-24 18:28:22', '2022-10-24 18:28:22');
INSERT INTO `jr_user` VALUES ('1584716999233699841', 'dq222', '96e79218965eb72c92a549dd5a330112', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/%E5%A4%A7%E7%99%BD%E7%86%8A.jpg', '2022-10-25 09:22:30', '2022-10-25 09:22:30');
INSERT INTO `jr_user` VALUES ('1584718385098850305', 'hkp', '96e79218965eb72c92a549dd5a330112', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/%E5%A4%A7%E7%99%BD%E7%86%8A.jpg', '2022-10-25 09:28:00', '2022-10-28 07:58:10');
INSERT INTO `jr_user` VALUES ('1584891458934603778', 'lzp', '96e79218965eb72c92a549dd5a330112', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/%E5%A4%A7%E7%99%BD%E7%86%8A.jpg', '2022-10-25 20:55:44', '2022-10-25 20:55:44');
INSERT INTO `jr_user` VALUES ('1586260171294310402', 'lzp', 'e10adc3949ba59abbe56e057f20f883e', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/%E5%A4%A7%E7%99%BD%E7%86%8A.jpg', '2022-10-29 15:34:31', '2022-10-29 15:34:31');
INSERT INTO `jr_user` VALUES ('1587009939134021633', '1', '96e79218965eb72c92a549dd5a330112', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/%E5%A4%A7%E7%99%BD%E7%86%8A.jpg', '2022-10-31 17:13:49', '2022-10-31 17:13:49');
INSERT INTO `jr_user` VALUES ('1587076700613181441', '李不想', 'a285a6d8e69077977cbc7fcedc08d241', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/%E5%A4%A7%E7%99%BD%E7%86%8A.jpg', '2022-10-31 21:39:07', '2022-10-31 21:39:07');
INSERT INTO `jr_user` VALUES ('1587255874564980737', 'yh', '5344c45a7e55cc5dad47f6f38861b8c6', 'https://jing-ran-1310347558.cos.ap-chengdu.myqcloud.com/%E5%A4%A7%E7%99%BD%E7%86%8A.jpg', '2022-11-01 09:31:05', '2022-11-01 09:31:05');

SET FOREIGN_KEY_CHECKS = 1;
